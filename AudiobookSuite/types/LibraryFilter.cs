﻿using AudiobookSuite.Commons;
using Newtonsoft.Json;

namespace AudiobookSuite.lib
{
    [JsonObject(MemberSerialization.OptIn)]
    public class LibraryFilter : ILibraryFilter
    {
        [JsonProperty]
        public string Identifier { get; set; }
        [JsonProperty]
        public bool IsActive { get; set; }
        public FilterActionDelegate FilterAction { get; set; }

        public bool CheckFilter(IAudiobook audiobook)
        {
            if (!IsActive)
                return true;

            if (FilterAction != null)
                return FilterAction(audiobook);

            return true;
        }

        public LibraryFilter(string identifier, FilterActionDelegate filterAction, bool isActive = false)
        {
            Identifier = identifier;
            IsActive = isActive;
            FilterAction = filterAction;
        }
    }
}
