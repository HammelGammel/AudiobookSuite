﻿using ATL;
using AudiobookSuite.Commons;
using AudiobookSuite.lib;
using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AudiobookSuite
{
    [JsonObject(MemberSerialization.OptIn)]
    public class AudioFileChapterData : IAudioFileChapterData
    {
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public uint StartTime { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Title { get; set; }

        public int ChapterIdx { get; set; }

        public AudioFileChapterData(uint startTime, string title, int chapterIdx)
        {
            StartTime = startTime;
            Title = title;
            ChapterIdx = chapterIdx;
        }
    }

    public class AudioFileData : IAudioFileData
    {
        public IAudioFile SourceFile { get; set; }
        public string FileName { get; set; }
        public string DirectoryPath { get; set; }
        public string AudioFilePath { get; set; }
        public string Title { get; set; }
        public string Album { get; set; }
        public List<string> Authors { get; set; }
        public List<string> Narrators { get; set; }
        public List<string> Genres { get; set; }
        public List<IAudioFileChapterData> Chapters { get; set; }
        public int TrackNumber { get; set; }
        public string Comment { get; set; }
        public bool ContainsPicture { get; set; }
        public double FileSizeMB { get; set; }

        public AudioFileData(AudioFile file)
        {
            SourceFile = file;
            AudioFilePath = file.AudioFilePath;
            FileName = file.GetFileName();
            DirectoryPath = Path.GetDirectoryName(file.AudioFilePath);

            Authors = new List<string>();
            Narrators = new List<string>();
            Genres = new List<string>();
            Chapters = new List<IAudioFileChapterData>();

            Title = "";

            FileSizeMB = 0;
        }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class AudioFile : IAudioFile
    {
        [JsonProperty("path")]
        public string AudioFilePath { get; set; }

        private TimeSpan _Duration;
        [JsonProperty("time")]
        public TimeSpan Duration
        {
            get { return _Duration; }
            set
            {
                _Duration = value;
                if (AssociatedAudiobook != null)
                    AssociatedAudiobook.UpdateFullDuration();
            }
        }

        public IAudiobook AssociatedAudiobook { get; set; }
        public bool IsPlaying { get; set; }

        public Task CacheDurationTask { get; set; }

        public int TrackNumber {
            get { return GetTrackNumber(); }
        }

        public AudioFile()
        {
        }

        public bool HasValidDuration()
        {
            return Duration.TotalMilliseconds > 1;
        }

        public void MoveToAudiobook(IAudiobook audiobook)
        {
            if (AssociatedAudiobook != null)
            {
                AssociatedAudiobook.AudioFiles.Remove(this);
            }

            audiobook.AudioFiles.Add(this);
            AssociatedAudiobook = audiobook;
        }

        public int GetTrackNumber()
        {
            if (AssociatedAudiobook == null)
                return -1;

            return AssociatedAudiobook.AudioFiles.FindIndex((IAudioFile file) => file.AudioFilePath == AudioFilePath) + 1;
        }

        /* loads audiofile data from disk. Data should not be permanently cached, 
         to save memory for large libraries, So use this method sparingly to
        temporarily cache data */
        public IAudioFileData LoadAudioFileData()
        {
            AudioFileData data = new AudioFileData(this);

            ReadMetaData(data);
            ReadFileSize(data);

            return data;
        }

        public List<IAudioFileChapterData> LoadChapterData(TimeSpan timeOffset, int idxOffset)
        {
            var chapterData = new List<IAudioFileChapterData>();

            Track atlTrack;
            try
            {
                atlTrack = new Track(AudioFilePath);

                if (atlTrack == null)
                {
                    DebugLog.WriteLine("Warning:  ATL .NET failed to read file '" + AudioFilePath + "' - null");
                    return chapterData;
                }
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("Warning: ATL .NET failed to read file '" + AudioFilePath + "' - exception: " + e.Message);
                return chapterData;
            }

            for (int i = 0; i < atlTrack.Chapters.Count; i++)
            {
                var chapter = atlTrack.Chapters[i];
                chapterData.Add(new AudioFileChapterData( (uint)timeOffset.TotalMilliseconds + chapter.StartTime, chapter.Title, idxOffset + i));
            }

            return chapterData;
        }

        public string GetFileName()
        {
            return Path.GetFileNameWithoutExtension(AudioFilePath);
        }

        public bool IsCachingDuration()
        {
            return CacheDurationTask != null && CacheDurationTask.Status.Equals(TaskStatus.Running);
        }

        /* call only when necessary, this is the slowest part of the 
         process of reading metadata from file because taglib data is inaccurate
        for almost every file, sometimes off by a factor of 2 */
        public void CacheDuration()
        {
            /* do not use taglib media duration! TagLib's algorithm is unreliable and 
            often returns the duration * 2 for some reason (and even when it doesn't, the duration is inaccurate)
            But since Windows is already capable of determining file durations itself, just use the Windows shell
            to get those values - SLOW, so parallelize this! */
            try
            {
                IShellProperty prop = ShellObject.FromParsingName(AudioFilePath).Properties.System.Media.Duration;
                object propObj = prop.ValueAsObject;
                if (prop != null && propObj != null)
                {
                    var t = (ulong)propObj;

                    Duration = TimeSpan.FromTicks((long)t);
                }
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("Exception during CacheDuration: " + e.Message);
            }

            OnCacheDurationFinished();
        }

        public Task CacheDurationAsync()
        {
            if (CacheDurationTask != null && CacheDurationTask.Status.Equals(TaskStatus.Running))
                return CacheDurationTask;

            Task task = new Task(CacheDuration);
            task.Start();

            CacheDurationTask = task;
            
            return task;
        }

        public void ReadFileSize(IAudioFileData data)
        {
            try
            {
                data.FileSizeMB = new System.IO.FileInfo(data.AudioFilePath).Length / 1000000.0;
            }
            catch (Exception e)
            {

            }
        }

        public void ReadMetaData(IAudioFileData data)
        {
            if (App.Globals.audiobookScanner == null)
                return;

            if (AudioFilePath == null)
            {
                DebugLog.WriteLine("Warning: TagLib failed to read file - audioFile or path is null");
                return;
            }

            if (!File.Exists(AudioFilePath))
            {
                DebugLog.WriteLine("Warning: TagLib failed to read file - path '" + AudioFilePath + "' is invalid");
                return;
            }

            Track atlTrack;
            try
            {
                atlTrack = new Track(AudioFilePath);

                if (atlTrack == null)
                {
                    DebugLog.WriteLine("Warning:  ATL .NET failed to read file '" + AudioFilePath + "' - null");
                    return;
                }
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("Warning: ATL .NET failed to read file '" + AudioFilePath + "' - exception: " + e.Message);
                data.Title = GetFileName();
                return;
            }

            data.Title = atlTrack.Title;
            data.Album = atlTrack.Album;

            AddToListAndSanitize(data.Narrators, atlTrack.Composer.Split(','));
            AddToListAndSanitize(data.Authors, atlTrack.Artist.Split(','));
            AddToListAndSanitize(data.Genres, atlTrack.Genre.Split(','));

            for (int i = 0; i < atlTrack.Chapters.Count; i++)
            {
                var chapter = atlTrack.Chapters[i];
                data.Chapters.Add(new AudioFileChapterData(chapter.StartTime, chapter.Title, i));
            }

            data.ContainsPicture = atlTrack.EmbeddedPictures.Count > 0;
        }

        public List<string> SeparateString(string strIn)
        {
            List<string> listOut = new List<string>();

            listOut.AddRange(strIn.Split(new char[] {',', ';', '/', '&', '\n' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()));
            for (int i = 0; i < listOut.Count; i++)
            {
                // remove single char entries
                if (listOut[i].Length <= 1)
                {
                    listOut.RemoveAt(i);
                    i--;
                }
            }

            return listOut;
        }

        public void AddToListAndSanitize(List<string> targetList, string[] listToAdd)
        {
            if (listToAdd == null || listToAdd.Length == 0 || targetList == null)
                return;

            foreach (string item in listToAdd)
            {
                if (item == null || item.Length == 0)
                    continue;

                if (!targetList.Exists(itemOther => itemOther != null && itemOther == item))
                {
                    List<string> separatedItems = SeparateString(item);
                    foreach (string separatedItem in separatedItems)
                    {
                        if (separatedItem != null && !targetList.Contains(separatedItem))
                        {
                            targetList.Add(separatedItem);
                        }
                    }
                }
            }

            targetList.Sort();
        }

        public event Action CacheDurationFinished;
        public void OnCacheDurationFinished()
        {
            CacheDurationFinished?.Invoke();
        }
    }
}
