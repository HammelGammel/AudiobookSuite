﻿using AudiobookSuite.Commons;
using AudiobookSuite.controls.usercontrols;
using AudiobookSuite.lib;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Windows.ApplicationModel;

namespace AudiobookSuite
{
    public partial class App : Application
    {
        public static int MajorVersion = 0;
        public static int MinorVersion = 0;
        public static int PatchVersion = 0;

        public static AppGlobals Globals = new AppGlobals();

        public static DispatcherTimer AutosaveTimer = new DispatcherTimer(DispatcherPriority.Render);

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            // suppress binding errors, such as datagrid using fallback values
            // those are usually harmless but can be a lot, especially for datagrids
            //#if DEBUG
            System.Diagnostics.PresentationTraceSources.DataBindingSource.Switch.Level = System.Diagnostics.SourceLevels.Error;
            //#endif

            SetupExceptionHandlers();

            LibVLCSharp.Shared.Core.Initialize();

            try
            {
                Package package = Package.Current;
                if (package != null)
                {
                    PackageVersion version = package.Id.Version;
                    MajorVersion = version.Major;
                    MinorVersion = version.Minor;
                    PatchVersion = version.Build;
                }
            }
            // might not run as MSIX package but as WPF application, in which case this will throw an exception
            // version will just remain at default values in that case; no need to worry
            catch (Exception ex) { }

            App.Globals.audiobookScanner = new AudiobookScanner();
            App.Globals.undoRedo = new UndoRedo();
            App.Globals.settings = new Settings();
            App.Globals.playerControls = new PlayerControls();
            App.Globals.groupManager = new GroupManager();
            App.Globals.keyboardListener = new LowLevelKeyboardListener();
            App.Globals.inputListener = new InputListener();
            App.Globals.textToSpeech = new TextToSpeech();

            App.Globals.keyboardListener.HookKeyboard();

            App.Globals.groupManager.DefaultGroup = new SortingGroup("All");

            AutosaveTimer.Interval = TimeSpan.FromMinutes(Globals.settings.AutosaveIntervalMinutes);
            AutosaveTimer.Tick += Application_Autosave;
            AutosaveTimer.Start();

            App.Globals.keyboardListener.OnKeyPressed += LowLevelOnKeyPressed;

            App.Globals.FinishedInit();

            Globals.settings.LoadSettings();
            Globals.settings.LoadGroupDefinitions();
            Globals.audiobookScanner.LoadBooksFromFile();
        }

        public static void UpdateAutosaveInterval()
        {
            if (AutosaveTimer != null)
            {
                AutosaveTimer.Interval = TimeSpan.FromMinutes(Globals.settings.AutosaveIntervalMinutes);
            }
        }

        private void Application_Autosave(object sender, EventArgs e)
        {
            DebugLog.WriteLine("Autosaving...");

            if (Globals.settings.SaveSettings(ESaveCause.Autosave))
                DebugLog.WriteLine("Settings: Saved");
            else
                DebugLog.WriteLine("Settings: No changes");

            if (Globals.audiobookScanner.SaveBooksToFile(ESaveCause.Autosave))
                DebugLog.WriteLine("Library: Saved");
            else
                DebugLog.WriteLine("Library: No changes");
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            DebugLog.WriteLine("Shutting down...");
            /*App.Globals.keyboardListener.UnHookKeyboard();

            Globals.settings.SaveSettings(ESaveCause.Shutdown);
            Globals.audiobookScanner.SaveBooksToFile(ESaveCause.Shutdown);*/
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            App.Globals.keyboardListener.UnHookKeyboard();

            Globals.settings.SaveSettings(ESaveCause.Shutdown);
            Globals.audiobookScanner.SaveBooksToFile(ESaveCause.Shutdown);
        }

        private void LowLevelOnKeyPressed(object sender, KeyPressedArgs e)
        {
            if (!ApplicationIsActivated())
                return;

            switch (e.KeyPressed)
            {
                case Key.Z:
                    if (Keyboard.IsKeyDown(Key.LeftCtrl))
                    {
                        if (Keyboard.IsKeyDown(Key.LeftShift))
                        {
                            App.Globals.undoRedo.Redo();
                        }
                        else
                        {
                            App.Globals.undoRedo.Undo();
                        }
                    }
                    break;
                case Key.Y:
                    if (Keyboard.IsKeyDown(Key.LeftCtrl))
                    {
                        App.Globals.undoRedo.Redo();
                    }
                    break;
            }
        }

        /// <summary>Returns true if the current application has focus, false otherwise</summary>
        public static bool ApplicationIsActivated()
        {
            var activatedHandle = GetForegroundWindow();
            if (activatedHandle == IntPtr.Zero)
            {
                return false;
            }

            var procId = Process.GetCurrentProcess().Id;
            int activeProcId;
            GetWindowThreadProcessId(activatedHandle, out activeProcId);

            return activeProcId == procId;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);

        private void SetupExceptionHandlers()
        {
            Application.Current.DispatcherUnhandledException += Application_DispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += Application_OnUnhandledException;

            DispatcherUnhandledException += Application_DispatcherUnhandledException;

            TaskScheduler.UnobservedTaskException += Application_TaskSchedulerOnUnobservedTaskException;
        }

        static void Application_TaskSchedulerOnUnobservedTaskException(object? sender, UnobservedTaskExceptionEventArgs e)
        {
            Application_HandleException(e.Exception);
            e.SetObserved();
        }

        private static void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            Application_HandleException(e.Exception);
            e.Handled = true;
        }

        private static void Application_OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception)e.ExceptionObject;
            Application_HandleException(ex);
        }

        public static void Application_HandleException(Exception e, bool shutDownAfter = true)
        {
            string exceptionTitle = shutDownAfter ? "Unhandled Critical Exception" : "Unhandled Exception";

            Dialog_ErrorMessage errorDialog = new Dialog_ErrorMessage(exceptionTitle,
                String.Format(
                    "{0} Error:  {1}\r\n\r\n{2}",
                    e.Source, e.Message, e.StackTrace));

            // problematic if exception is thrown before MainWindow is initialized
            if (Application.Current.MainWindow != errorDialog)
                errorDialog.Owner = Application.Current.MainWindow;

            errorDialog.ShowDialog();

            /* without this, the application will remain opened in the background
             * after encountering an error*/
            if (shutDownAfter)
                Environment.Exit(1);
        }
    }
}
