﻿using AudiobookSuite.Commons;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Controls;

namespace AudiobookSuite.lib
{
    public class DataGridLayoutInfoSettings : IDataGridLayoutInfoSettings
    {
        public int DisplayIndex { get; set; }
        public DataGridLength Width { get; set; }

        [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public ListSortDirection? SortDirection { get; set; }

        public DataGridLayoutInfoSettings(int displayIndex, DataGridLength width, ListSortDirection? sortDirection)
        {
            DisplayIndex = displayIndex;
            Width = width;
            SortDirection = sortDirection;
        }
    }

    /* stores and saves global application settings using JSON */
    [JsonObject(MemberSerialization.OptIn)]
    public class Settings : ISettings
    {
        public List<IDataGridLayoutInfoSettings> LibraryColumnsDefault { get; set; }

        /* */
        /* saveable properties */
        /* */

        private double _AutosaveIntervalMinutes = 5.0;
        [JsonProperty]
        public double AutosaveIntervalMinutes
        {
            get { return _AutosaveIntervalMinutes; }
            set
            {
                if (_AutosaveIntervalMinutes != value)
                {
                    _AutosaveIntervalMinutes = value;
                    App.UpdateAutosaveInterval();
                    OnChangeOption(nameof(AutosaveIntervalMinutes));
                    MarkChanged();
                }
            }
        }

        private List<string> _ScanDirectories;
        [JsonProperty]
        public List<string> ScanDirectories
        {
            get { return _ScanDirectories; }
            set
            {
                if (_ScanDirectories != value)
                {
                    _ScanDirectories = value;
                    OnChangeOption(nameof(ScanDirectories));
                    MarkChanged();
                }
            }
        }

        private List<IKeyBinding> _KeyBindings;
        [JsonProperty(ItemConverterType = typeof(JsonGenericTypeConverter<KeyBinding>))]
        public List<IKeyBinding> KeyBindings
        {
            get { return _KeyBindings; }
            set
            {
                if (_KeyBindings != value)
                {
                    _KeyBindings = value;
                    OnChangeOption(nameof(KeyBindings));
                    MarkChanged();
                }
            }
        }

        private int _ForwardBackwardIntervalSeconds;
        [JsonProperty]
        public int ForwardBackwardIntervalSeconds
        {
            get { return _ForwardBackwardIntervalSeconds; }
            set
            {
                if (_ForwardBackwardIntervalSeconds != value)
                {
                    _ForwardBackwardIntervalSeconds = value;
                    OnChangeOption(nameof(ForwardBackwardIntervalSeconds));
                    MarkChanged();
                }
            }
        }

        private int _Volume;
        [JsonProperty]
        public int Volume
        {
            get { return _Volume; }
            set
            {
                if (_Volume != value)
                {
                    _Volume = value;
                    OnChangeOption(nameof(Volume));
                    MarkChanged();
                }
            }
        }

        private int _FolderSearchDepth;
        [JsonProperty]
        public int FolderSearchDepth
        {
            get { return _FolderSearchDepth; }
            set
            {
                if (_FolderSearchDepth != value)
                {
                    _FolderSearchDepth = value;
                    OnChangeOption(nameof(FolderSearchDepth));
                    MarkChanged();
                }
            }
        }

        private bool _EnableHardwareAcceleration;
        [JsonProperty]
        public bool EnableHardwareAcceleration
        {
            get { return _EnableHardwareAcceleration; }
            set
            {
                if (_EnableHardwareAcceleration != value)
                {
                    _EnableHardwareAcceleration = value;
                    OnChangeOption(nameof(EnableHardwareAcceleration));
                    MarkChanged();

                    MainWindow mainWindow = App.Current.MainWindow as MainWindow;
                    if (mainWindow != null)
                    {
                        if (EnableHardwareAcceleration)
                            mainWindow.EnableHardwareAcceleration();
                        else
                            mainWindow.DisableHardwareAcceleration();
                    }
                }
            }
        }

        private bool _EnableImageCache;
        [JsonProperty]
        public bool EnableImageCache
        {
            get { return _EnableImageCache; }
            set
            {
                if (_EnableImageCache != value)
                {
                    _EnableImageCache = value;
                    OnChangeOption(nameof(EnableImageCache));
                    MarkChanged();

                    if (EnableImageCache)
                        ImageSourceConverter.EnableImageCache();
                    else
                        ImageSourceConverter.DisableImageCache();
                }
            }
        }

        private string _PlayingAudiobookName = "";
        [DefaultValue("")]
        [JsonProperty]
        public string PlayingAudiobookName
        {
            get { return _PlayingAudiobookName; }
            set
            {
                if (_PlayingAudiobookName != value)
                {
                    _PlayingAudiobookName = value;
                    OnChangeOption(nameof(PlayingAudiobookName));
                    MarkChanged();
                }
            }
        }

        private List<int> _LastPlayedIDs;
        [JsonProperty]
        public List<int> LastPlayedIDs
        {
            get { return _LastPlayedIDs; }
            set 
            {
                if (_LastPlayedIDs != value)
                {
                    _LastPlayedIDs = value;
                    OnChangeOption(nameof(LastPlayedIDs));
                }
            }
        }

        private double _LibraryScrollOffset;
        [JsonProperty]
        public double LibraryScrollOffset
        {
            get { return _LibraryScrollOffset; }
            set 
            {
                if (_LibraryScrollOffset != value)
                {
                    _LibraryScrollOffset = value;
                    OnChangeOption(nameof(LibraryScrollOffset));
                }
            }
        }

        private Dictionary<string, bool> _LibraryExpanderStates;
        [JsonProperty]
        public Dictionary<string, bool> LibraryExpanderStates
        {
            get { return _LibraryExpanderStates; }
            set
            {
                if (_LibraryExpanderStates != value)
                {
                    _LibraryExpanderStates = value;
                    OnChangeOption(nameof(LibraryExpanderStates));
                }
            }
        }

        private bool _ScanGenres;
        [JsonProperty]
        public bool ScanGenres
        {
            get { return _ScanGenres; }
            set
            {
                if (_ScanGenres != value)
                {
                    _ScanGenres = value;
                    OnChangeOption(nameof(ScanGenres));
                }
            }
        }

        private bool _CreateParentFolderGroup;
        [JsonProperty]
        public bool CreateParentFolderGroup
        {
            get { return _CreateParentFolderGroup; }
            set
            {
                if (_CreateParentFolderGroup != value)
                {
                    _CreateParentFolderGroup = value;
                    OnChangeOption(nameof(CreateParentFolderGroup));
                }
            }
        }

        private string _SleepTimerWarningSound;
        [JsonProperty]
        public string SleepTimerWarningSound
        {
            get { return _SleepTimerWarningSound; }
            set
            {
                if (_SleepTimerWarningSound != value)
                {
                    _SleepTimerWarningSound = value;
                    OnChangeOption(nameof(SleepTimerWarningSound));
                }
            }
        }

        private int _SleepTimerRewindSeconds;
        [JsonProperty]
        public int SleepTimerRewindSeconds
        {
            get { return _SleepTimerRewindSeconds; }
            set
            {
                if (_SleepTimerRewindSeconds != value)
                {
                    _SleepTimerRewindSeconds = value;
                    OnChangeOption(nameof(SleepTimerRewindSeconds));
                }
            }
        }

        private int _SleepTimerWarningSoundVolume;
        [JsonProperty]
        public int SleepTimerWarningSoundVolume
        {
            get { return _SleepTimerWarningSoundVolume; }
            set
            {
                if (_SleepTimerWarningSoundVolume != value)
                {
                    _SleepTimerWarningSoundVolume = value;
                    OnChangeOption(nameof(SleepTimerWarningSoundVolume));
                }
            }
        }

        private int _SleepTimerFadeOutSpeed;
        public int SleepTimerFadeOutSpeed
        {
            get { return _SleepTimerFadeOutSpeed; }
            set
            {
                if (_SleepTimerFadeOutSpeed != value)
                {
                    _SleepTimerFadeOutSpeed = value;
                    OnChangeOption(nameof(SleepTimerFadeOutSpeed));
                }
            }
        }

        private TimeSpan _SleepTimerFadeOut = new TimeSpan();
        public TimeSpan SleepTimerFadeOut
        {
            get { return _SleepTimerFadeOut; }
            set
            {
                if (_SleepTimerFadeOut != value)
                {
                    _SleepTimerFadeOut = value;
                    OnChangeOption(nameof(SleepTimerFadeOut));
                }
            }
        }

        private TimeSpan _SleepTimerInterval = TimeSpan.FromMinutes(15);
        [JsonProperty]
        public TimeSpan SleepTimerInterval
        {
            get { return _SleepTimerInterval; }
            set
            {
                if (_SleepTimerInterval != value)
                {
                    _SleepTimerInterval = value;
                    OnChangeOption(nameof(SleepTimerInterval));
                }
            }
        }

        private ESleepTimerType _SleepTimerType = ESleepTimerType.TimerSeconds;
        //[JsonProperty]
        public ESleepTimerType SleepTimerType
        {
            get { return _SleepTimerType; }
            set
            {
                if (_SleepTimerType != value)
                {
                    _SleepTimerType = value;
                    OnChangeOption(nameof(SleepTimerType));
                }
            }
        }

        private List<IDataGridLayoutInfoSettings> _LibraryColumns;
        [JsonProperty(ItemConverterType = typeof(JsonGenericTypeConverter<DataGridLayoutInfoSettings>))]
        public List<IDataGridLayoutInfoSettings> LibraryColumns
        {
            get { return _LibraryColumns; }
            set 
            {
                if (_LibraryColumns != value)
                {
                    _LibraryColumns = value;
                    OnChangeOption(nameof(LibraryColumns));
                }
            }
        }

        private float _PlaybackSpeed;
        [JsonProperty]
        public float PlaybackSpeed {
            get { return _PlaybackSpeed; }
            set 
            {
                if (_PlaybackSpeed != value)
                {
                    _PlaybackSpeed = value;
                    OnChangeOption(nameof(PlaybackSpeed));
                    MarkChanged();
                }
            }
        }

        private bool _IsMuted;
        [JsonProperty]
        public bool IsMuted {
            get { return _IsMuted; }
            set 
            {
                if (_IsMuted != value)
                {
                    _IsMuted = value;
                    OnChangeOption(nameof(IsMuted));
                    MarkChanged();
                }
            }
        }
        private List<ILibraryFilter> _LibraryFilters;
        /* tell json, which class to instanciate, when loading the ILibraryFilter (non-instanciable interface) */
        [JsonProperty(ItemConverterType = typeof(JsonGenericTypeConverter<LibraryFilter>))]
        public List<ILibraryFilter> LibraryFilters
        {
            get { return _LibraryFilters; }
            set
            {
                if (_LibraryFilters != value)
                {
                    _LibraryFilters = value;
                    OnChangeOption(nameof(LibraryFilters));
                }
            }
        }

        private string _SearchFilter = "";

        [JsonProperty]
        public string SearchFilter
        {
            get { return _SearchFilter; }
            set
            {
                if (_SearchFilter != value)
                {
                    _SearchFilter = value;
                    OnChangeOption(nameof(SearchFilter));
                }
            }
        }

        private bool _IsConsoleOpen;
        [JsonProperty]
        public bool IsConsoleOpen
        {
            get { return _IsConsoleOpen; }
            set
            {
                if (_IsConsoleOpen != value)
                {
                    _IsConsoleOpen = value;
                    OnChangeOption(nameof(IsConsoleOpen));
                    MarkChanged();
                }
            }
        }

        private bool _IsTTSEnabled;
        [JsonProperty]
        public bool IsTTSEnabled
        {
            get { return _IsTTSEnabled; }
            set
            {
                if (_IsTTSEnabled != value)
                {
                    _IsTTSEnabled = value;
                    OnChangeOption(nameof(IsTTSEnabled));
                }
            }
        }

        private ETTSVoiceType _TTSVoiceType;
        [JsonProperty]
        public ETTSVoiceType TTSVoiceType
        {
            get { return _TTSVoiceType; }
            set
            {
                if (_TTSVoiceType != value)
                {
                    _TTSVoiceType = value;
                    OnChangeOption(nameof(TTSVoiceType));
                }
            }
        }

        private EATLReadChaptersFormat _ATLReadChaptersFormat;
        [JsonProperty]
        public EATLReadChaptersFormat ATLReadChaptersFormat
        {
            get { return _ATLReadChaptersFormat; }
            set
            {
                if (_ATLReadChaptersFormat != value)
                {
                    _ATLReadChaptersFormat = value;
                    OnChangeOption(nameof(ATLReadChaptersFormat));
                }
            }
        }

        private int _MaxLastPlayedAudiobooks = 10;
        [JsonProperty]
        public int MaxLastPlayedAudiobooks
        {
            get { return _MaxLastPlayedAudiobooks; }
            set
            {
                if (_MaxLastPlayedAudiobooks != value)
                {
                    _MaxLastPlayedAudiobooks = value;
                    OnChangeOption(nameof(MaxLastPlayedAudiobooks));
                }
            }
        }

        public GroupDefinitions SettingsGroupDefinitions { get; set; }

        /* only save when any value has changed */
        public bool HasAnythingChanged { get; set; }

        /* */
        /* */

        public Settings()
        {
            ScanDirectories = new List<string>();

            KeyBindings = new List<IKeyBinding>();

            ScanGenres = true;
            ForwardBackwardIntervalSeconds = 15;
            Volume = 50;
            SleepTimerWarningSoundVolume = 50;
            SleepTimerRewindSeconds = 30;
            FolderSearchDepth = 5;
            EnableHardwareAcceleration = true;
            EnableImageCache = true;
            LastPlayedIDs = new List<int>();
            LibraryColumns = new List<IDataGridLayoutInfoSettings>();
            LibraryColumnsDefault = new List<IDataGridLayoutInfoSettings>();
            LibraryExpanderStates = new Dictionary<string, bool>();
            PlaybackSpeed = 1.0f;
            SleepTimerFadeOutSpeed = 2;
            TTSVoiceType = ETTSVoiceType.Female;

            LibraryFilters = new List<ILibraryFilter>();
            LibraryFilters.Add(new LibraryFilter("HideFinished", (IAudiobook audiobook) => { return !audiobook.IsFinished; }, false));
            LibraryFilters.Add(new LibraryFilter("HideHidden", (IAudiobook audiobook) => { return !audiobook.IsHidden; }, true));
            LibraryFilters.Add(new LibraryFilter("ShowOnlyHidden", (IAudiobook audiobook) => { return audiobook.IsHidden; }, false));
        }

        public void FinishedInit()
        {
            App.Globals.audiobookScanner.LoadedAudiobooks += OnScannerLoadedAudiobooks;

            App.Globals.groupManager.RenamedGroup += OnGroupManagerRenamedGroup;
        }

        public bool LoadGroupDefinitions(string? path = null)
        {
            if (path == null)
                path = AppDomain.CurrentDomain.BaseDirectory + ISettings.GroupDefinitionsFilePath;

            if (!File.Exists(path))
            {
                DebugLog.WriteLine("Failed to load group definitions file: '" + path + "' - file doesn't exist");
                return false;
            }

            var settings = new JsonSerializerSettings
            {
                Error = (se, ev) => { ev.ErrorContext.Handled = true; },
                ObjectCreationHandling = ObjectCreationHandling.Replace
            };
            GroupDefinitions loadedGroupDef = JsonConvert.DeserializeObject<GroupDefinitions>(File.ReadAllText(path), settings);

            if (loadedGroupDef == null)
                return false;

            SettingsGroupDefinitions = loadedGroupDef;
            return true;
        }

        public void ToggleLibraryFilter(string identifier, bool? mode = null)
        {
            foreach (LibraryFilter filter in LibraryFilters)
            {
                if (filter.Identifier == identifier)
                {
                    if (mode == filter.IsActive)
                        return;

                    if (mode == null)
                        mode = !(filter.IsActive);

                    filter.IsActive = (bool)mode;
                    OnChangeOption("LibraryFilters");
                    break;
                }
            }
        }

        public bool IsLibraryFilterActive(string identifier)
        {
            foreach (LibraryFilter filter in LibraryFilters)
                if (filter.Identifier == identifier)
                    return filter.IsActive;

            return false;
        }

        public void AddLastPlayedAudiobook(IAudiobook audiobook)
        {
            if (audiobook == null)
                return;

            LastPlayedIDs.Remove(audiobook.UniqueID);
            LastPlayedIDs.Add(audiobook.UniqueID);
            OnChangeOption("LastPlayedIDs");

            while(LastPlayedIDs.Count > 0 && LastPlayedIDs.Count > MaxLastPlayedAudiobooks)
                LastPlayedIDs.RemoveAt(0);
        }

        public void RemoveLastPlayedAudiobook(IAudiobook audiobook)
        {
            if (audiobook == null)
                return;

            LastPlayedIDs.Remove(audiobook.UniqueID);
            OnChangeOption("LastPlayedIDs");
        }

        public void MarkChanged()
        {
            HasAnythingChanged = true;
        }

        public void MarkNothingChanged()
        {
            HasAnythingChanged = false;
        }

        public bool SaveSettings(ESaveCause saveCause = ESaveCause.Manual, string? path = null)
        {
            /* always save again when not autosave, just in case */
            if (HasAnythingChanged || saveCause != ESaveCause.Autosave)
            {
                string jsonString = JsonConvert.SerializeObject(this, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize
                });

                string storagePath = GetSettingsDirectory();

                if (path == null)
                    path = storagePath + ISettings.SettingsFileName;
                string dir = Path.GetDirectoryName(path);

                Directory.CreateDirectory(dir);
                System.IO.File.WriteAllText(path, jsonString);

                if (saveCause == ESaveCause.Autosave)
                    MarkNothingChanged();

                return true;
            }

            return false;
        }

        public bool LoadSettings(string? path = null)
        {
            string storagePath = GetSettingsDirectory();

            if (path == null)
                path = storagePath + ISettings.SettingsFileName;

            if (!File.Exists(path))
            {
                DebugLog.WriteLine("Failed to load settings file: '" + path + "' - file doesn't exist");
                return false;
            }

            var settings = new JsonSerializerSettings { 
                Error = (se, ev) => { ev.ErrorContext.Handled = true; },
                ObjectCreationHandling = ObjectCreationHandling.Replace
            };
            ISettings loadedSettings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(path), settings);

            if (loadedSettings == null)
                return false;

            RestoreFromLoadedObject(loadedSettings);
            OnLoadedSettings();

            ATL.Settings.MP4_readChaptersFormat = (int)ATLReadChaptersFormat;

            /* values were just reloaded, don't need to save them again */
            MarkNothingChanged();

            return true;
        }

        public void RestoreFromLoadedObject(ISettings loadedSettings)
        {
            List<ILibraryFilter> cachedFilters = new List<ILibraryFilter>(LibraryFilters);
            List<ILibraryFilter> loadedFilters = loadedSettings.LibraryFilters;

            Utility.CopyProperties(loadedSettings, this);

            App.Globals.inputListener.InitDefaultKeyBindings();
            // only allow replacing existing bindings from loaded settings. This way, unused bindings will be purged from
            // the .json upon resaving
            foreach (var loadedBinding in loadedSettings.KeyBindings)
            {
                if (loadedBinding == null)
                    continue;

                for(int i = 0; i < KeyBindings.Count; i++)
                {
                    var existingBinding = KeyBindings[i];
                    if (loadedBinding.Identifier == existingBinding.Identifier)
                    {
                        KeyBindings[i] = loadedBinding;
                    }
                }
            }

            LibraryFilters = cachedFilters;

            /* filters can't and shouldn't be saved fully (eg. their action delegate and identifier might change between builds)
             * so only their identifier and IsActive state are saved. Before loading, the current filter list is cached, then
             restored. On the restored list, check if any identifiers match identifiers loaded from file. If so, copy the
            IsActive state over to the actual list */
            foreach (ILibraryFilter currentFilter in LibraryFilters)
            {
                ILibraryFilter currentLoadedFilter = loadedFilters.Find((item) => item.Identifier == currentFilter.Identifier);
                if (currentLoadedFilter != null)
                {
                    currentFilter.IsActive = currentLoadedFilter.IsActive;
                }
            }

            if (IsConsoleOpen)
                ConsoleAllocator.ShowConsoleWindow();
        }

        public string GetSettingsDirectory()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\AudiobookSuite\\saved\\";
        }

        public bool HasValidScanDirectory()
        {
            foreach (string scanDir in ScanDirectories)
            {
                if (Directory.Exists(scanDir))
                    return true;
            }

            return false;
        }

        public void ValidateLoadedGroups()
        {
            var expanderStatesOld = new Dictionary<string, bool>(LibraryExpanderStates);
            LibraryExpanderStates = new Dictionary<string, bool>();

            foreach (var pair in expanderStatesOld)
            {
                if (App.Globals.groupManager.DoesGroupExist(pair.Key) || 
                    pair.Key.ToLower() == App.Globals.groupManager.GetDefaultGroupName().ToLower())
                {
                    LibraryExpanderStates.Add(pair.Key.ToLower(), pair.Value);
                }
            }
        }

        public void OnScannerLoadedAudiobooks()
        {
            /* can only do after audiobooks were loaded - groups are created only if a member 
             registers to them */
            ValidateLoadedGroups();
        }

        /* expanderstaes in settings are saved by the group name. When renamed,
        * remove old entry in LibraryExpanderStates and create new one under new name */
        public void OnGroupManagerRenamedGroup(ISortingGroup group, string oldName, string newName)
        {
            if ( !LibraryExpanderStates.ContainsKey(oldName.ToLower()) )
                return;
             
            bool expanderStateCached = LibraryExpanderStates[oldName.ToLower()];

            LibraryExpanderStates.Remove(oldName.ToLower());
            LibraryExpanderStates.Add(newName.ToLower(), expanderStateCached);
        }

        public event Action LoadedSettings;
        public void OnLoadedSettings()
        {
            LoadedSettings?.Invoke();
        }

        public event ChangeOptionDelegate ChangeOption;
        public void OnChangeOption(string identifier)
        {
            ChangeOption?.Invoke(identifier);
        }
    }
}
