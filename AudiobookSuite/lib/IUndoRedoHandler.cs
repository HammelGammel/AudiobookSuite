﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudiobookSuite.lib
{
    internal interface IUndoRedoHandler
    {
        public bool CanUndo()
        {
            return App.Globals.undoRedo.CanUndo();
        }

        public bool CanRedo()
        {
            return App.Globals.undoRedo.CanRedo();
        }

        public void InitUndoRedo()
        {
            App.Globals.undoRedo.UndoAction += OnUndo;
            App.Globals.undoRedo.RedoAction += OnRedo;
            App.Globals.undoRedo.PushedAction += OnPushedUndoAction;
            App.Globals.undoRedo.RemovedActions += OnRemovedUndoAction;
        }

        public void RemoveUndoRedo()
        {
            App.Globals.undoRedo.UndoAction -= OnUndo;
            App.Globals.undoRedo.RedoAction -= OnRedo;
            App.Globals.undoRedo.PushedAction -= OnPushedUndoAction;
            App.Globals.undoRedo.RemovedActions -= OnRemovedUndoAction;
        }

        public void OnUndo();

        public void OnRedo();

        public void OnPushedUndoAction();

        public void OnRemovedUndoAction();
    }
}
