﻿using System;
using System.Collections.Generic;
using System.Timers;
using System.Windows.Input;
using AudiobookSuite.Commons;
using SharpDX.XInput;

namespace AudiobookSuite.lib
{
    /// <summary>
    /// 
    /// </summary>
    public class InputListener
    {
        public Controller GamepadController { get; set; }

        public System.Timers.Timer GamepadInputPollingTimer = new System.Timers.Timer() { AutoReset = true };

        public GamepadButtonFlags PreviousButtonFlags;

        public event EventHandler<GamepadInputEventArgs> OnGamepadInputStarted;
        public event EventHandler<GamepadInputEventArgs> OnGamepadInputStopped;

        /**/

        public static Dictionary<int, string> KeyNames = new Dictionary<int, string>
        {
            { 0, "None" },
            { 1, "Cancel" },
            { 2, "Back" },
            { 3, "Tab" },
            { 4, "LineFeed" },
            { 5, "Clear" },
            { 6, "Return" },
            { 7, "Pause" },
            { 8, "Capital" },
            { 9, "Kana Mode" },
            { 10, "Junja Mode" },
            { 11, "Final Mode" },
            { 12, "Hanja Mode" },
            { 13, "Escape" },
            { 14, "Convert" },
            { 15, "Non Convert" },
            { 16, "Accept" },
            { 17, "Mode Change" },
            { 18, "Space" },
            { 19, "Page Up" },
            { 20, "Page Down" },
            { 21, "End" },
            { 22, "Home" },
            { 23, "Left" },
            { 24, "Up" },
            { 25, "Right" },
            { 26, "Down" },
            { 27, "Select" },
            { 28, "Print" },
            { 29, "Execute" },
            { 30, "Snapshot" },
            { 31, "Insert" },
            { 32, "Delete" },
            { 33, "Help" },
            { 34, "0" },
            { 35, "1" },
            { 36, "2" },
            { 37, "3" },
            { 38, "4" },
            { 39, "5" },
            { 40, "6" },
            { 41, "7" },
            { 42, "8" },
            { 43, "9" },
            { 44, "A" },
            { 45, "B" },
            { 46, "C" },
            { 47, "D" },
            { 48, "E" },
            { 49, "F" },
            { 50, "G" },
            { 51, "H" },
            { 52, "I" },
            { 53, "J" },
            { 54, "K" },
            { 55, "L" },
            { 56, "M" },
            { 57, "N" },
            { 58, "O" },
            { 59, "P" },
            { 60, "Q" },
            { 61, "R" },
            { 62, "S" },
            { 63, "T" },
            { 64, "U" },
            { 65, "V" },
            { 66, "W" },
            { 67, "X" },
            { 68, "Y" },
            { 69, "Z" },
            { 70, "L Win" },
            { 71, "R Win" },
            { 72, "Apps" },
            { 73, "Sleep" },
            { 74, "Numpad 0" },
            { 75, "Numpad 1" },
            { 76, "Numpad 2" },
            { 77, "Numpad 3" },
            { 78, "Numpad 4" },
            { 79, "Numpad 5" },
            { 80, "Numpad 6" },
            { 81, "Numpad 7" },
            { 82, "Numpad 8" },
            { 83, "Numpad 9" },
            { 84, "Multiply" },
            { 85, "Add" },
            { 86, "Separator" },
            { 87, "Subtract" },
            { 88, "Decimal" },
            { 89, "Divide" },
            { 90, "F1" },
            { 91, "F2" },
            { 92, "F3" },
            { 93, "F4" },
            { 94, "F5" },
            { 95, "F6" },
            { 96, "F7" },
            { 97, "F8" },
            { 98, "F9" },
            { 99, "F10" },
            { 100, "F11" },
            { 101, "F12" },
            { 102, "F13" },
            { 103, "F14" },
            { 104, "F15" },
            { 105, "F16" },
            { 106, "F17" },
            { 107, "F18" },
            { 108, "F19" },
            { 109, "F20" },
            { 110, "F21" },
            { 111, "F22" },
            { 112, "F23" },
            { 113, "F24" },
            { 114, "NumLock" },
            { 115, "Scroll" },
            { 116, "Left Shift" },
            { 117, "Right Shift" },
            { 118, "Left Ctrl" },
            { 119, "Right Ctrl" },
            { 120, "Left Alt" },
            { 121, "Right Alt" },
            { 122, "Browser Back" },
            { 123, "Browser Forward" },
            { 124, "Browser Refresh" },
            { 125, "Browser Stop" },
            { 126, "Browser Search" },
            { 127, "Browser Favorites" },
            { 128, "BrowserHome" },
            { 129, "Volume Mute" },
            { 130, "Volume Down" },
            { 131, "Volume Up" },
            { 132, "Media Next Track" },
            { 133, "Media Previous Track" },
            { 134, "Media Stop" },
            { 135, "Media Play/Pause" },
            { 136, "Launch Mail" },
            { 137, "Select Media" },
            { 138, "Launch Application 1" },
            { 139, "Launch Application 2" },
            { 140, "Semicolon" },
            { 141, "Plus" },
            { 142, "Comma" },
            { 143, "Minus" },
            { 144, "Period" },
            { 145, "Question" },
            { 146, "Tilde" },
            { 147, "AbntC1" },
            { 148, "AbntC2" },
            { 149, "Open Brackets" },
            { 150, "Pipe" },
            { 151, "Close Brackets" },
            { 152, "Quotes" },
            { 153, "Oem8" },
            { 154, "Backslash" },
            { 155, "Processed" },
            { 156, "F10" },
            { 157, "Attn" },
            { 158, "Finish" },
            { 159, "Copy" },
            { 160, "Char" },
            { 161, "Enlw" },
            { 162, "Back Tab" },
            { 163, "NoRoman" },
            { 164, "EnterWordRegisterMode" },
            { 165, "EnterImeConfigureMode" },
            { 166, "EraseEof" },
            { 167, "Play" },
            { 168, "Zoom" },
            { 169, "NoName" },
            { 170, "Pa1" },
            { 171, "Clear" },
            { 172, "Dead Char Processed" },
        };

        public static Dictionary<int, string> GamepadButtonNames = new Dictionary<int, string>
        {
            { 0x0, "None" },
            { 0x1, "Gamepad DPadUp" },
            { 0x2, "Gamepad DPadDown" },
            { 0x4, "Gamepad DPadLeft" },
            { 0x8, "Gamepad DPadRight" },
            { 0x10, "Gamepad Start" },
            { 0x20, "Gamepad Back" },
            { 0x40, "Gamepad LeftThumb" },
            { 0x80, "Gamepad RightThumb" },
            { 0x100, "Gamepad LeftShoulder" },
            { 0x200, "Gamepad RightShoulder" },
            { 0x1000, "Gamepad A" },
            { 0x2000, "Gamepad B" },
            { 0x4000, "Gamepad X" },
            { 0x8000, "Gamepad Y" },
        };

        /**/

        public InputListener()
        {
            GamepadController = new Controller(UserIndex.One);
        }

        ~InputListener()
        {
            App.Globals.keyboardListener.OnKeyPressed -= LowLevelOnKeyPressed;

            OnGamepadInputStarted -= OnGamepadInputStartedCallback;
            OnGamepadInputStopped -= OnGamepadInputStoppedCallback;

            GamepadInputPollingTimer.Elapsed -= CheckGamepadStateChanged;
        }

        public void FinishedInit()
        {
            App.Globals.keyboardListener.OnKeyPressed += LowLevelOnKeyPressed;

            OnGamepadInputStarted += OnGamepadInputStartedCallback;
            OnGamepadInputStopped += OnGamepadInputStoppedCallback;

            GamepadInputPollingTimer.Interval = 50;
            GamepadInputPollingTimer.Elapsed += CheckGamepadStateChanged;
            GamepadInputPollingTimer.Start();
        }

        public void InitDefaultKeyBindings()
        {
            App.Globals.settings.KeyBindings = new List<IKeyBinding>
            {
                new KeyBinding("Toggle Pause",
                new List<ITrigger> { }),

                new KeyBinding("Play",
                new List<ITrigger> { }),

                new KeyBinding("Pause",
                new List<ITrigger> { }),

                new KeyBinding("Stop",
                new List<ITrigger> { }),

                new KeyBinding("Seek Forward",
                new List<ITrigger> { }),

                new KeyBinding("Seek Back",
                new List<ITrigger> { }),

                new KeyBinding("Next Chapter",
                new List<ITrigger> { }),

                new KeyBinding("Previous Chapter",
                new List<ITrigger> { }),

                new KeyBinding("Volume Up",
                new List<ITrigger> { }),

                new KeyBinding("Volume Down",
                new List<ITrigger> { }),

                new KeyBinding("Jump To Next Bookmark",
                new List<ITrigger> { }),

                new KeyBinding("Jump To Earlier Bookmark",
                new List<ITrigger> { }),

                new KeyBinding("Add Bookmark",
                new List<ITrigger> { }),

                new KeyBinding("Remove Bookmark",
                new List<ITrigger> { }),

                new KeyBinding("Next Audiobook",
                new List<ITrigger> { }),

                new KeyBinding("Previous Audiobook",
                new List<ITrigger> { }),
            };
        }

        public void OnKeyBindingKeyPressed(IKeyBinding binding)
        {
            DebugLog.WriteLine("KeyBinding was triggered: " + binding.Identifier);

            switch (binding.Identifier)
            {
                case "Toggle Pause":
                    App.Globals.playerControls.PlayPause();
                    break;

                case "Play":
                    App.Globals.playerControls.Play();
                    break;

                case "Pause":
                    App.Globals.playerControls.Pause();
                    break;

                case "Stop":
                    App.Globals.playerControls.Stop();
                    break;

                case "Seek Forward":
                    App.Globals.playerControls.Forward();
                    break;

                case "Seek Back":
                    App.Globals.playerControls.Rewind();
                    break;

                case "Next Chapter":
                    App.Globals.playerControls.NextChapter();
                    break;

                case "Previous Chapter":
                    App.Globals.playerControls.PreviousChapter();
                    break;

                case "Volume Up":
                    App.Globals.playerControls.IncrementVolume();
                    break;

                case "Volume Down":
                    App.Globals.playerControls.DecrementVolume();
                    break;

                case "Jump To Next Bookmark":
                    App.Globals.playerControls.JumpToNextBookmark();
                    break;

                case "Jump To Earlier Bookmark":
                    App.Globals.playerControls.JumpToPreviousBookmark();
                    break;

                case "Add Bookmark":
                    App.Globals.playerControls.AddAutomaticBookMark();
                    break;

                case "Remove Bookmark":
                    App.Globals.playerControls.RemoveCloseBookMark();
                    break;

                case "Next Audiobook":
                    App.Globals.playerControls.PlayNextAudiobook();
                    break;

                case "Previous Audiobook":
                    App.Globals.playerControls.PlayPreviousAudiobook();
                    break;
            }
        }

        private void CheckGamepadStateChanged(object source, ElapsedEventArgs e)
        {
            if (!GamepadController.IsConnected)
                return;

            var currentButtonFlags = GamepadController.GetState().Gamepad.Buttons;

            if (currentButtonFlags != PreviousButtonFlags)
            {
                foreach (GamepadButtonFlags button in Enum.GetValues(typeof(GamepadButtonFlags)))
                {
                    if (currentButtonFlags.HasFlag(button) && !PreviousButtonFlags.HasFlag(button))
                    {
                        OnGamepadInputStarted(this, new GamepadInputEventArgs(button));
                    }
                    else if (!currentButtonFlags.HasFlag(button) && PreviousButtonFlags.HasFlag(button))
                    {
                        OnGamepadInputStopped(this, new GamepadInputEventArgs(button));
                    }
                }

                PreviousButtonFlags = currentButtonFlags;
            }
        }

        private void OnGamepadInputStartedCallback(object? sender, GamepadInputEventArgs e)
        {
            CheckKeyBindingControllerInputChanged(e.InputFlags);
        }

        private void OnGamepadInputStoppedCallback(object? sender, GamepadInputEventArgs e)
        {
            CheckKeyBindingControllerInputChanged(e.InputFlags);
        }

        private void LowLevelOnKeyPressed(object sender, KeyPressedArgs e)
        {
            var pCtrols = App.Globals.playerControls;

            switch (e.KeyPressed)
            {
                case Key.MediaPlayPause:
                    pCtrols.PlayPause();
                    break;

                case Key.MediaStop:
                    pCtrols.Stop();
                    break;

                case Key.MediaNextTrack:
                    pCtrols.Forward();
                    break;

                case Key.MediaPreviousTrack:
                    pCtrols.Rewind();
                    break;
            }

            if (!pCtrols.IsInputEnabled())
                return;

            CheckKeyBindingKeyPressed(e);

            if (!App.ApplicationIsActivated())
                return;

            switch (e.KeyPressed)
            {
                case Key.Space:
                    pCtrols.PlayPause();
                    break;

                case Key.Left:
                    pCtrols.Rewind();
                    break;

                case Key.Right:
                    pCtrols.Forward();
                    break;
            }
        }

        public void CheckKeyBindingKeyPressed(KeyPressedArgs e)
        {
            foreach (var keyBinding in App.Globals.settings.KeyBindings)
            {
                if (keyBinding.IsPressed(e))
                    OnKeyBindingKeyPressed(keyBinding);
            }
        }

        public void CheckKeyBindingControllerInputChanged(GamepadButtonFlags inputFlags)
        {
            foreach (var keyBinding in App.Globals.settings.KeyBindings)
            {
                if (keyBinding.IsPressed(inputFlags))
                    OnKeyBindingKeyPressed(keyBinding);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GamepadInputEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public GamepadButtonFlags InputFlags { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public GamepadInputEventArgs(GamepadButtonFlags inputFlags)
        {
            InputFlags = inputFlags;
        }
    }
}
