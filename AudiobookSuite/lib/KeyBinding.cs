﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using AudiobookSuite.Commons;
using Newtonsoft.Json;
using SharpDX.XInput;

namespace AudiobookSuite.lib
{
    /// <summary>
    /// 
    /// </summary>
    public class KeyBinding : IKeyBinding
    {
        public string Identifier { get; set; }

        [JsonProperty(ItemConverterType = typeof( JsonGenericTypeConverter<Trigger>) )]
        public List<ITrigger> Triggers { get; set; } = new List<ITrigger>();

        [JsonProperty]
        public bool RequireAppFocus { get; set; }

        /**/

        public KeyBinding()
        {
            Identifier = "None";
            Triggers = new List<ITrigger>();
            RequireAppFocus = true;
        }

        public KeyBinding(string identifier, List<ITrigger> triggers)
        {
            Identifier = identifier;
            RequireAppFocus = true;

            if (triggers != null)
                Triggers = triggers;
        }

        /**/

        public bool AreTriggersEmpty()
        {
            if (Triggers.Count == 0)
                return true;

            foreach (ITrigger trigger in Triggers)
            {
                if (trigger.IsKeyboardTrigger())
                {
                    if (trigger.TriggerKeyboard.HasValue && trigger.TriggerKeyboard.Value != Key.None)
                        return false;
                }
                else
                {
                    if (trigger.TriggerGamepad.HasValue && trigger.TriggerGamepad.Value != GamepadButtonFlags.None)
                        return false;
                }
            }

            return true;
        }

        public bool IsPressed()
        {
            if (AreTriggersEmpty() || (RequireAppFocus && !App.ApplicationIsActivated()))
                return false;

            var keyboardListener = App.Globals.keyboardListener;

            foreach (var trigger in Triggers)
                if (trigger.TriggerKeyboard != null && trigger.TriggerKeyboard != Key.None && !keyboardListener.IsKeyDown(trigger.TriggerKeyboard.Value))
                    return false;

            try
            {
                Controller gamepadController = App.Globals.inputListener.GamepadController;
                if (gamepadController.IsConnected)
                {
                    GamepadButtonFlags gamepadButtons = gamepadController.GetState().Gamepad.Buttons;

                    // check controller input
                    foreach (var trigger in Triggers)
                        if (!gamepadButtons.HasFlag(trigger.TriggerGamepad))
                            return false;
                }
            }
            catch (Exception e)
            { }

            return true;
        }

        public bool IsPressed(KeyPressedArgs instigator)
        {
            if (AreTriggersEmpty() || (RequireAppFocus && !App.ApplicationIsActivated()))
                return false;

            foreach (var trigger in Triggers)
                if (trigger.TriggerKeyboard == instigator.KeyPressed)
                    return IsPressed();

            return false;
        }

        public bool IsPressed(GamepadButtonFlags instigator)
        {
            if (AreTriggersEmpty() || instigator == GamepadButtonFlags.None || (RequireAppFocus && !App.ApplicationIsActivated()))
                return false;

            Controller gamepadController = App.Globals.inputListener.GamepadController;
            if (!gamepadController.IsConnected)
                return false;
            
            GamepadButtonFlags gamepadButtons = gamepadController.GetState().Gamepad.Buttons;

            if (gamepadButtons.HasFlag(instigator))
                 return IsPressed();

            return false;
        }
    }

    public class Trigger : ITrigger
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Key? TriggerKeyboard { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public GamepadButtonFlags? TriggerGamepad { get; set; }

        /**/

        public Trigger()
        { }

        public Trigger(System.Windows.Input.Key triggerKeyboard)
        {
            TriggerKeyboard = triggerKeyboard;
            TriggerGamepad = null;
        }

        public Trigger(GamepadButtonFlags triggerGamepad)
        {
            TriggerGamepad = triggerGamepad;
            TriggerKeyboard = null;
        }

        /**/

        public void SetTrigger(Key triggerKeyboard)
        {
            TriggerKeyboard = triggerKeyboard;
            TriggerGamepad = null;
        }

        public void SetTrigger(GamepadButtonFlags triggerGamepad)
        {
            TriggerGamepad = triggerGamepad;
            TriggerKeyboard = null;
        }

        public bool IsKeyboardTrigger()
        {
            return TriggerKeyboard != null;
        }

        public bool IsGamepadTrigger()
        {
            return TriggerGamepad != null;
        }

        public string GetTriggerDisplayString()
        {
            if (IsKeyboardTrigger())
            {
                if (TriggerKeyboard != null && InputListener.KeyNames.ContainsKey((int)TriggerKeyboard))
                    return InputListener.KeyNames[(int)TriggerKeyboard];
            }
            else if (IsGamepadTrigger())
            {
                if (TriggerGamepad != null && InputListener.GamepadButtonNames.ContainsKey((int)TriggerGamepad))
                    return InputListener.GamepadButtonNames[(int)TriggerGamepad];
            }

            return "None";
        }
    }
}
