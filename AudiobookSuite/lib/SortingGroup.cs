﻿using AudiobookSuite.Commons;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace AudiobookSuite.lib
{
    [JsonObject(MemberSerialization.OptIn)]
    class SortingGroup : ISortingGroup
    {
        public bool IsEnabled { get; set; }
        
        [JsonProperty]
        public string GroupName { get; set; }

        public List<object> LinkedObjects { get; set; }

        public SortingGroup(string title, bool isEnabled = true)
        {
            GroupName = title;
            IsEnabled = isEnabled;
            LinkedObjects = new List<object>();
        }
    }
}
