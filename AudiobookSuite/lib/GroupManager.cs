﻿using AudiobookSuite.Commons;
using System.Collections.Generic;
using System.Reflection;

namespace AudiobookSuite.lib
{
    class GroupManager : IGroupManager
    {
        public Dictionary<string, ISortingGroup> Groups { get; set; }
        public ISortingGroup DefaultGroup { get; set; }

        public GroupManager()
        {
            Groups = new Dictionary<string, ISortingGroup>();
        }

        public void FinishedInit()
        {

        }

        public bool AddToGroup(ISortingGroup group, object linkedObject)
        {
            if (group != null && !(group.LinkedObjects.Contains(linkedObject)) )
            {
                group.LinkedObjects.Add(linkedObject);
                return true;
            }

            return false;
        }

        public bool AddToGroup(string groupName, object linkedObject)
        {
            ISortingGroup group = GetGroup(groupName);
            if (group == null)
            {
                AddGroup(groupName);
                group = GetGroup(groupName);
            }

            return AddToGroup(group, linkedObject);
        }

        public void RemoveFromAllGroups(object item)
        {
            foreach (var groupPair in Groups)
            {
                RemoveFromGroup(groupPair.Value, item);
            }
        }

        public void RemoveFromGroup(ISortingGroup group, object item)
        {
            if (group != null)
            {
                MethodInfo method = item.GetType().GetMethod("GroupManager_RemoveFromGroup");
                if (method != null)
                {
                    object[] firstParameter = new object[] { group };
                    object[] parameters = new object[] { firstParameter };

                    method.Invoke(item, parameters);
                }

                group.LinkedObjects.Remove(item);

                /* group has no members, remove it */
                if (group.LinkedObjects.Count == 0)
                {
                    RemoveGroup(group);
                }
            }
        }

        public void RemoveFromGroup(string groupName, object linkedObject)
        {
            groupName = SanitizeGroupName(groupName);

            RemoveFromGroup(GetGroup(groupName.ToLower()), linkedObject);
        }

        public bool AddGroup(ISortingGroup group)
        {
            string groupNameLower = SanitizeGroupName(group.GroupName).ToLower();

            if (!Groups.ContainsKey(groupNameLower))
            {
                Groups.Add(groupNameLower, group);
                return true;
            }

            return false;
        }

        public bool AddGroup(string groupName)
        {
            if (groupName == null || groupName.Length == 0)
                return false;

            groupName = SanitizeGroupName(groupName);

            ISortingGroup existingGroup = GetGroup(groupName);

            if (existingGroup == null)
            {
                ISortingGroup newGroup = new SortingGroup(groupName);
                Groups.Add(groupName.ToLower(), newGroup);

                OnAddedGroup(newGroup);
                return true;
            }
            /* updates upper/lowercase of the group */
            else
                existingGroup.GroupName = groupName;

            return false;
        }

        public bool RemoveGroup(ISortingGroup group)
        {
            if (group == null)
                return false;

            for (int i = 0; i < group.LinkedObjects.Count; i++)
            {
                object item = group.LinkedObjects[i];
                
                if (item != null)
                {
                    MethodInfo method = item.GetType().GetMethod("GroupManager_RemoveFromGroup");
                    if (method != null)
                    {
                        object[] firstParameter = new object[] { group };
                        object[] parameters = new object[] { firstParameter };

                        method.Invoke(item, parameters);
                    }

                    group.LinkedObjects.Remove(item);
                    i--;
                }
            }

            group.LinkedObjects.Clear();
            Groups.Remove(group.GroupName.ToLower());

            OnRemovedGroup(group);

            return true;
        }

        public bool RemoveGroup(string groupName)
        {
            return RemoveGroup(GetGroup(groupName));
        }

        public ISortingGroup? GetGroup(string groupName)
        {
            string groupNameLower = groupName.ToLower();

            if (Groups.ContainsKey(groupNameLower))
                return Groups[groupNameLower];
            return null;
        }

        public bool DoesGroupExist(string groupName)
        {
            return Groups.ContainsKey(groupName.ToLower());
        }

        public string GetDefaultGroupName()
        {
            if (DefaultGroup == null)
                return "";
            return DefaultGroup.GroupName;
        }

        public void RenameGroup(ISortingGroup group, string newName)
        {
            newName = SanitizeGroupName(newName);

            if (group.GroupName == newName)
                return;

            string oldName = group.GroupName;

            Groups.Remove(group.GroupName.ToLower());

            group.GroupName = newName;
            AddGroup(group);

            OnRenamedGroup(group, oldName, newName);
        }

        public string SanitizeGroupName(string groupName)
        {
            return groupName.Trim();
        }

        public ISortingGroup GetDefaultGroup()
        {
            return DefaultGroup;
        }

        public event GroupDelegate AddedGroup;
        public void OnAddedGroup(ISortingGroup group)
        {
            AddedGroup?.Invoke(group);
        }

        public event GroupDelegate RemovedGroup;
        public void OnRemovedGroup(ISortingGroup group)
        {
            RemovedGroup?.Invoke(group);
        }

        public event GroupRenamedDelegate RenamedGroup;
        public void OnRenamedGroup(ISortingGroup group, string oldName, string newName)
        {
            RenamedGroup?.Invoke(group, oldName, newName);
        }
    }
}
