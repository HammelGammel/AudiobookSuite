﻿using AudiobookSuite.Commons;

namespace AudiobookSuite.lib
{
    public class AppGlobals : IAppGlobals
    {
        public IAudiobookScanner audiobookScanner { get; set; }
        public IUndoRedo undoRedo { get; set; }
        public ISettings settings { get; set; }
        public IPlayerControls playerControls { get; set; }
        public IGroupManager groupManager { get; set; }
        public LowLevelKeyboardListener keyboardListener { get; set; }
        public InputListener inputListener { get; set; }
        public TextToSpeech textToSpeech { get; set; }

        public void FinishedInit()
        {
            audiobookScanner.FinishedInit();
            undoRedo.FinishedInit();
            settings.FinishedInit();
            playerControls.FinishedInit();
            groupManager.FinishedInit();
            keyboardListener.FinishedInit();
            inputListener.FinishedInit();
            textToSpeech.FinishedInit();
        }
    }
}
