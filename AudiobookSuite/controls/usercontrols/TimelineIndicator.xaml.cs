﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for TimelineIndicator.xaml
    /// </summary>
    public partial class TimelineIndicator : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private static int HeightOneLine = 30;
        private static int HeightTwoLines = 45;

        private TimeSpan _TimelinePosition;
        public TimeSpan TimelinePosition {
            get { return _TimelinePosition; }
            set
            {
                _TimelinePosition = value;
                NotifyPropertyChanged("TimelinePosition");
            }
        }

        private string _ChapterName;
        public string ChapterName
        {
            get { return _ChapterName; }
            set
            {
                _ChapterName = value;
                NotifyPropertyChanged("ChapterName");
            }
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public TimelineIndicator()
        {
            InitializeComponent();

            TimelinePosition = TimeSpan.FromSeconds(0);
        }

        public void SetTimelinePosition(TimeSpan position)
        {
            TimelinePosition = position;
        }

        public void SetChapterName(string name)
        {
            ChapterName = name;

            if (name == null || name.Length == 0)
            {
                TxtChapterName.Visibility = Visibility.Collapsed;
                Height = HeightOneLine;
            }
            else
            {
                TxtChapterName.Visibility = Visibility.Visible;
                Height = HeightTwoLines;
            }
        }
    }
}
