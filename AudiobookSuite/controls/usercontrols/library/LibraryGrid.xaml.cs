﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace AudiobookSuite
{
    /// <summary>
    /// Interaction logic for LibraryGrid.xaml
    /// </summary>
    public partial class LibraryGrid : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /**/

        public LibraryGrid()
        {
            DataContext = this;

            InitializeComponent();

            Unloaded += OnUnloaded;
            App.Globals.audiobookScanner.LoadedAudiobooks += OnLoadedAudiobooks;
            App.Globals.settings.LoadedSettings += LoadedSettings;
        }

        ~LibraryGrid()
        {
        }

        public void FinishedInit()
        {
        }

        void OnUnloaded(object sender, EventArgs e)
        {
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
        }

        public void SettingsSetOption(string identifier)
        {
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void LoadedSettings()
        {
        }

        public void OnLoadedAudiobooks()
        {
        }

        private void LibraryGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
        }
    }
}
