﻿using AudiobookSuite.Commons;
using AudiobookSuite.lib;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AudiobookSuite.controls
{
    /// <summary>
    /// Interaction logic for KeyBindingCell.xaml
    /// </summary>
    public partial class KeyBindingCell : UserControl
    {
        public ITrigger AssociatedTrigger { get; set; }
        public KeyBindingRow AssociatedRow{ get; set; }

        public bool IsEditing { get; set; } = false;

        /**/

        public KeyBindingCell()
        {
            DataContext = this;
            InitializeComponent();
        }

        public KeyBindingCell(KeyBindingRow associatedRow, ITrigger associatedTrigger)
        {
            DataContext = this;
            InitializeComponent();

            AssociatedRow = associatedRow;
            AssociatedTrigger = associatedTrigger;

            Reconstruct();
        }

        /**/

        public void Reconstruct()
        {
            TriggerDisplayName.Text = AssociatedTrigger.GetTriggerDisplayString();

            TriggerButton.ToolTip = AssociatedTrigger.GetTriggerDisplayString();
        }

        public void StartEditing(bool calledFromParent = false)
        {
            TriggerDisplayName.Text = "Enter Key...";
            TriggerButton.ToolTip = "";

            if (!calledFromParent)
                AssociatedRow.OnStartEditing(this);

            App.Globals.inputListener.OnGamepadInputStarted += OnGamepadInputStartedCallback;

            IsEditing = true;
        }

        public void StopEditing(bool calledFromParent = false)
        {
            if (!IsEditing)
                return;

            if (!calledFromParent)
                AssociatedRow.OnStopEditing(this);

            App.Globals.inputListener.OnGamepadInputStarted -= OnGamepadInputStartedCallback;

            IsEditing = false;
        }

        private void Cell_Click(object sender, RoutedEventArgs e)
        {
            StartEditing();
        }

        private void Cell_KeyDown(object sender, KeyEventArgs e)
        {
            if (IsEditing)
            {
                if (e.Key == Key.Escape)
                    StopEditing();
                else
                {
                    AssociatedTrigger.SetTrigger(e.Key);
                    StopEditing();
                }
            }
        }

        private void OnGamepadInputStartedCallback(object? sender, GamepadInputEventArgs e)
        {
            if (IsEditing)
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    AssociatedTrigger.SetTrigger(e.InputFlags);
                    StopEditing();
                }));
            }
        }

        private void Cell_LostFocus(object sender, RoutedEventArgs e)
        {
            StopEditing();
        }

        private void ButtonRemoveEntry_Click(object sender, RoutedEventArgs e)
        {
            AssociatedRow.RemoveTrigger(this);
        }
    }
}
