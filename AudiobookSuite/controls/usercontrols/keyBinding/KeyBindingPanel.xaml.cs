﻿using System.Windows.Controls;

namespace AudiobookSuite.controls
{
    /// <summary>
    /// Interaction logic for KeyBindingPanel.xaml
    /// </summary>
    public partial class KeyBindingPanel : UserControl
    {
        public KeyBindingPanel()
        {
            DataContext = this;
            InitializeComponent();

            Reconstruct();
        }

        /**/

        public void Reconstruct()
        {
            KeyBindingsAnchor.Children.Clear();

            var keyBindings = App.Globals.settings.KeyBindings;
            foreach (var keyBinding in keyBindings)
            {
                KeyBindingRow newRow = new KeyBindingRow(this, keyBinding);
                KeyBindingsAnchor.Children.Add(newRow);
            }
        }

        /**/

        public void CancelEditing()
        {
            var playerControls = App.Globals.playerControls;
            playerControls.SetInputEnabled("rebindshortcut");

            foreach (KeyBindingRow child in KeyBindingsAnchor.Children)
            {
                child.Reconstruct();
            }
        }

        public void OnStartEditing(KeyBindingCell callingCell)
        {
            var playerControls = App.Globals.playerControls;
            playerControls.SetInputDisabled("rebindshortcut");
        }

        public void OnStopEditing(KeyBindingCell callingCell)
        {
            var playerControls = App.Globals.playerControls;
            playerControls.SetInputEnabled("rebindshortcut");

            foreach (KeyBindingRow child in KeyBindingsAnchor.Children)
            {
                child.Reconstruct();
            }
        }
    }
}
