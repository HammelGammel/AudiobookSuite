﻿using AudiobookSuite.Commons;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;

namespace AudiobookSuite
{
    /// <summary>
    /// Interaction logic for LastPlayedRow.xaml
    /// </summary>
    public partial class LastPlayedRow : UserControl, INotifyPropertyChanged
    {
        public delegate void RemoveLastPlayedDelegate(IAudiobook LinkedAudiobook);
        public event PropertyChangedEventHandler PropertyChanged;

        private IAudiobook _LinkedAudiobook;
        public IAudiobook LinkedAudiobook
        {
            get { return _LinkedAudiobook; }
            set
            {
                _LinkedAudiobook = value;
                PropertyChanged(this, new PropertyChangedEventArgs("LinkedAudiobook"));
            }
        }

        public LastPlayedRow()
        {
            DataContext = this;
            InitializeComponent();
        }

        private void UserControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (LinkedAudiobook != null)
            {
                App.Globals.playerControls.PlayAudiobook(LinkedAudiobook);
            }
        }

        private void Click_Play(object sender, System.Windows.RoutedEventArgs e)
        {
            App.Globals.playerControls.PlayAudiobook(LinkedAudiobook);
        }

        private void Click_Details(object sender, System.Windows.RoutedEventArgs e)
        {
            AudiobookDetails audiobookDetails = new AudiobookDetails(LinkedAudiobook);

            var window = App.Current.MainWindow as NavigationWindow;
            window.NavigationService.Navigate(audiobookDetails);
        }

        private void Click_RemoveRecentlyPlayed(object sender, System.Windows.RoutedEventArgs e)
        {
            App.Globals.settings.RemoveLastPlayedAudiobook(LinkedAudiobook);
            OnRemoveLastPlayed(LinkedAudiobook);
        }

        public event RemoveLastPlayedDelegate RemoveLastPlayed;
        public void OnRemoveLastPlayed(IAudiobook audiobook)
        {
            RemoveLastPlayed?.Invoke(audiobook);
        }
    }
}
