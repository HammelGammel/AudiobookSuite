﻿using AudiobookSuite.Commons;
using AudiobookSuite.controls.usercontrols;
using AudiobookSuite.lib;
using LibVLCSharp.Shared;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AudiobookSuite
{
    /// <summary>
    /// Interaction logic for PlayerControls.xaml
    /// </summary>
    public partial class PlayerControls : UserControl, IPlayerControls, INotifyPropertyChanged
    {
        public static LibVLC GlobalLibVLC = new LibVLC("--aout=directsound");
        public static LibVLC SFXLibVLC = new LibVLC("--aout=directsound");

        public static LibVLCSharp.Shared.MediaPlayer GlobalMediaPlayer = new LibVLCSharp.Shared.MediaPlayer(GlobalLibVLC);
        public static LibVLCSharp.Shared.MediaPlayer SFXMediaPlayer = new LibVLCSharp.Shared.MediaPlayer(SFXLibVLC);

        public IAudioFileChapterData? CurrentChapter { get; set; }

        public int Volume { get; set; } = App.Globals.settings.Volume;
        public float PlaybackSpeed { get; set; } = App.Globals.settings.PlaybackSpeed;
        
        public bool IsMuted { get; set; }

        public bool IsTimelineIndicatorVisible { get; set; }

        private IAudiobook? _CurrentAudiobook;
        public IAudiobook? CurrentAudiobook {
            get { return _CurrentAudiobook; }
            set
            {
                if (_CurrentAudiobook != value)
                {
                    /* binding to events of current audiobook */
                    if (_CurrentAudiobook != null)
                    {
                        _CurrentAudiobook.ChangedChapters -= OnAudiobookChaptersChanged;
                        _CurrentAudiobook.ChangedBookMarks -= OnAudiobookBookmarksChanged;
                    }

                    if (value != null)
                    {
                        value.ChangedChapters += OnAudiobookChaptersChanged;
                        value.ChangedBookMarks += OnAudiobookBookmarksChanged;
                    }

                    _CurrentAudiobook = value;
                    Image_AudiobookIcon.GetBindingExpression(System.Windows.Controls.Image.SourceProperty).UpdateTarget();
                }
            }
        }

        public IAudioFile? CurrentAudioFile { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsPlaying { get; set; } = false;

        public int AudiobookProgressMaxSeconds { get; set; } = 1;
        public int AudiobookProgressSeconds { get; set; } = 0;

        private bool IsMouseDragging = false;

        /* for undo/redo action, need to cache position in audiobook before the first click on the timeline */
        private TimeSpan AudiobookPositionPreDrag = TimeSpan.FromSeconds(0);

        /* workaround for VLC being weird: 
         when initializing audio playback, you always have to call mediaPlayer.play() once to initialize things, even
        if playback should be paused at the start. However, if using the 'start-paused' option - which would, at the first call
        of mediaPlayer.play() pause the playback immediately, mediaPlayer.Time will always be reset to 0, which in turn 
        means that until the user presses play once, the position of the player is always at the beginning of the 
        current audiofile - definitely undesired effect (this also means that if the user never presses play, closes
        the application and reopens it, the audiobook will be reset to the beginning of the audiofile permanently and
        the position will be lost) 
        
         now one would assume the solution would be: call mediaPlayer.Play(), then mediaPlayer.Pause(),
        unfortunately while still buffering, any immediate call to mediaPlayer.Pause() after the initial
        call to mediaPlayer.Play() is ignored 
        
         VLC media player behaves weird when attempting to use while still buffering,
        unfortunately I can't find a way to check whether or not the player is currently buffering, there is
        however an event during buffering: GlobalMediaPlayer.Buffering
        
         as an ugly workaround I'm stopping and starting a timer 'StoppedBufferingTimer' within the buffering event,
        so if no buffering event is called within the timer's interval, I presume buffering is done, in which case 
        I set the playback position to the starting position again, then pause 
        
         UPDATE: using Media.StateChanged, it is possible to determine if buffering.
        Between call to 'opening' and call to 'playing', buffering is in progress*/
        private bool PauseAfterBuffering = false;
        private TimeSpan PreBufferTime = new TimeSpan();

        public bool IsMediaPlayerBuffering { get; set; } = false;

        /**/

        private DispatcherTimer ProgressTimer = new DispatcherTimer(DispatcherPriority.Render);

        public List<string> InputDisabledSources { get; set; }

        /**/

        public PlayerControls()
        {
            DataContext = this;

            InitializeComponent();
            ProgressTimer.Interval = TimeSpan.FromMilliseconds(250);

            InitializeComponent();

            InputDisabledSources = new List<string>();

            Unloaded += OnUnloaded;
            App.Globals.audiobookScanner.LoadedAudiobooks += OnLoadedAudiobooks;
            App.Globals.settings.LoadedSettings += LoadedSettings;

            GlobalMediaPlayer.SetRole(MediaPlayerRole.Music);
            SFXMediaPlayer.SetRole(MediaPlayerRole.Music);
        }

        ~PlayerControls()
        {
        }

        public void FinishedInit()
        {
        }

        void OnUnloaded(object sender, EventArgs e)
        {
            ProgressTimer.Tick -= UpdateProgressTick;
            ProgressTimer.Stop();

            App.Globals.settings.ChangeOption -= SettingsSetOption;
            GlobalMediaPlayer.EndReached -= MediaPlayer_MediaEnded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            /* new playercontrols were created, change reference in Application statics */
            App.Globals.playerControls = this;

            ProgressTimer.Start();
            ProgressTimer.Tick += UpdateProgressTick;

            GlobalMediaPlayer.EndReached += MediaPlayer_MediaEnded;

            App.Globals.settings.ChangeOption += SettingsSetOption;

            Timeline_CreateChapterMarkers();
            Timeline_CreateBookMarks();

            OpenDialog_FailedLoadedFiles();
        }

        public void SettingsSetOption(string identifier)
        {
            switch (identifier)
            {
                case "PlaybackSpeed":
                    PlaybackSpeed = App.Globals.settings.PlaybackSpeed;
                    GlobalMediaPlayer.SetRate(App.Globals.settings.PlaybackSpeed);
                    NotifyPropertyChanged("PlaybackSpeed");

                    if (OnPlaybackSpeedChange != null)
                        OnPlaybackSpeedChange.Invoke(this, new EventArgs());
                    break;

                case "Volume":
                    Volume = App.Globals.settings.Volume;
                    GlobalMediaPlayer.Volume = App.Globals.settings.Volume;
                    NotifyPropertyChanged("Volume");

                    if (OnChangeVolume != null)
                        OnChangeVolume.Invoke(this, new EventArgs());
                    break;
                    
                case "IsMuted":
                    IsMuted = App.Globals.settings.IsMuted;
                    GlobalMediaPlayer.Mute = IsMuted;
                    NotifyPropertyChanged("IsMuted");

                    if (OnIsMutedChange != null)
                        OnIsMutedChange.Invoke(this, new EventArgs());
                    break;

                default:
                    break;
            }
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void LoadedSettings()
        {
            SetVolume(App.Globals.settings.Volume);
            SetPlaybackSpeed(App.Globals.settings.PlaybackSpeed);
            SetIsMuted(App.Globals.settings.IsMuted);
        }

        public void OnLoadedAudiobooks()
        {
            /* restore last played audiobook from settings */
            PlayAudiobookByTitle(App.Globals.settings.PlayingAudiobookName, false);
        }

        void UpdateProgressTick(object sender, EventArgs e)
        {
            UpdateProgress();
        }

        void MediaPlayer_MediaEnded(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(requiredParameter => this.NextFile());
        }

        void MediaPlayer_StartedBuffering()
        {
            IsMediaPlayerBuffering = true;
        }

        void MediaPlayer_StoppedBuffering()
        {
            if (IsMediaPlayerBuffering)
            {
                IsMediaPlayerBuffering = false;

                ThreadPool.QueueUserWorkItem(requiredParameter =>
                {
                    if (CurrentAudiobook != null)
                    {
                        GlobalMediaPlayer.Time = (long)PreBufferTime.TotalMilliseconds;

                        if (PauseAfterBuffering)
                        {
                            PauseAfterBuffering = false;
                            Pause();
                        }

                        UpdateControlData();
                        UpdateTimelineIndicator();
                    }
                });
            }
        }

        public void UpdateControlData()
        {
            /* due to VLCSharp being a pain in the a**. Calling VLC related methods such as
             * player.Play(), Pause() or Stop() and apparently other things from within a VLC callback causes a deadlock in VLCSharp
             * and for the application to freeze. As a workaround I route all VLC event callbacks to a different thread 
             * if I need them to trigger anything. Unfortunately, this different thread causes an exception when it 
             * attempts to access some of the UI thread's values, such as the below textblocks' text properties.
             * Manually routing the VLC event callbacks to the UI thread still causes a deadlock, so 
             * the only solution I found was this: from VLC events, route to different thread, then manually
             * route the parts that cause exceptions back to UI thread via Dispatcher.Invoke 
             * 
             * Alternatively, I could propably use databinding here, but that's a pain in the ass here
             * for other reasons - no comment */
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                if (CurrentAudiobook != null)
                {
                    var timeSpan = CurrentAudiobook.FullDuration;
                    string fullDuration = String.Format("{0}:{1:00}:{2:00}", Math.Truncate(timeSpan.TotalHours), timeSpan.Minutes, timeSpan.Seconds);

                    //MediaControlTextDuration.Text = fullDuration;

                    if (CurrentAudiobook.Chapters.Count < 1)
                        ChapterDisplay.Visibility = Visibility.Collapsed;
                    else
                    {
                        ChapterDisplay.Visibility = Visibility.Visible;
                    }

                    UpdateMaxProgress();

                    if (OnControlDataUpdate != null)
                        OnControlDataUpdate.Invoke(this, new EventArgs());
                }
                else
                {
                    MediaControlTextTimePlayed.Text = "";

                    ChapterDisplay.Visibility = Visibility.Collapsed;

                    AudiobookProgressMaxSeconds = 1;
                    AudiobookProgressSeconds = 0;

                    NotifyPropertyChanged("AudiobookProgressMaxSeconds");
                    NotifyPropertyChanged("AudiobookProgressSeconds");

                    Timeline_RemoveChapterMarkers();
                    Timeline_RemoveBookMarks();
                }

                UpdateProgress();
            }));
        }

        public void UpdateMaxProgress()
        {
            AudiobookProgressMaxSeconds = (int)CurrentAudiobook.FullDuration.TotalSeconds;

            NotifyPropertyChanged("AudiobookProgressMaxSeconds");
        }

        public void UpdateTimelineIndicator()
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                if (CurrentAudiobook == null)
                    return;

                double maxWidth = Timeline.ActualWidth;
                // get mousepos relative to Timeline
                Point mousePos = Mouse.GetPosition(Timeline);
                Point newIndicatorPos = mousePos;

                // translate mouse pos to relative TimelineIndicator pos
                newIndicatorPos = Timeline.TranslatePoint(mousePos, TimelineIndicator);

                newIndicatorPos = TimelineIndicator.RenderTransform.Transform(newIndicatorPos);
                newIndicatorPos.X -= TimelineIndicator.ActualWidth / 2.0;

                double xMin = -(maxWidth / 2.0) + TimelineIndicator.ActualWidth / 2.0;
                if (newIndicatorPos.X < xMin)
                    newIndicatorPos.X = xMin;
                double xMax = (maxWidth / 2.0) - TimelineIndicator.ActualWidth / 2.0;
                if (newIndicatorPos.X > xMax)
                    newIndicatorPos.X = xMax;

                TimelineIndicator.RenderTransform = new TranslateTransform(newIndicatorPos.X, 0);

                double maximum = CurrentAudiobook.FullDuration.TotalSeconds;

                double factor = mousePos.X / maxWidth;
                int newPositionSeconds = (int)(maximum * factor);

                TimeSpan currPosTimeSpan = TimeSpan.FromSeconds(newPositionSeconds);
                IAudioFileChapterData currPosChapter = CurrentAudiobook.GetChapterAtDuration(currPosTimeSpan);

                TimelineIndicator.SetTimelinePosition(currPosTimeSpan);
                TimelineIndicator.SetChapterName(currPosChapter != null ? currPosChapter.Title : null);
            }));
        }

        public void UpdateProgress()
        {
            if (CurrentAudiobook != null)
            {
                IAudioFile currentFile = CurrentAudiobook.GetCurrentAudioFile();

                if (currentFile != null)
                {
                    /* try catch due to multithreading weirdness. This wouldn't be an issue
                    if VLC sharp weren't buggy as hell. Calling VLC related methods from within a callback called by
                    VLC sharp itself, causes a deadlock. VLC's callbacks work on the UI thread and as a workaround I 
                    route them away to a separate thread, which fixes the deadlock but causes immense
                    pains in the behind if attempting to access values that are used on the UI thread, such as
                    CurrentAudiobook, especially if these values are checked via polling such as UpdateProgress 
            
                     due to a racecondition I have no deeper understanding of, CurrentAudiobook consistently changes to null
                    inbetween the if check and the access to it in the try block, probably due to being accessed
                    from another thread called from a VLC callback (somewhere from MediaEnded)
                    
                     I see no harm in just catching the exception and being done with this, so here we go */
                    try
                    {
                        CurrentAudiobook.AudioFilePosition = TimeSpan.FromMilliseconds(GlobalMediaPlayer.Time);
                        AudiobookProgressSeconds = (int)CurrentAudiobook.AudiobookPosition.TotalSeconds;

                        NotifyPropertyChanged("AudiobookProgressSeconds");
                        NotifyPropertyChanged("Played");

                        var fullDuration = CurrentAudiobook.FullDuration;
                        string sFullDuration = String.Format("{0}:{1:00}:{2:00}", Math.Truncate(fullDuration.TotalHours), fullDuration.Minutes, fullDuration.Seconds);

                        string currentPosition = CurrentAudiobook.AudiobookPosition.ToString(@"hh\:mm\:ss");
                        MediaControlTextTimePlayed.Text = currentPosition + " / " + sFullDuration;

                        var chapter = CurrentAudiobook.GetChapterAtDuration(CurrentAudiobook.AudiobookPosition);
                        if (chapter != null)
                        {
                            if (CurrentChapter == null || chapter.ChapterIdx != CurrentChapter.ChapterIdx)
                            {
                                CurrentChapter = chapter;
                                NotifyPropertyChanged("CurrentChapter");

                                if (OnChangeChapter != null)
                                    OnChangeChapter.Invoke(this, new EventArgs());
                            }
                        }

                        if (OnTimelineUpdate != null)
                            OnTimelineUpdate.Invoke(this, new EventArgs());
                    }
                    catch (NullReferenceException e)
                    {
                    }
                }
            }
        }

        public void ApplyBookMark(IBookMark bookMark)
        {
            if (CurrentAudiobook != bookMark.AssociatedAudiobook)
            {
                PlayAudiobook(bookMark.AssociatedAudiobook);
            }

            PushUndoRedoActionBookMark(bookMark, AudiobookPositionPreDrag, GetCurrentAudiobookPosition());
            SetAudiobookToPosition(bookMark.Position);

            App.Globals.textToSpeech.Speak("Bookmark: " + bookMark.Identifier);
        }

        public void RemoveBookMark(IBookMark bookMark)
        {
            if (CurrentAudiobook != null)
            {
                if (CurrentAudiobook.RemoveBookMark(bookMark))
                {
                    Timeline_CreateBookMarks();
                }
            }
        }

        public void SetAudiobookToPosition(TimeSpan duration)
        {
            if (CurrentAudiobook != null)
            {
                IAudioFile audioFile = CurrentAudiobook.GetFileAtDuration(duration);
                if (audioFile == null)
                    return;

                CurrentChapter = CurrentAudiobook.GetChapterAtDuration(duration);
                NotifyPropertyChanged("CurrentChapter");

                if (OnChangeChapter != null)
                    OnChangeChapter.Invoke(this, new EventArgs());

                /* get timespan within current audiofile, as opposed to timespan within full audiobook */
                TimeSpan localDuration = CurrentAudiobook.GetLocalDurationFromTotal(duration, audioFile);

                if (audioFile != CurrentAudiobook.GetCurrentAudioFile())
                {
                    PlayAudioFile(audioFile, localDuration, IsPlaying);
                }
                else
                {
                    GlobalMediaPlayer.Time = (long)localDuration.TotalMilliseconds;
                }

                CurrentAudiobook.AudiobookPosition = duration;
                UpdateProgress();
            }
        }

        public void PlayAudiobook(IAudiobook audiobook, bool startPlaying = true, bool isUndoRedo = false)
        {
            if (audiobook == null)
            {
                Stop();
                return;
            }

            if ( (CurrentAudiobook == null || CurrentAudiobook != audiobook) && audiobook.AudioFiles.Count > 0)
            {
                IAudiobook previousAudiobook = CurrentAudiobook;

                if (!isUndoRedo && previousAudiobook != null)
                    PushUndoRedoActionAudiobookSwitch(previousAudiobook, audiobook);

                Stop();

                CurrentAudiobook = audiobook;
                CurrentAudiobook.IsPlaying = true;

                NotifyPropertyChanged("CurrentAudiobook");

                App.Globals.settings.PlayingAudiobookName = CurrentAudiobook.Title;
                App.Globals.settings.AddLastPlayedAudiobook(CurrentAudiobook);

                CurrentAudiobook.LastPlayedDate = DateTime.Now;

                IAudioFile audioFile = audiobook.GetCurrentAudioFile();

                TimeSpan position = CurrentAudiobook.AudioFilePosition;
                if (position >= audiobook.FullDuration)
                    position = TimeSpan.FromSeconds(0);

                OnPlayedAudiobook(CurrentAudiobook);

                CurrentChapter = CurrentAudiobook.GetChapterAtDuration(position);
                NotifyPropertyChanged("CurrentChapter");
                if (OnChangeChapter != null)
                    OnChangeChapter.Invoke(this, new EventArgs());

                PlayAudioFile(audioFile, position, startPlaying);

                UpdateControlData();
                UpdateProgress();

                Timeline_CreateChapterMarkers();
                Timeline_CreateBookMarks();

                if (CurrentAudiobook != null)
                {
                    if (!startPlaying)
                        App.Globals.textToSpeech.Speak("Playing audiobook: " + CurrentAudiobook.Title + " - paused.");
                    else
                        App.Globals.textToSpeech.Speak("Playing audiobook: " + CurrentAudiobook.Title);
                }
            }
        }

        public void PlayAudiobookByTitle(string title, bool startPlaying = true)
        {
            if (title == null)
                return;

            IAudiobook audiobook = App.Globals.audiobookScanner.AudioBookList.FirstOrDefault(item => item.Title == title);

            if (audiobook != null)
            {
                PlayAudiobook(audiobook, startPlaying);
            }
        }

        public void PlaySFXAudioFile(string filePath, int volume)
        {
            if (!File.Exists(filePath))
            {
                DebugLog.WriteLine("info: PlaySFXAudioFile file is null");

                SFXMediaPlayer.Stop();
                return;
            }

            SFXMediaPlayer.Media = new LibVLCSharp.Shared.Media(SFXLibVLC, new Uri(filePath));
            SFXMediaPlayer.Media.AddOption(":no-video");
            SFXMediaPlayer.Media.AddOption(":audio-output=waveout");

            SFXMediaPlayer.Play();
            SFXMediaPlayer.Volume = volume;

            DebugLog.WriteLine("info: PlaySFXAudioFile, playing file: '" + filePath + "'");
        }

        public void PlayAudioFile(IAudioFile file, TimeSpan position, bool startPlaying = true)
        {
            if (file != null && GlobalMediaPlayer != null)
            {
                if (!File.Exists(file.AudioFilePath))
                {
                    DebugLog.WriteLine("Warning: File couldn't be found at '" + file.AudioFilePath + "'");
                    Stop();
                    return;
                }

                if (position >= file.Duration)
                    position = TimeSpan.FromSeconds(0);

                GlobalMediaPlayer.Media = new LibVLCSharp.Shared.Media(GlobalLibVLC, new Uri(file.AudioFilePath));
                GlobalMediaPlayer.Media.StateChanged += Media_StateChanged;
                GlobalMediaPlayer.Media.AddOption(":no-video");
                GlobalMediaPlayer.Media.AddOption(":audio-output=waveout");

                if (CurrentAudiobook != null)
                {
                    CurrentAudiobook.SetAudioFile(file);
                }

                GlobalMediaPlayer.Time = (long)position.TotalMilliseconds;
                PreBufferTime = position;
                GlobalMediaPlayer.Play();
                GlobalMediaPlayer.Mute = App.Globals.settings.IsMuted;
                GlobalMediaPlayer.Volume = App.Globals.settings.Volume;
                IsMediaPlayerBuffering = true;

                if (CurrentAudioFile != null)
                    CurrentAudioFile.IsPlaying = false;
                CurrentAudioFile = file;
                CurrentAudioFile.IsPlaying = true;

                //CurrentChapter = CurrentAudiobook.GetChapterAtDuration(TimeSpan.FromSeconds(0));

                if (OnChangeChapter != null)
                    OnChangeChapter.Invoke(this, new EventArgs());

                OnPlayedAudioFile(CurrentAudioFile);

                if (!startPlaying)
                {
                    PauseAfterBuffering = true;
                }
                else
                {
                    Play();
                }
            }
            else if (GlobalMediaPlayer != null)
            {
                Stop();
            }
        }

        private void Media_StateChanged(object? sender, MediaStateChangedEventArgs e)
        {
            if (e.State == VLCState.Opening)
            {
                MediaPlayer_StartedBuffering();
            }
            else if (e.State == VLCState.Playing)
            {
                MediaPlayer_StoppedBuffering();
            }
        }

        public void PlayPause()
        {
            if (IsPlaying)
                Pause();
            else
                Play();
        }

        public void Play()
        {
            if (GlobalMediaPlayer != null && CurrentAudiobook != null)
            {
                // reset volume in case sleep timer or something else messed with it
                GlobalMediaPlayer.Volume = App.Globals.settings.Volume;
                GlobalMediaPlayer.Play();

                IsPlaying = true;
                NotifyPropertyChanged("IsPlaying");

                if (OnPlay != null)
                    OnPlay.Invoke(this, new EventArgs());
            }
        }

        public void Pause()
        {
            if (GlobalMediaPlayer != null && CurrentAudiobook != null)
            {
                if (GlobalMediaPlayer.IsPlaying)
                    GlobalMediaPlayer.Pause();

                IsPlaying = false;

                NotifyPropertyChanged("IsPlaying");

                if (OnPause != null)
                    OnPause.Invoke(this, new EventArgs());
            }
        }

        public void Stop()
        {
            if (GlobalMediaPlayer != null && CurrentAudiobook != null)
            {
                CurrentAudiobook.IsPlaying = false;

                CurrentAudiobook.LastPlayedDate = DateTime.Now;

                GlobalMediaPlayer.Stop();
                CurrentAudiobook = null;
                CurrentChapter = null;
                IsPlaying = false;

                App.Globals.settings.PlayingAudiobookName = "";

                NotifyPropertyChanged("CurrentAudiobook");
                NotifyPropertyChanged("CurrentChapter");
                if (OnChangeChapter != null)
                    OnChangeChapter.Invoke(this, new EventArgs());

                UpdateControlData();

                NotifyPropertyChanged("IsPlaying");

                Timeline_RemoveChapterMarkers();
                Timeline_RemoveBookMarks();

                if (OnStop != null)
                    OnStop.Invoke(this, new EventArgs());
            }
        }

        public void NextChapter()
        {
            if (GlobalMediaPlayer != null && CurrentAudiobook != null &&
                CurrentChapter != null && CurrentAudiobook.Chapters.Count > 0)
            {
                if (CurrentAudiobook.Chapters.Count > CurrentChapter.ChapterIdx + 1)
                {
                    CurrentChapter = CurrentAudiobook.Chapters[CurrentChapter.ChapterIdx + 1];
                    var chapterTMP = CurrentChapter;

                    SetAudiobookToPosition(TimeSpan.FromMilliseconds(CurrentChapter.StartTime));

                    if (CurrentChapter.Title.Length > 0)
                        App.Globals.textToSpeech.Speak("Playing: " + CurrentChapter.Title);
                    else
                        App.Globals.textToSpeech.Speak("Playing: Chapter " + CurrentChapter.ChapterIdx);
                }
            }
        }

        public void PreviousChapter()
        {
            if (GlobalMediaPlayer != null && CurrentAudiobook != null &&
                CurrentChapter != null && CurrentAudiobook.Chapters.Count > 0)
            {
                TimeSpan currentStartTime = TimeSpan.FromMilliseconds(CurrentChapter.StartTime);

                if (CurrentAudiobook.AudiobookPosition -
                    currentStartTime >
                    TimeSpan.FromSeconds(5))
                {
                    SetAudiobookToPosition(currentStartTime);
                }
                else
                {
                    if (CurrentChapter.ChapterIdx - 1 >= 0)
                    {
                        CurrentChapter = CurrentAudiobook.Chapters[CurrentChapter.ChapterIdx - 1];
                        SetAudiobookToPosition(TimeSpan.FromMilliseconds(CurrentChapter.StartTime));
                    }
                    else
                        SetAudiobookToPosition(TimeSpan.Zero);
                }

                if (CurrentChapter.Title.Length > 0)
                    App.Globals.textToSpeech.Speak("Playing: " + CurrentChapter.Title);
                else
                    App.Globals.textToSpeech.Speak("Playing: Chapter " + CurrentChapter.ChapterIdx);
            }
        }

        public void NextFile()
        {
            if (GlobalMediaPlayer != null && CurrentAudiobook != null)
            {
                if (CurrentAudiobook.NextFile())
                {
                    PlayAudioFile(CurrentAudiobook.GetCurrentAudioFile(), TimeSpan.FromSeconds(0));
                }
                else
                {
                    CurrentAudiobook.MarkRead();

                    Application.Current.Dispatcher.Invoke(new Action(() =>
                    {
                        Stop();
                    }));

                    OnFinishedAudiobook(CurrentAudiobook);
                }
            }
        }

        public void PreviousFile()
        {
            if (GlobalMediaPlayer != null && CurrentAudiobook != null)
            {
                if (CurrentAudiobook.PreviousFile())
                {
                    PlayAudioFile(CurrentAudiobook.GetCurrentAudioFile(), TimeSpan.FromSeconds(0));
                }
            }
        }

        public void RewindBy(TimeSpan rewindAmount)
        {
            if (CurrentAudiobook != null && !IsMediaPlayerBuffering && App.Globals.settings != null)
            {
                TimeSpan pos = TimeSpan.FromSeconds(CurrentAudiobook.AudiobookPosition.TotalSeconds - rewindAmount.TotalSeconds);

                if (pos.TotalSeconds <= 0)
                    pos = TimeSpan.FromSeconds(0);

                SetAudiobookToPosition(pos);
            }
        }

        public void RewindBy(int rewindSeconds)
        {
            RewindBy(TimeSpan.FromSeconds(rewindSeconds));
        }

        public void Rewind()
        {
            RewindBy(App.Globals.settings.ForwardBackwardIntervalSeconds);
        }

        public void Forward()
        {
            if (CurrentAudiobook != null && App.Globals.settings != null)
            {
                UpdateProgress();

                TimeSpan pos = TimeSpan.FromSeconds(AudiobookProgressSeconds + App.Globals.settings.ForwardBackwardIntervalSeconds);
                if (pos > CurrentAudiobook.FullDuration)
                    pos = CurrentAudiobook.FullDuration;

                SetAudiobookToPosition(pos);
            }
        }

        private void ClickPlay(object sender, RoutedEventArgs e)
        {
            Play();
        }

        private void ClickPause(object sender, RoutedEventArgs e)
        {
            Pause();
        }

        private void ClickPlayPause(object sender, RoutedEventArgs e)
        {
            PlayPause();
        }

        private void ClickPrevious(object sender, RoutedEventArgs e)
        {
            AudiobookPositionPreDrag = GetCurrentAudiobookPosition();

            PreviousChapter();
            PushUndoRedoActionTimeline(AudiobookPositionPreDrag, GetCurrentAudiobookPosition());
        }

        private void ClickNext(object sender, RoutedEventArgs e)
        {
            AudiobookPositionPreDrag = GetCurrentAudiobookPosition();
            
            NextChapter();
            PushUndoRedoActionTimeline(AudiobookPositionPreDrag, GetCurrentAudiobookPosition());
        }

        private void ClickRewind(object sender, RoutedEventArgs e)
        {
            Rewind();
        }

        private void ClickForward(object sender, RoutedEventArgs e)
        {
            Forward();
        }

        private void GlobalPlayerControls_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        public void SetVolume(int value)
        {
            if (GlobalMediaPlayer != null)
            {
                if (value > 100)
                    value = 100;
                else if (value < 0)
                    value = 0;

                Volume = value;
                GlobalMediaPlayer.Volume = value;
                App.Globals.settings.Volume = value;
                NotifyPropertyChanged("Volume");

                if (OnChangeVolume != null)
                    OnChangeVolume.Invoke(this, new EventArgs());
            }
        }

        private void VolumeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (GlobalMediaPlayer != null)
            {
                int value = (int)VolumeSlider.Value;
                SetVolume(value);
            }
        }

        /**/

        public int GetCurrentAudiobookPositionSeconds()
        {
            return (int)GetCurrentAudiobookPosition().TotalSeconds;
        }

        public TimeSpan GetCurrentAudiobookPosition()
        {
            if (CurrentAudiobook == null)
                return TimeSpan.FromSeconds(-1);

            return CurrentAudiobook.GetTotalDurationFromLocal(
                TimeSpan.FromMilliseconds(GlobalMediaPlayer.Time), 
                CurrentAudiobook.GetCurrentAudioFile());
        }

        private void Timeline_RemoveChapterMarkers()
        {
            TimelineContainer.Children.Clear();
        }

        private void Timeline_CreateChapterMarkers()
        {
            Timeline_RemoveChapterMarkers();

            if (CurrentAudiobook == null || CurrentAudiobook.Chapters.Count == 0 || CurrentAudiobook.AudioFiles.Count == 0)
                return;

            double timelineWidth = Timeline.ActualWidth;
            double durationMs = CurrentAudiobook.FullDuration.TotalMilliseconds;

            Brush recBrush = (Brush) App.Current.TryFindResource("Style_TimelineChapterMarkerColor");

            for (int i = 0; i < CurrentAudiobook.Chapters.Count; i++)
            {
                var chapter = CurrentAudiobook.Chapters[i];

                double percentage = chapter.StartTime / durationMs;
                double posX = percentage * timelineWidth;

                Rectangle rec = new Rectangle()
                {
                    Width = 2,
                    Height = Timeline.ActualHeight + 5,
                    Fill = recBrush,
                    IsHitTestVisible = false,
                };

                TimelineContainer.Children.Add(rec);
                Canvas.SetLeft(rec, posX - rec.Width / 2);
            }
        }

        private void Timeline_CreateBookMarks()
        {
            Timeline_RemoveBookMarks();

            if (CurrentAudiobook == null || CurrentAudiobook.BookMarks.Count == 0 || CurrentAudiobook.AudioFiles.Count == 0)
                return;

            double timelineWidth = Timeline.ActualWidth;
            double durationMs = CurrentAudiobook.FullDuration.TotalMilliseconds;

            for (int i = 0; i < CurrentAudiobook.BookMarks.Count; i++)
            {
                var bookMark = CurrentAudiobook.BookMarks[i];

                double percentage = bookMark.Position.TotalMilliseconds / durationMs;
                double posX = percentage * timelineWidth;

                TimelineBookMark marker = new TimelineBookMark(bookMark);

                TimelineBookMarks.Children.Add(marker);
                Canvas.SetLeft(marker, posX - marker.Width / 2);
                Canvas.SetBottom(marker, 15);
            }

            if (OnUpdateBookMarks != null)
                OnUpdateBookMarks.Invoke(this, new EventArgs());
        }

        private void Timeline_UpdateBookMarkToolTips()
        {
            foreach (TimelineBookMark bookMarkElem in TimelineBookMarks.Children)
            {
                if (bookMarkElem != null)
                {
                    bookMarkElem.NotifyPropertyChanged("AssociatedBookMark");
                }
            }

            if (OnUpdateBookMarkToolTips != null)
                OnUpdateBookMarkToolTips.Invoke(this, new EventArgs());
        }

        private void Timeline_RemoveBookMarks()
        {
            TimelineBookMarks.Children.Clear();
        }

        private void Timeline_CreateBookMark(IBookMark bookMark)
        {
        }

        private void Timeline_RemoveBookMark(IBookMark bookMark)
        {
        }

        private void Timeline_MouseLeave(object sender, MouseEventArgs e)
        {
            IsTimelineIndicatorVisible = false;
            NotifyPropertyChanged("IsTimelineIndicatorVisible");

            if (IsMouseDragging)
            {
                IsMouseDragging = false;
                PushUndoRedoActionTimeline(AudiobookPositionPreDrag, GetCurrentAudiobookPosition());
            }
        }

        private void Timeline_MouseDown(object sender, MouseEventArgs e)
        {
            IsMouseDragging = true;

            AudiobookPositionPreDrag = GetCurrentAudiobookPosition();

            Timeline_MouseMove(sender, e);
        }

        private void Timeline_MouseMove(object sender, MouseEventArgs e)
        {
            UpdateTimelineIndicator();

            if (!IsMouseDragging || e.LeftButton != MouseButtonState.Pressed || CurrentAudiobook == null || IsMediaPlayerBuffering)
                return;

            double MousePosition = e.GetPosition(Timeline).X;
            double width = Timeline.ActualWidth;
            double maximum = CurrentAudiobook.FullDuration.TotalSeconds;

            double factor = MousePosition / width;

            int newPositionSeconds = (int)(maximum * factor);

            SetAudiobookToPosition(TimeSpan.FromSeconds(newPositionSeconds));
        }

        private void Timeline_MouseUp(object sender, MouseButtonEventArgs e)
        {
            PushUndoRedoActionTimeline(AudiobookPositionPreDrag, GetCurrentAudiobookPosition());

            IsMouseDragging = false;
        }

        private void Timeline_MouseEnter(object sender, MouseEventArgs e)
        {
            UpdateTimelineIndicator();

            if (CurrentAudiobook != null)
            {
                IsTimelineIndicatorVisible = true;
                NotifyPropertyChanged("IsTimelineIndicatorVisible");
            }
        }

        public void SetIsMuted(bool isMuted)
        {
            IsMuted = isMuted;
            GlobalMediaPlayer.Mute = isMuted;
            App.Globals.settings.IsMuted = isMuted;

            NotifyPropertyChanged("IsMuted");

            if (OnIsMutedChange != null)
                OnIsMutedChange.Invoke(this, new EventArgs());
        }

        public void SetPlaybackSpeed(float rate)
        {
            App.Globals.settings.PlaybackSpeed = rate;
            PlaybackSpeed = rate;
            GlobalMediaPlayer.SetRate(rate);

            NotifyPropertyChanged("PlaybackSpeed");

            if (OnPlaybackSpeedChange != null)
                OnPlaybackSpeedChange.Invoke(this, new EventArgs());
        }

        private void PushUndoRedoActionAudiobookSwitch(IAudiobook previous, IAudiobook next)
        {
            IUndoRedoAction action = new UndoRedoAction(
                (IUndoRedo undoRedo, IUndoRedoAction action) =>
                {
                    if (!IsMediaPlayerBuffering)
                    {
                        PlayAudiobook(previous, false, true);
                        return true;
                    }
                    return false;
                },
                (IUndoRedo undoRedo, IUndoRedoAction action) =>
                {
                    if (!IsMediaPlayerBuffering)
                    {
                        PlayAudiobook(next, false, true);
                        return true;
                    }
                    return false;
                },
                CurrentAudiobook.Title);

            App.Globals.undoRedo.PushAction(action);
        }

        private void PushUndoRedoActionBookMark(IBookMark mark, TimeSpan oldPosition, TimeSpan newPosition)
        {
            if (CurrentAudiobook == null)
                return;

            /* if the difference in timeline is smaller than 2 seconds, no new undo/redo action */
            if (Math.Abs(newPosition.TotalSeconds - oldPosition.TotalSeconds) < 2)
                return;

            /* uses audiobook title as identifier for undo/redo action, so all actions of this 
             audiobook can later easily be removed */
            IUndoRedoAction action = new UndoRedoAction(
                (IUndoRedo undoRedo, IUndoRedoAction action) =>
                {
                    return TimelineUndo(action, oldPosition);
                },
                (IUndoRedo undoRedo, IUndoRedoAction action) =>
                {
                    return TimelineUndo(action, newPosition);
                },
                CurrentAudiobook.Title);

            App.Globals.undoRedo.PushAction(action);
        }

        public void PushUndoRedoActionTimeline(TimeSpan oldPosition, TimeSpan newPosition)
        {
            if (CurrentAudiobook == null)
                return;

            /* if the difference in timeline is smaller than 2 seconds, no new undo/redo action */
            if (Math.Abs(newPosition.TotalSeconds - oldPosition.TotalSeconds) < 2)
                return;

            /* uses audiobook title as identifier for undo/redo action, so all actions of this 
             audiobook can later easily be removed */
            IUndoRedoAction action = new UndoRedoAction(
                (IUndoRedo undoRedo, IUndoRedoAction action) =>
                {
                    return TimelineUndo(action, oldPosition);
                },
                (IUndoRedo undoRedo, IUndoRedoAction action) =>
                {
                    return TimelineUndo(action, newPosition);
                },
                CurrentAudiobook.Title);

            App.Globals.undoRedo.PushAction(action);
        }

        public bool TimelineUndo(IUndoRedoAction action, TimeSpan time)
        {
            TimeSpan cachedTime = GetCurrentAudiobookPosition();
            action.RedoAction =
            (IUndoRedo undoRedo, IUndoRedoAction action) =>
            {
                return TimelineRedo(action, cachedTime);
            };

            if (action.Identifier != CurrentAudiobook.Title)
                PlayAudiobookByTitle(action.Identifier);

            SetAudiobookToPosition(time);
            return true;
        }

        public bool TimelineRedo(IUndoRedoAction action, TimeSpan time)
        {
            TimeSpan cachedTime = GetCurrentAudiobookPosition();
                action.UndoAction =
            (IUndoRedo undoRedo, IUndoRedoAction action) =>
            {
                return TimelineUndo(action, cachedTime);
            };

            if (action.Identifier != CurrentAudiobook.Title)
                PlayAudiobookByTitle(action.Identifier);

            SetAudiobookToPosition(time);
            return true;
        }

        public void SetInputEnabled(string source = "")
        {
            InputDisabledSources.Remove(source.ToLower());
        }

        public void SetInputDisabled(string source = "")
        {
            if (!InputDisabledSources.Contains(source.ToLower()))
                InputDisabledSources.Add(source.ToLower());
        }

        public void ClearInputDisabled()
        {
            InputDisabledSources.Clear();
        }

        public bool IsInputEnabled()
        {
            return InputDisabledSources.Count == 0;
        }

        public void OpenNewBookMarkDialog(DependencyObject sender)
        {
            if (CurrentAudiobook == null)
                return;

            Dialog_NewBookMark newWindow = new Dialog_NewBookMark(CurrentAudiobook);
            Window parentWindow = Window.GetWindow(sender);
            newWindow.Owner = parentWindow;

            TimeSpan pos = GetCurrentAudiobookPosition();

            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_newbookmark");

            // check for null again because during ShowDialog, CurrentAudiobook could become empty
            if (newWindow.ShowDialog() == true && CurrentAudiobook != null)
            {
                CurrentAudiobook.AddBookMark(newWindow.BookMarkNameText, pos);

                Timeline_CreateBookMarks();
            }

            controls.SetInputEnabled("menu_newbookmark");
        }

        public double OpenPlaybackSpeedDialog(DependencyObject sender)
        {
            Dialog_SetPlaybackSpeed newWindow = new Dialog_SetPlaybackSpeed(PlaybackSpeed);
            Window parentWindow = Window.GetWindow(sender);
            newWindow.Owner = parentWindow;

            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_playbackspeed");

            double newPlaybackSpeed = PlaybackSpeed;
            if (newWindow.ShowDialog() == true)
            {
                newPlaybackSpeed = newWindow.NewSpeed;
            }

            controls.SetInputEnabled("menu_playbackspeed");

            return newPlaybackSpeed;
        }

        Dialog_FailedLoadedFiles FailedFilesDiag;
        public void OpenDialog_FailedLoadedFiles()
        {
            IAudiobookScanner scanner = App.Globals.audiobookScanner;
            if (scanner == null)
            {
                DebugLog.WriteLine("Warning: cannot open 'FailedLoadedFiles' dialog because global AudiobookScanner ref is null");
            }
            else if (scanner.FilesFailedToLoad.Count > 0)
            {
                List<IAudioFile> fileListCopy = new List<IAudioFile>();
                for (int i = 0; i < scanner.FilesFailedToLoad.Count; i++)
                    fileListCopy.Add(scanner.FilesFailedToLoad[i]);

                FailedFilesDiag = new Dialog_FailedLoadedFiles(fileListCopy);
                Window parentWindow = Window.GetWindow(this);
                FailedFilesDiag.Owner = parentWindow;

                scanner.FilesFailedToLoad.Clear();

                FailedFilesDiag.Show();
            }
        }

        public void ToggleSleepTimerDialog(DependencyObject sender)
        {
            if (IsSleepTimerWindowOpen)
                CloseSleepTimerDialog();
            else
                OpenSleepTimerDialog(sender);
        }

        private bool IsSleepTimerWindowOpen;
        private Dialog_SleepTimer SleepTimerWindow;

        public void OpenSleepTimerDialog(DependencyObject sender)
        {
            if (IsSleepTimerWindowOpen)
                CloseSleepTimerDialog();

            SleepTimerWindow = new Dialog_SleepTimer();
            Window parentWindow = Window.GetWindow(sender);
            SleepTimerWindow.Owner = parentWindow;

            SleepTimerWindow.Show();

            IsSleepTimerWindowOpen = true;
        }

        public void CloseSleepTimerDialog()
        {
            if (IsSleepTimerWindowOpen) 
            {
                SleepTimerWindow.Close();
                IsSleepTimerWindowOpen = false;
            }
        }

        public void OpenManageBookMarksDialog(DependencyObject sender)
        {
            if (CurrentAudiobook == null)
                return;

            IAudiobook audiobook = CurrentAudiobook;

            Dialog_ManageBookMarks newWindow = new Dialog_ManageBookMarks(CurrentAudiobook);
            Window parentWindow = Window.GetWindow(sender);
            newWindow.Owner = parentWindow;

            var controls = App.Globals.playerControls;
            controls.SetInputDisabled("menu_managebookmarks");

            if (newWindow.ShowDialog() == true && audiobook.BookMarks.Count != newWindow.TempBookMarks.Count)
            {
                audiobook.BookMarks = newWindow.TempBookMarks;

                if (OnUpdateBookMarks != null)
                    OnUpdateBookMarks.Invoke(sender, new EventArgs());
            }

            Timeline_UpdateBookMarkToolTips();

            controls.SetInputEnabled("menu_managebookmarks");
        }

        public void OpenAudiobookDetails()
        {
            if (CurrentAudiobook == null)
                return;

            AudiobookDetails audiobookDetails = new AudiobookDetails(CurrentAudiobook);

            var window = App.Current.MainWindow as NavigationWindow;
            window.NavigationService.Navigate(audiobookDetails);
        }

        private void Button_PlaybackSpeed_Click(object sender, RoutedEventArgs e)
        {
            SetPlaybackSpeed((float)OpenPlaybackSpeedDialog(this));
        }

        private void Button_SleepTimer_Click(object sender, RoutedEventArgs e)
        {
            ToggleSleepTimerDialog(this);
        }

        private void Click_BookMark(object sender, RoutedEventArgs e)
        {
            OpenBookMarContextMenu();
        }

        public void OpenBookMarContextMenu()
        {
            ContextMenu contextMenu = Button_BookMark.ContextMenu;
            contextMenu.PlacementTarget = Button_BookMark;
            contextMenu.IsOpen = true;
        }

        private void Click_NewBookMark(object sender, RoutedEventArgs e)
        {
            OpenNewBookMarkDialog(this);
        }

        private void Click_ManageBookMarks(object sender, RoutedEventArgs e)
        {
            OpenManageBookMarksDialog(this);
        }

        private void ButtonMute_Click(object sender, RoutedEventArgs e)
        {
            bool newMuteState = !IsMuted;
            SetIsMuted(newMuteState);
        }

        private void Title_Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentAudiobook != null)
                OpenAudiobookDetails();
        }

        private void Icon_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                ShowLargeIconDialog();
        }

        public void ShowLargeIconDialog()
        {
            if (CurrentAudiobook == null || !CurrentAudiobook.HasIcon() || Dialog_Image.IsOpen)
                return;

            Dialog_Image newWindow = new Dialog_Image(CurrentAudiobook.IconPath);
            Window parentWindow = Window.GetWindow(this);
            newWindow.Owner = parentWindow;

            newWindow.Show();
        }

        public void ShowSetIconDialog()
        {
            if (CurrentAudiobook != null)
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "All Images Files (*.png;*.jpeg;*.gif;*.jpg;*.bmp;*.tiff;*.tif;*.mp3;*.mp4;*.m4b;*.m4a)|*.png;*.jpeg;*.gif;*.jpg;*.bmp;*.tiff;*.tif;*.mp3;*.mp4;*.m4b;*.m4a;" +
                "|PNG Portable Network Graphics (*.png)|*.png" +
                "|JPEG File Interchange Format (*.jpg *.jpeg *jfif)|*.jpg;*.jpeg;*.jfif" +
                "|BMP Windows Bitmap (*.bmp)|*.bmp" +
                "|TIF Tagged Imaged File Format (*.tif *.tiff)|*.tif;*.tiff" +
                "|GIF Graphics Interchange Format (*.gif)|*.gif" +
                "|Embedded audio file covers (*.mp3;*.mp4;*.m4b;*.m4a)|*.mp3;*.mp4;*.m4b;*.m4a" +
                "|All Files|*";

                if (openFileDialog.ShowDialog() == true)
                {
                    CurrentAudiobook.IconPath = openFileDialog.FileName;

                    /* allow one autosave */
                    if (App.Globals.audiobookScanner != null)
                        App.Globals.audiobookScanner.MarkChanged();
                }
            }
        }

        public event EventHandler OnChangeVolume;
        public event EventHandler OnPlay;
        public event EventHandler OnPause;
        public event EventHandler OnStop;
        public event EventHandler OnTimelineUpdate;
        public event EventHandler OnControlDataUpdate;
        public event EventHandler OnPlaybackSpeedChange;
        public event EventHandler OnIsMutedChange;
        public event EventHandler OnChangeChapter;
        public event EventHandler OnUpdateBookMarks;
        public event EventHandler OnUpdateBookMarkToolTips;

        public event AudioFileDelegate PlayedAudioFile;
        public void OnPlayedAudioFile(IAudioFile file)
        {
            PlayedAudioFile?.Invoke(file);
        }

        public event AudiobookDelegate FinishedAudiobook;
        protected void OnFinishedAudiobook(IAudiobook audiobook)
        {
            FinishedAudiobook?.Invoke(audiobook);
        }

        public event AudiobookDelegate PlayedAudiobook;
        public void OnPlayedAudiobook(IAudiobook audiobook)
        {
            PlayedAudiobook?.Invoke(audiobook);
        }

        private void GlobalPlayerControls_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Timeline_CreateChapterMarkers();
            Timeline_CreateBookMarks();
        }

        private void Image_View_Click(object sender, RoutedEventArgs e)
        {
            ShowLargeIconDialog();
        }

        private void Image_SetIcon_Click(object sender, RoutedEventArgs e)
        {
            ShowSetIconDialog();
        }

        private void Audiobook_Details_Click(object sender, RoutedEventArgs e)
        {
            OpenAudiobookDetails();
        }

        private void OnAudiobookBookmarksChanged()
        {
            App.Current.Dispatcher.Invoke(new Action(() => { Timeline_CreateBookMarks(); }));
        }

        private void OnAudiobookChaptersChanged()
        {
            App.Current.Dispatcher.Invoke(new Action(() => { Timeline_CreateChapterMarkers(); }));
        }

        public void IncrementVolume()
        {
            var currVolume = App.Globals.settings.Volume;

            SetVolume(currVolume + 1);
        }

        public void DecrementVolume()
        {
            var currVolume = App.Globals.settings.Volume;

            SetVolume(currVolume - 1);
        }

        private void ButtonMaximize_Click(object sender, RoutedEventArgs e)
        {
            FullScreenPlayer fullScreenPlayer = new FullScreenPlayer();

            var window = App.Current.MainWindow as NavigationWindow;
            window.NavigationService.Navigate(fullScreenPlayer);
        }

        double MaxBookMarkDeletionSeconds = 2.0d;
        public void RemoveCloseBookMark()
        {
            if (CurrentAudiobook == null)
                return;

            foreach (var bookMark in CurrentAudiobook.BookMarks)
            {
                double bookMarkDelta = Math.Abs(bookMark.Position.TotalSeconds - CurrentAudiobook.AudiobookPosition.TotalSeconds);

                if (bookMarkDelta <= MaxBookMarkDeletionSeconds)
                {
                    CurrentAudiobook.RemoveBookMark(bookMark);
                    return;
                }
            }
        }

        public void AddAutomaticBookMark()
        {
            if (CurrentAudiobook == null)
                return;

            var newBookMarkPos = CurrentAudiobook.AudiobookPosition;

            if (!CurrentAudiobook.HasBookMarkAtPosition(newBookMarkPos))
                CurrentAudiobook.AddBookMark(CurrentAudiobook.GetFreeBookMarkIdentifier(), newBookMarkPos);
            else
            {
                DebugLog.WriteLine("info: AddAutomaticBookMark failed because there is already a bookmark closeby");
                App.Globals.textToSpeech.Speak("There is already a bookmark here.");
            }
        }

        double MinBookMarkJumpDelta = 2.0d;
        public void JumpToNextBookmark()
        {
            if (CurrentAudiobook == null)
                return;

            for (int i = 0; i < CurrentAudiobook.BookMarks.Count; i++)
            {
                var currentBookMark = CurrentAudiobook.BookMarks[i];
                if (currentBookMark != null)
                {
                    double bookMarkDelta = Math.Abs(currentBookMark.Position.TotalSeconds - CurrentAudiobook.AudiobookPosition.TotalSeconds);
                    if (currentBookMark.Position > CurrentAudiobook.AudiobookPosition &&
                        bookMarkDelta >= MinBookMarkJumpDelta)
                    {
                        ApplyBookMark(currentBookMark);
                        return;
                    }
                }
            }
        }

        public void JumpToPreviousBookmark()
        {
            if (CurrentAudiobook == null)
                return;

            for (int i = CurrentAudiobook.BookMarks.Count - 1; i >= 0; i--)
            {
                var currentBookMark = CurrentAudiobook.BookMarks[i];
                if (currentBookMark != null)
                {
                    double bookMarkDelta = Math.Abs(currentBookMark.Position.TotalSeconds - CurrentAudiobook.AudiobookPosition.TotalSeconds);
                    if (currentBookMark.Position < CurrentAudiobook.AudiobookPosition &&
                        bookMarkDelta >= MinBookMarkJumpDelta)
                    {
                        ApplyBookMark(currentBookMark);
                        return;
                    }
                }
            }
        }

        public void PlayNextAudiobook()
        {
            List<IAudiobook> audiobookList = App.Globals.audiobookScanner.AudioBookList.OrderBy(audiobook => audiobook.Title).ToList();

            if (audiobookList.Count == 0) 
            {
                DebugLog.WriteLine("info: couldn't find an audiobook to switch to");
                App.Globals.textToSpeech.Speak("Couldn't find an audiobook to switch to");
                return;
            }

            IAudiobook newAudiobook = null;

            if (CurrentAudiobook != null)
            {
                int currentBookIdx = audiobookList.FindIndex(audiobook => audiobook == CurrentAudiobook);
                if (currentBookIdx >= 0)
                {
                    if (currentBookIdx + 1 < audiobookList.Count)
                        newAudiobook = audiobookList[currentBookIdx + 1];
                    else
                        newAudiobook = audiobookList[0];
                }
            }

            if (newAudiobook == null)
                newAudiobook = audiobookList[0];

            PlayAudiobook(newAudiobook, false, true);
        }

        public void PlayPreviousAudiobook()
        {
            List<IAudiobook> audiobookList = App.Globals.audiobookScanner.AudioBookList.OrderBy(audiobook => audiobook.Title).ToList();

            if (audiobookList.Count == 0)
            {
                DebugLog.WriteLine("info: couldn't find an audiobook to switch to");
                App.Globals.textToSpeech.Speak("Couldn't find an audiobook to switch to");
                return;
            }

            IAudiobook newAudiobook = null;

            if (CurrentAudiobook != null)
            {
                int currentBookIdx = audiobookList.FindIndex(audiobook => audiobook == CurrentAudiobook);
                if (currentBookIdx >= 0)
                {
                    if (currentBookIdx - 1 >= 0)
                        newAudiobook = audiobookList[currentBookIdx - 1];
                    else
                        newAudiobook = audiobookList[audiobookList.Count - 1];
                }
            }

            if (newAudiobook == null)
                newAudiobook = audiobookList[0];

            PlayAudiobook(newAudiobook, false, true);
        }
    }
}
