﻿using AudiobookSuite.Commons;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for Dialog_ConfirmDeleteAudiobooks.xaml
    /// </summary>
    public partial class Dialog_ConfirmDeleteGroup : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ISortingGroup LinkedGroup { get; set; }

        public Dialog_ConfirmDeleteGroup(ISortingGroup group)
        {
            DataContext = this;

            LinkedGroup = group;

            InitializeComponent();

            NotifyPropertyChanged(nameof(LinkedGroup));
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Button_Yes_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        public void Button_No_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Back)
            {
                DialogResult = false;
            }
            else if (e.Key == Key.Return)
            {
                DialogResult = true;
            }
        }
    }
}
