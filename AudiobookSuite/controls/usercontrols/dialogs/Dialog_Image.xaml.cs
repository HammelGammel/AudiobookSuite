﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for Dialog_NewAudiobook.xaml
    /// </summary>
    public partial class Dialog_Image : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public string ImagePath { get; set; }

        public static int IsOpenCtr = 0;
        public static bool IsOpen { get { return IsOpenCtr > 0; } }

        private bool IsClosing = false;

        public Dialog_Image(string imagePath)
        {
            DataContext = this;

            ImagePath = imagePath;

            InitializeComponent();
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Back)
            {
                Close();
            }
            else if (e.Key == Key.Return)
            {
                Close();
            }
        }

        private void Button_Close_Click(object sender, RoutedEventArgs e)
        {
            IsClosing = true;
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            IsOpenCtr--;
            IsClosing = true;
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            IsOpenCtr++;
        }

        private void Window_Deactivated(object sender, EventArgs e)
        {
            if (!IsClosing)
            {
                IsClosing = true;
                Close();
            }
        }

        private void WindowHeader_MouseDown(object sender, MouseButtonEventArgs e)
        {
            // exit maximized mode
            WindowState = WindowState.Normal;

            DragMove();
        }
    }
}
