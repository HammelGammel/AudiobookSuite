﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;

namespace AudiobookSuite.controls.usercontrols
{
    public partial class Dialog_Message : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public string MessageTitle { get; set; }
        public List<Run> MessageTexts { get; set; }

        public Dialog_Message(string title, string message)
        {
            DataContext = this;

            MessageTitle = title;
            
            MessageTexts = new List<Run>();
            MessageTexts.Add(new Run(message));

            InitializeComponent();

            ConstructMessageInlines();
        }

        public Dialog_Message(string title, List<Run> texts)
        {
            DataContext = this;

            MessageTitle = title;
            MessageTexts = texts;

            InitializeComponent();

            ConstructMessageInlines();
        }

        public void ConstructMessageInlines()
        {
            MessageTextblock.Inlines.Clear();

            for (int i = 0; i < MessageTexts.Count; i++)
            {
                MessageTextblock.Inlines.Add(MessageTexts[i]);
            }
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Button_Confirm_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Back)
            {
                DialogResult = false;
            }
            else if (e.Key == Key.Return)
            {
                DialogResult = true;
            }
        }
    }
}
