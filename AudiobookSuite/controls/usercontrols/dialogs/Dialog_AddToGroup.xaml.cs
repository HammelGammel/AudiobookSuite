﻿using AudiobookSuite.Commons;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for Dialog_NewAudiobook.xaml
    /// </summary>
    public partial class Dialog_AddToGroup : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public string GroupNameText { get; set; }
        public List<string> GroupNames { get; set; }

        public Dialog_AddToGroup()
        {
            DataContext = this;

            GroupNameText = "";
            GroupNames = new List<string>();

            InitializeComponent();
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Button_Create_Click(object sender, RoutedEventArgs e)
        {
            var selectedItems = GroupsAnchor.SelectedItems;
            if (selectedItems.Count > 0)
            {
                foreach(var selectedItem in selectedItems)
                {
                    GroupNames.Add(selectedItem.ToString());
                }

                DialogResult = true;
            }
        }

        private void Button_CreateFromName_Click(object sender, RoutedEventArgs e)
        {
            if (GroupNameText.Length > 0 && GroupNameText != App.Globals.groupManager.GetDefaultGroupName())
            {
                GroupNames.Add(GroupNameText);

                DialogResult = true;
            }
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Back)
            {
                DialogResult = false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GroupsAnchor.Items.Clear();

            Dictionary<string, ISortingGroup> groups = App.Globals.groupManager.Groups;

            foreach (var pair in groups)
            {
                GroupsAnchor.Items.Add(pair.Value.GroupName);
            }
        }

        private void GroupsAnchor_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta / 10.0);
            e.Handled = true;
        }
    }
}
