﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for ListEditDialog.xaml
    /// </summary>
    public partial class ListEditDialog : Window
    {
        public List<string> DataList { get; set; }
        public string DefaultValue { get; set; }
        public string DialogTitle { get; set; }

        public ListEditDialog(List<string> dataList, string Title, string defaultValue)
        {
            DataList = new List<string>(dataList);
            DefaultValue = defaultValue;
            DialogTitle = Title;

            InitializeComponent();

            _ListEditControl.Initialize(DataList, DefaultValue);
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void ButtonConfirm_Click(object sender, RoutedEventArgs e)
        {
            DataList = _ListEditControl.DataList;

            DialogResult = true;
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                DialogResult = false;
            }
            else if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }
    }
}
