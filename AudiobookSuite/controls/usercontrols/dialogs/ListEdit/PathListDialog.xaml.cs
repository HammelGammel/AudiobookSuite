﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using Microsoft.WindowsAPICodePack.Dialogs;
using AudiobookSuite.lib;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for ListEditDialog.xaml
    /// </summary>
    public partial class PathListDialog : Window
    {
        public List<string> PathList { get; set; }
        public string DialogTitle { get; set; }

        public PathListDialog(List<string> initialPaths, string Title)
        {
            PathList = new List<string>();
            PathList.AddRange(initialPaths);

            DialogTitle = Title;

            InitializeComponent();

            _ListEditControl.Initialize(PathList, "");

            _ListEditControl.UserAddedEntry += OnAddedPath;
        }

        ~PathListDialog()
        {
            _ListEditControl.UserAddedEntry -= OnAddedPath;
        }

        private void OnAddedPath(int dataIdx)
        {
            string newPath = OpenScanPathDialog();

            if (newPath.Length == 0 || _ListEditControl.ContainsEntry(newPath))
            {
                _ListEditControl.RemoveEntry(dataIdx);
                return;
            }

            _ListEditControl.SetEntry(dataIdx, newPath);
        }

        public string OpenScanPathDialog()
        {
            try
            {
                CommonOpenFileDialog dialog = new CommonOpenFileDialog();
                //dialog.InitialDirectory = Txt_ScanPath.Text;
                dialog.IsFolderPicker = true;

                Window parentWindow = Window.GetWindow(this);

                CommonFileDialogResult Action = dialog.ShowDialog(parentWindow);

                if (Action == CommonFileDialogResult.Ok)
                {
                    return dialog.FileName;
                }
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("OpenScanPathDialog failed");
            }

            return "";
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            PathList = _ListEditControl.DataList;

            DialogResult = false;
        }

        private void ButtonConfirm_Click(object sender, RoutedEventArgs e)
        {
            PathList = _ListEditControl.DataList;

            DialogResult = true;
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                DialogResult = false;
            }
            else if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }
    }
}
