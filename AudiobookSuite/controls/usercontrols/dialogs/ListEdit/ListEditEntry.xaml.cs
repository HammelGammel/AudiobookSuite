﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for ListEditEntry.xaml
    /// </summary>
    public partial class ListEditEntry : UserControl
    {
        public delegate void RemovedEventHandler(ListEditEntry entry);

        public int ContainingListIdx;
        public ListEditControl ContainingListEdit;

        public string DataText { get; set; }

        public ListEditEntry(ListEditControl container, int idx)
        {
            ContainingListEdit = container;
            ContainingListIdx = idx;

            DataText = GetData();

            InitializeComponent();
        }

        public string GetData()
        {
            if (ContainingListEdit == null || ContainingListEdit.DataList.Count < ContainingListIdx)
                return "NaN";

            return ContainingListEdit.DataList[ContainingListIdx];
        }

        public void UpdateContainerData()
        {
            if (ContainingListEdit == null || ContainingListEdit.DataList.Count < ContainingListIdx)
                return;

            ContainingListEdit.DataList[ContainingListIdx] = DataText;
        }

        private void ButtonRemoveEntry_Click(object sender, RoutedEventArgs e)
        {
            OnRemove(this);
        }

        public event RemovedEventHandler Remove;
        public void OnRemove(ListEditEntry entry)
        {
            Remove?.Invoke(entry);
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            UpdateContainerData();
        }
    }
}
