﻿using AudiobookSuite.Commons;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for TimelineBookMark.xaml
    /// </summary>
    public partial class TimelineBookMark : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public IBookMark AssociatedBookMark { get; set; }
        public TimeSpan TimelinePosition { get; set; }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public TimelineBookMark(IBookMark bookMark)
        {
            DataContext = this;
            InitializeComponent();

            AssociatedBookMark = bookMark;
            TimelinePosition = bookMark.Position;

            NotifyPropertyChanged(nameof(AssociatedBookMark));
        }

        public TimelineBookMark()
        {
            DataContext = this;
            InitializeComponent();

            TimelinePosition = TimeSpan.FromSeconds(0);
        }

        private void Marker_Click(object sender, RoutedEventArgs e)
        {
            var controls = App.Globals.playerControls;
            if (controls != null)
                controls.ApplyBookMark(AssociatedBookMark);
        }

        private void RemoveBookMark_Click(object sender, RoutedEventArgs e)
        {
            var controls = App.Globals.playerControls;
            if (controls != null)
                controls.RemoveBookMark(AssociatedBookMark);
        }
    }
}
