﻿using AudiobookSuite.Commons;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for BookMarkLine.xaml
    /// </summary>
    public partial class BookMarkLine : UserControl, INotifyPropertyChanged
    {
        public delegate void RemovedEventHandler(BookMarkLine entry);

        public IBookMark AssociatedBookMark { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public BookMarkLine(IBookMark bookMark)
        {
            DataContext = this;

            AssociatedBookMark = bookMark;

            InitializeComponent();
        }

        public BookMarkLine()
        {
            InitializeComponent();
        }

        private void ButtonRemoveEntry_Click(object sender, RoutedEventArgs e)
        {
            OnRemove(this);
        }

        public event RemovedEventHandler Remove;
        public void OnRemove(BookMarkLine entry)
        {
            Remove?.Invoke(entry);
        }

        private void ButtonPlay_Click(object sender, RoutedEventArgs e)
        {
            IPlayerControls controls = App.Globals.playerControls;
            if (controls != null)
            {
                controls.ApplyBookMark(AssociatedBookMark);
            }
        }
    }
}
