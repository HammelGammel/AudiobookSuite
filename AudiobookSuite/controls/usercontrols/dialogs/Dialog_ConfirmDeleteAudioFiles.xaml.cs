﻿using AudiobookSuite.Commons;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for Dialog_ConfirmDeleteAudiobooks.xaml
    /// </summary>
    public partial class Dialog_ConfirmDeleteAudioFiles : Window, INotifyPropertyChanged
    {
        private List<IAudioFileData> AudioFilesToDelete;
        public int FileCount { get; set; }
        public bool JustOneFile { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public Dialog_ConfirmDeleteAudioFiles(List<IAudioFileData> audioFilesToDelete)
        {
            AudioFilesToDelete = audioFilesToDelete;
            FileCount = audioFilesToDelete.Count;
            JustOneFile = audioFilesToDelete.Count <= 1;

            NotifyPropertyChanged("FileCount");
            NotifyPropertyChanged("JustOneFile");

            InitializeComponent();

            DataContext = this;
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Button_Yes_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        public void Button_No_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Back)
            {
                DialogResult = false;
            }
            else if (e.Key == Key.Return)
            {
                DialogResult = true;
            }
        }
    }
}
