﻿using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for Dialog_SetPlaybackSpeed.xaml
    /// </summary>
    public partial class Dialog_SetPlaybackSpeed : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public double NewSpeed { get; set; }

        public Dialog_SetPlaybackSpeed(double playbackSpeed)
        {
            DataContext = this;
            InitializeComponent();

            PlaybackSpeedSlider.Value = playbackSpeed;
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Button_Confirm_Click(object sender, RoutedEventArgs e)
        {
            NewSpeed = PlaybackSpeedSlider.Value;

            DialogResult = true;
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Back)
            {
                DialogResult = false;
            }
            else if (e.Key == Key.Return)
            {
                DialogResult = true;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void Click_SetSpeed(object sender, RoutedEventArgs e)
        {
            Button senderBtn = sender as Button;
            if (senderBtn != null)
            {
                NewSpeed = float.Parse((string)senderBtn.Tag, CultureInfo.InvariantCulture);
                PlaybackSpeedSlider.Value = NewSpeed;
            }
        }
    }
}
