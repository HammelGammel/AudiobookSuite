﻿using AudiobookSuite.Commons;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AudiobookSuite.controls.usercontrols
{
    /// <summary>
    /// Interaction logic for Dialog_NewAudiobook.xaml
    /// </summary>
    public partial class Dialog_SelectGroup : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public Audiobook LinkedAudiobook;
        public List<ISortingGroup> SelectedGroups;

        public Dialog_SelectGroup(Audiobook? audiobook = null)
        {
            DataContext = this;
            LinkedAudiobook = audiobook;

            SelectedGroups = new List<ISortingGroup>();

            InitializeComponent();
        }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Button_Select_Click(object sender, RoutedEventArgs e)
        {
            var selectedItems = GroupsAnchor.SelectedItems;
            if (selectedItems.Count > 0)
            {
                foreach(var selectedItem in selectedItems)
                {
                    ISortingGroup group = App.Globals.groupManager.GetGroup(selectedItem.ToString());

                    if (group != null)
                    {
                        SelectedGroups.Add(group);
                    }
                }

                DialogResult = true;
            }
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Back)
            {
                DialogResult = false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GroupsAnchor.Items.Clear();

            List<ISortingGroup> groups;
            if (LinkedAudiobook != null)
                groups = LinkedAudiobook.Groups;
            else
            {
                groups = new List<ISortingGroup>();

                foreach (var pair in App.Globals.groupManager.Groups)
                {
                    groups.Add(pair.Value);
                }
            }

            foreach (ISortingGroup group in groups)
            {
                GroupsAnchor.Items.Add(group.GroupName);
            }
        }

        private void GroupsAnchor_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta / 10.0);
            e.Handled = true;
        }
    }
}
