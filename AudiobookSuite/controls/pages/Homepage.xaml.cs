﻿using AudiobookSuite.Commons;
using AudiobookSuite.controls.usercontrols;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ATL;
using AudiobookSuite.lib;

namespace AudiobookSuite
{
    public partial class Homepage : Page
    {
        private List<IAudiobook> LastPlayedAudiobooks;

        public Homepage()
        {
            LastPlayedAudiobooks = new List<IAudiobook>();

            InitializeComponent();

            UserControl playUserCtrl = App.Globals.playerControls as UserControl;
            Utility.RemoveChild(playUserCtrl.Parent, playUserCtrl);
            Grid_PlayerControls.Children.Add(playUserCtrl);
        }

        public void RefreshLastPlayedAudiobooks()
        {
            foreach (UIElement element in PanelLastPlayedFiles.Children)
            {
                LastPlayedRow row = (LastPlayedRow)element;
                if (row != null)
                    row.RemoveLastPlayed -= OnRemoveLastPlayed;
            }

            LastPlayedAudiobooks.Clear();
            PanelLastPlayedFiles.Children.Clear();

            List<int> uniqueIds = App.Globals.settings.LastPlayedIDs;

            int j = 0;
            for (int i = uniqueIds.Count - 1; i >= 0 && j < App.Globals.settings.MaxLastPlayedAudiobooks; i--)
            {
                int uniqueId = uniqueIds[i];

                IAudiobook currentAudiobook = App.Globals.audiobookScanner.GetAudiobookByID(uniqueId);

                if (currentAudiobook != null)
                {
                    LastPlayedAudiobooks.Add(currentAudiobook);

                    LastPlayedRow row = new LastPlayedRow();
                    row.LinkedAudiobook = currentAudiobook;

                    row.RemoveLastPlayed += OnRemoveLastPlayed;

                    PanelLastPlayedFiles.Children.Add(row);
                    j++;
                }
            }
        }

        private void OnRemoveLastPlayed(IAudiobook audiobook)
        {
            RefreshLastPlayedAudiobooks();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshLastPlayedAudiobooks();

            App.Globals.playerControls.PlayedAudiobook += OnPlayedAudiobook;
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            App.Globals.playerControls.PlayedAudiobook -= OnPlayedAudiobook;
        }

        private void OnPlayedAudiobook(IAudiobook audiobook)
        {
            RefreshLastPlayedAudiobooks();
        }

        private void Click_Library(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(Library.Instance);
        }

        private void Click_Settings(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("controls/pages/SettingsPage.xaml", UriKind.Relative));
        }

        private void Click_Help(object sender, RoutedEventArgs e)
        {
            Dialog_GenericConfirmDeny newDiag = new Dialog_GenericConfirmDeny("Open Help Page", "This will open an external browser page to display AudiobookSuite's wiki page.");

            Window parentWindow = Window.GetWindow(this);
            newDiag.Owner = parentWindow;

            IPlayerControls controls = App.Globals.playerControls;
            controls.SetInputDisabled("message");

            if (newDiag.ShowDialog() == true)
            {
                string url = "https://gitlab.com/Shrimperator/AudiobookSuite/-/wikis/home";
                Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
            }

            controls.SetInputEnabled("message");
        }

        private void Grid_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Keyboard.ClearFocus();
            FocusManager.SetFocusedElement(FocusManager.GetFocusScope(this), null);
        }
    }
}
