﻿using System;
using System.ComponentModel;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Navigation;

namespace AudiobookSuite
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : NavigationWindow, INotifyPropertyChanged
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetCursorPos(out POINT lpPoint);

        private const int WM_GETMINMAXINFO = 0x0024;

        private const uint MONITOR_DEFAULTTONEAREST = 0x00000002;

        [DllImport("user32.dll")]
        private static extern IntPtr MonitorFromWindow(IntPtr handle, uint flags);

        [DllImport("user32.dll")]
        private static extern bool GetMonitorInfo(IntPtr hMonitor, ref MONITORINFO lpmi);

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;

            public RECT(int left, int top, int right, int bottom)
            {
                this.Left = left;
                this.Top = top;
                this.Right = right;
                this.Bottom = bottom;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MONITORINFO
        {
            public int cbSize;
            public RECT rcMonitor;
            public RECT rcWork;
            public uint dwFlags;
        }

        [Serializable]
        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;
            public int Y;

            public POINT(int x, int y)
            {
                this.X = x;
                this.Y = y;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MINMAXINFO
        {
            public POINT ptReserved;
            public POINT ptMaxSize;
            public POINT ptMaxPosition;
            public POINT ptMinTrackSize;
            public POINT ptMaxTrackSize;
        }

        /* - - -*/

        public bool RestoreIfMoved { get; set; }

        public int DefaultResizeBorderThickness { get; set; }
        public int ResizeBorderThickness { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;

            DefaultResizeBorderThickness = 5;
            ResizeBorderThickness = DefaultResizeBorderThickness;

            /* remove backspace to navigate page */
            KeyGesture backKeyGesture = null;
            foreach (var gesture in NavigationCommands.BrowseBack.InputGestures)
            {
                KeyGesture keyGesture = gesture as KeyGesture;
                if ((keyGesture != null) &&
                   (keyGesture.Key == Key.Back) &&
                   (keyGesture.Modifiers == ModifierKeys.None))
                {
                    backKeyGesture = keyGesture;
                }
            }

            if (backKeyGesture != null)
            {
                NavigationCommands.BrowseBack.InputGestures.Remove(backKeyGesture);
            }
        }

        public static IntPtr HookProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == WM_GETMINMAXINFO)
            {
                // We need to tell the system what our size should be when maximized. Otherwise it will
                // cover the whole screen, including the task bar.
                MINMAXINFO mmi = (MINMAXINFO)Marshal.PtrToStructure(lParam, typeof(MINMAXINFO));

                // Adjust the maximized size and position to fit the work area of the correct monitor
                IntPtr monitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);

                if (monitor != IntPtr.Zero)
                {
                    MONITORINFO monitorInfo = new MONITORINFO();
                    monitorInfo.cbSize = Marshal.SizeOf(typeof(MONITORINFO));
                    GetMonitorInfo(monitor, ref monitorInfo);
                    RECT rcWorkArea = monitorInfo.rcWork;
                    RECT rcMonitorArea = monitorInfo.rcMonitor;
                    mmi.ptMaxPosition.X = Math.Abs(rcWorkArea.Left - rcMonitorArea.Left);
                    mmi.ptMaxPosition.Y = Math.Abs(rcWorkArea.Top - rcMonitorArea.Top);
                    mmi.ptMaxSize.X = Math.Abs(rcWorkArea.Right - rcWorkArea.Left);
                    mmi.ptMaxSize.Y = Math.Abs(rcWorkArea.Bottom - rcWorkArea.Top);
                }

                Marshal.StructureToPtr(mmi, lParam, true);
            }

            return IntPtr.Zero;
        }

        public void DisableHardwareAcceleration()
        {
            var hwndSource = PresentationSource.FromVisual(this) as HwndSource;
            if (hwndSource != null)
                hwndSource.CompositionTarget.RenderMode = RenderMode.SoftwareOnly;
        }

        public void EnableHardwareAcceleration()
        {
            var hwndSource = PresentationSource.FromVisual(this) as HwndSource;
            if (hwndSource != null)
                hwndSource.CompositionTarget.RenderMode = RenderMode.Default;
        }

        private void NavigationWindow_Loaded(object sender, RoutedEventArgs e)
        {
            ((HwndSource)PresentationSource.FromVisual(this)).AddHook(HookProc);

            if (App.Globals.settings.EnableHardwareAcceleration)
                EnableHardwareAcceleration();
            else
                DisableHardwareAcceleration();

            // setup mouse down events for title bar
            // has to be specific to the title bar instead of just using the MainWindow's MouseLeftButtonDown event, otherwise
            // it'll trigger a DragMove when clicking the background

            NavigationWindow navWnd = this as NavigationWindow;
            Grid windowCtrls = (Grid)navWnd.Template.FindName("TitleBar", navWnd);

            if (windowCtrls != null)
            {
                windowCtrls.MouseLeftButtonDown += NavigationWindow_MouseLeftButtonDown;
                windowCtrls.PreviewMouseLeftButtonDown += NavigationWindow_MouseLeftButtonUp;
                windowCtrls.MouseMove += NavigationWindow_MouseMove;
            }
        }

        private void NavigationWindow_Unloaded(object sender, RoutedEventArgs e)
        {
        }

        private void SwitchWindowState()
        {
            switch (WindowState)
            {
                case WindowState.Normal:
                    {
                        WindowState = WindowState.Maximized;
                        break;
                    }
                case WindowState.Maximized:
                    {
                        WindowState = WindowState.Normal;
                        break;
                    }
            }
        }

        Point MouseDownPos;

        private void NavigationWindow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MouseDownPos = e.GetPosition(this);

            if (e.ClickCount == 2)
            {
                if ((ResizeMode == ResizeMode.CanResize) || (ResizeMode == ResizeMode.CanResizeWithGrip))
                    SwitchWindowState();
            }
            else if (WindowState == WindowState.Maximized)
                RestoreIfMoved = true;
            else
                DragMove();
        }

        public event PropertyChangedEventHandler? PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        float MinMouseDelta = 5.0f;

        private void NavigationWindow_MouseMove(object sender, MouseEventArgs e)
        {
            Point currentMousePos = e.GetPosition(this);
            Vector2 mouseDelta = new Vector2((float)(MouseDownPos.X - currentMousePos.X), (float)(MouseDownPos.Y - currentMousePos.Y));

            if (mouseDelta.Length() >= MinMouseDelta && 
                RestoreIfMoved && 
                e.LeftButton == MouseButtonState.Pressed)
            {
                RestoreIfMoved = false;

                double percentHorizontal = currentMousePos.X / ActualWidth;
                double targetHorizontal = RestoreBounds.Width * percentHorizontal;

                double percentVertical = currentMousePos.Y / ActualHeight;
                double targetVertical = RestoreBounds.Height * percentVertical;

                WindowState = WindowState.Normal;

                POINT lMousePosition;
                GetCursorPos(out lMousePosition);

                Left = lMousePosition.X - targetHorizontal;
                Top = lMousePosition.Y - targetVertical;

                DragMove();
            }
        }

        private void NavigationWindow_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            RestoreIfMoved = false;
        }

        private void NavigationWindow_StateChanged(object sender, EventArgs e)
        {
            ResizeMode = ResizeMode.CanResize;

            if (WindowState == WindowState.Maximized)
            {
                ResizeBorderThickness = 0;
            }
            else
            {
                ResizeBorderThickness = DefaultResizeBorderThickness;
            }

            NotifyPropertyChanged(nameof(ResizeBorderThickness));
        }

        private void CommandBinding_Minimize_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void CommandBinding_Maximize_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            WindowState = WindowState.Maximized;
        }

        private void CommandBinding_Restore_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            WindowState = WindowState.Normal;
        }

        private void CommandBinding_Close_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }
    }
}
