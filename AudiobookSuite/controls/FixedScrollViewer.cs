﻿using System.Windows.Input;
using Wpf = System.Windows;
using WpfControls = System.Windows.Controls;

namespace AudiobookSuite.controls
{
    public class FixedScrollViewer : WpfControls.ScrollViewer
    {
        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            if (ComputedVerticalScrollBarVisibility == Wpf.Visibility.Collapsed)
                return;
            base.OnMouseWheel(e);
        }
    }
}
