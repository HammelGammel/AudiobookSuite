using System;
using System.Collections.Generic;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// data type for an Audiobook
    /// </summary>
    public interface IAudiobook
    {
        /// <summary>
        /// audiobook bookmarks
        /// </summary>
        public List<IBookMark> BookMarks { get; set; }
        /// <summary>
        /// cached chapter data
        /// </summary>
        public List<IAudioFileChapterData> Chapters { get; set; }
        /// <summary>
        /// file dialog filters for image files supported for audiobook cover images
        /// </summary>
        static string ImageFileFilters;
        /// <summary>
        /// unique id of this audiobook. IDs do not have to be consecutive, but
        /// attempt to be
        /// </summary>
        public int UniqueID { get; set; }
        /// <summary>
        /// true if currently being played by the active PlayerControls
        /// </summary>
        public bool IsPlaying { get; set; }
        /// <summary>
        /// true if playback is at nearly 100%
        /// </summary>
        public bool IsFinished { get; set; }
        /// <summary>
        /// true if the audiobook should (by default) not be shown in the library. Hidden
        /// files have to be enabled via filter on the library page
        /// </summary>
        public bool IsHidden { get; set; }
        /// <summary>
        /// list of authors, sanitized to remove duplicates, sorted alphabetically.
        /// Scanned from first audiofile metadata of this audiobook
        /// </summary>
        public List<string> Authors { get; set; }
        /// <summary>
        /// list of narrators, sanitized to remove duplicates, sorted alphabetically.
        /// Scanned from first audiofile metadata of this audiobook
        /// </summary>
        public List<string> Narrators { get; set; }
        /// <summary>
        /// path to the audiobook's cover image. Can either be a path to an image file, or
        /// to an audiofile with embedded cover image. AudiobookSuite automatically uses a
        /// converter in the latter case to create a bitmap image out of the embedded
        /// cover art.
        /// </summary>
        public string IconPath { get; set; }
        /// <summary>
        /// Unique display title. Either created from first audiofile title metadata,
        /// or from sanitized first audiofile filename. If neither is valid, created from
        /// containing foldername as last resort.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// date the audiobook was created, using the local timezone
        /// </summary>
        public DateTime? CreationDate { get; set; }
        /// <summary>
        /// date the audiobook was created, using the local timezone
        /// </summary>
        public DateTime? LastPlayedDate { get; set; }
        /// <summary>
        /// percentage of the audiobook the position is currently at 
        /// </summary>
        public double Played { get; set; }
        /// <summary>
        /// the sum of all registered audiofiles' durations
        /// </summary>
        public TimeSpan FullDuration { get; set; }
        /// <summary>
        /// the path where the audiobook is located. Generated from
        /// the first registered audiofile
        /// </summary>
        public string AudiobookPath { get; set; }
        /// <summary>
        /// index of the currently playing audioFile in AudioFiles
        /// </summary>
        public int AudioFileIdx { get; set; }
        /// <summary>
        /// total playback position within the entire audiobook, across all registered audiofiles
        /// </summary>
        public TimeSpan AudiobookPosition { get; set; }
        /// <summary>
        /// playback position within the currently playing audiofile
        /// </summary>
        public TimeSpan AudioFilePosition { get; set; }
        /// <summary>
        /// list of all registered audiofiles. Note that IAudioFile only contains the most necessary
        /// data of an AudioFile. For more data, refer to IAudioFileData, which contains a bunch of
        /// data that doesn't have to be cached at all times, to save memory
        /// </summary>
        public List<IAudioFile> AudioFiles { get; set; }
        /// <summary>
        /// a combined, comma separated string of all entries of Authors
        /// </summary>
        public string JoinedAuthors { get; set; }
        /// <summary>
        /// a combined, comma separated string of all entries of Narrators
        /// </summary>
        public string JoinedNarrators { get; set; }
        /// <summary>
        /// list of all groups this audiobook is registered in
        /// </summary>
        public List<ISortingGroup> Groups { get; set; }

        /// <summary>
        /// add this audiobook to the supplied sorting group
        /// </summary>
        /// <param name="group"></param>
        /// <returns>true if successfully added to group</returns>
        public bool AddToGroup(ISortingGroup group);
        /// <summary>
        /// add this audiobook to the supplied sorting group. Case insensitive
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns>true if successfully added to group</returns>
        public bool AddToGroup(string groupName);

        /// <summary>
        /// returns true if there is already a bookmark at the given position
        /// </summary>
        /// <returns></returns>
        public bool HasBookMarkAtPosition(TimeSpan position);
        /// <summary>
        /// iterates from 1 to maxint until an identifier isn't a duplicate, then returns that
        /// Returns "NONE" if it fails (practically impossible)
        /// </summary>
        /// <returns></returns>
        public string GetFreeBookMarkIdentifier();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="position"></param>
        /// <returns>true if bookmark was successfully added</returns>
        public bool AddBookMark(string identifier, TimeSpan position);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookMark"></param>
        /// <returns>true if bookmark was successfully removed</returns>
        public bool RemoveBookMark(IBookMark bookMark);

        /// <summary>
        /// updates JoinedAuthors and JoinedNarrators from their respective list
        /// </summary>
        public void RefreshJoinedStrings();

        /// <summary>
        /// recaches data from sample audiofile (first file in AudioFiles), and
        /// resets all audiobook metadata to cached data
        /// </summary>
        /// <param name="data">if no data parameter is supplied, automatically caches data from sample audiofile</param>
        public void RefreshMetadata(IAudioFileData? data = null);

        /// <summary>
        /// recaches data from sample audiofile (first file in AudioFiles), and
        /// resets some audiobook metadata to cached data. Limited variant only
        /// refreshes data not expected to be changed by the user
        /// </summary>
        public void RefreshMetadataLimited();

        /// <summary>
        /// refreshes chapter metadata and generates chapters from it
        /// </summary>
        public void RefreshChapterData();

        /// <summary>
        /// 
        /// </summary>
        /// <returns>true if the audiobook has a cover image set</returns>
        public bool HasIcon();

        public bool CheckHasDuplicateOrInvalidTrackNumbers();
        public bool CheckHasValidTrackNumbers();

        public void AutoSortAudioFiles();

        /// <summary>
        /// attempts to read a cover image from the supplied audiofile data
        /// </summary>
        /// <param name="data">if no data parameter is supplied, automatically caches data from sample audiofile</param>
        public void RefreshIcon(IAudioFileData? data = null);

        /// <summary>
        /// iterates AudioFiles and returns the first valid entry
        /// </summary>
        /// <returns>a valid AudioFile or null</returns>
        public IAudioFile GetFirstValidAudioFile();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        public void NotifyPropertyChanged(string propertyName);

        /// <summary>
        /// sets playback position to 100%
        /// </summary>
        public void MarkRead();

        /// <summary>
        /// sets playback position to 0%
        /// </summary>
        public void MarkUnread();

        /// <summary>
        /// sets playback percentage depending on position within the audiobook
        /// </summary>
        public void UpdatePlayedPercent();

        /// <summary>
        /// get current audiofile depending on playback position
        /// </summary>
        /// <returns>currently active audiofile or null</returns>
        public IAudioFile GetCurrentAudioFile();

        /// <summary>
        /// sets currently playing AudioFile to the next index
        /// </summary>
        /// <returns></returns>
        public bool NextFile();

        /// <summary>
        /// sets currently playing AudioFile to the previous index
        /// </summary>
        /// <returns></returns>
        public bool PreviousFile();

        /// <summary>
        /// if the AudioFile is registered to this audiobook, sets the index of the currently
        /// active AudioFile to the supplied file
        /// </summary>
        /// <param name="file"></param>
        public void SetAudioFile(IAudioFile file);

        /// <summary>
        /// register an AudioFile to this Audiobook
        /// </summary>
        /// <param name="file"></param>
        public void AddAudioFile(IAudioFile file);

        /// <summary>
        /// register an AudioFile to this Audiobook at a specified index within the AudioFiles list
        /// </summary>
        /// <param name="file"></param>
        /// <param name="targetIdx"></param>
        /// <returns></returns>
        public bool InsertAudioFile(IAudioFile file, int targetIdx);

        /// <summary>
        /// remove a specific AudioFile from the list of registered AudioFiles
        /// </summary>
        /// <param name="file"></param>
        public void RemoveAudioFile(IAudioFile file);

        /// <summary>
        /// move a registered AudioFile from one index in AudioFiles to another
        /// </summary>
        /// <param name="idxFrom"></param>
        /// <param name="idxTo"></param>
        /// <returns></returns>
        public bool RearrangeAudiofile(int idxFrom, int idxTo);

        /// <summary>
        /// sets the path this Audiobook is located at. Used, for example, to scan for a valid icon within
        /// the supplied folder
        /// </summary>
        /// <param name="path"></param>
        public void SetAudiobookPath(string path);

        /// <summary>
        /// scan for a valid cover image in AudiobookPath
        /// </summary>
        public void ScanForIcon();

        /// <summary>
        /// get the name of the first valid registered AudioFile
        /// </summary>
        /// <returns>name of the first valid registered AudioFile or Audiobook title</returns>
        public string GetSampleFilename();

        /// <summary>
        /// get the first valid registered AudioFile
        /// </summary>
        /// <returns>first valid registered AudioFile or null</returns>
        public IAudioFile GetSampleFile();

        /// <summary>
        /// caches AudioFileData for the first valid registered AudioFile
        /// </summary>
        /// <returns>cached AudioFileData</returns>
        public IAudioFileData CacheSampleData();

        /// <summary>
        /// attempts to find a cover image with a similar name to this Audiobook in AudiobookPath
        /// </summary>
        /// <param name="paths"></param>
        /// <returns>path to the cover image or empty string</returns>
        public string GetIdealIcon(IEnumerable<string> paths);

        /// <summary>
        /// gets any valid cover image within AudiobookPath
        /// </summary>
        /// <param name="paths"></param>
        /// <returns>path to the cover image or empty string</returns>
        public string GetAnyIcon(IEnumerable<string> paths);

        /// <summary>
        /// called after the AudioBook was successfully loaded from json library file
        /// </summary>
        public void PostLoad();

        /// <summary>
        /// caching duration is slow, so duration is stored in the save file. If, after
        /// loading no valid duration has been restored, try caching again
        /// </summary>
        public void CheckFileDurationPostLoad();

        /// <summary>
        /// updates AudiobookPosition from AudioFilePosition
        /// </summary>
        public void UpdateAudiobookPosition();

        /// <summary>
        /// updates duration of all combined registerd AudioFiles
        /// </summary>
        public void UpdateFullDuration();

        /// <summary>
        /// given a position within the Audiobook, returns data of the chapter that should be playing
        /// at that position, or null
        /// </summary>
        /// <param name="duration"></param>
        /// <returns></returns>
        public IAudioFileChapterData? GetChapterAtDuration(TimeSpan duration);

        /// <summary>
        /// given a position within the Audiobook, returns the AudioFile that should be playing
        /// at that position, or null
        /// </summary>
        /// <param name="duration"></param>
        /// <returns>audiofile at that position or null</returns>
        public IAudioFile? GetFileAtDuration(TimeSpan duration);

        /// <summary>
        /// given a position within the Audiobook, returns the AudioFile's index that should be playing
        /// at that position, or null
        /// </summary>
        /// <param name="duration"></param>
        /// <returns>index of the AudioFile at that position or top index in AudioFiles</returns>
        public int GetFileIdxAtDuration(TimeSpan duration);

        /// <summary>
        /// position within the Audiobook into position in specific AudioFile registered in this Audiobook
        /// </summary>
        /// <param name="duration"></param>
        /// <param name="file"></param>
        /// <returns>duration within supplied AudioFile or empty TimeSpan</returns>
        public TimeSpan GetLocalDurationFromTotal(TimeSpan duration, IAudioFile file);

        /// <summary>
        /// converts position within an AudioFile into position within the full AudioBook
        /// </summary>
        /// <param name="duration"></param>
        /// <param name="file"></param>
        /// <returns>duration in the full Audiobook or empty TimeSpan</returns>
        public TimeSpan GetTotalDurationFromLocal(TimeSpan duration, IAudioFile file);

        /// <summary>
        /// 
        /// </summary>
        public delegate void ChangedChaptersDelegate();
        /// <summary>
        /// 
        /// </summary>
        public event ChangedChaptersDelegate ChangedChapters;

        /// <summary>
        /// 
        /// </summary>
        public delegate void ChangedBookMarksDelegate();
        /// <summary>
        /// 
        /// </summary>
        public event ChangedBookMarksDelegate ChangedBookMarks;
    }
}
