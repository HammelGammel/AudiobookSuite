﻿using System;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// contains data for an audiobook bookmark
    /// </summary>
    /// 
    public interface IBookMark
    {
        /// <summary>
        /// name of this bookmark
        /// </summary>
        public string Identifier { get; set; }
        
        /// <summary>
        /// position within the entire audiobook
        /// </summary>
        public TimeSpan Position { get; set; }
        
        /// <summary>
        /// Audiobook this bookmark belongs to
        /// </summary>
        public IAudiobook AssociatedAudiobook { get; set; }
    }
}
