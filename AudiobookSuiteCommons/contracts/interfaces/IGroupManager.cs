﻿using System.Collections.Generic;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="group"></param>
    public delegate void GroupDelegate(ISortingGroup group);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="group"></param>
    /// <param name="oldName"></param>
    /// <param name="newName"></param>
    public delegate void GroupRenamedDelegate(ISortingGroup group, string oldName, string newName);

    /// <summary>
    /// manages all registered groups and linked items within
    /// </summary>
    public interface IGroupManager
    {
        /// <summary>
        /// list of all registered groups
        /// </summary>
        public Dictionary<string, ISortingGroup> Groups { get; set; }

        /// <summary>
        /// in the case of audiobook library, this is the "All" group. Audiobooks cannot
        /// be removed from this default group
        /// </summary>
        public ISortingGroup DefaultGroup { get; set; }

        /// <summary>
        /// called after all App.Globals have been initialized and registered
        /// </summary>
        public void FinishedInit();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool AddGroup(ISortingGroup group);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public bool AddGroup(string groupName);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool RemoveGroup(ISortingGroup group);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public bool RemoveGroup(string groupName);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="group"></param>
        /// <param name="linkedObject">eg. audiobook</param>
        /// <returns></returns>
        public bool AddToGroup(ISortingGroup group, object linkedObject);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="linkedObject">eg. audiobook</param>
        /// <returns></returns>
        public bool AddToGroup(string groupName, object linkedObject);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="group"></param>
        /// <param name="linkedObject">eg. audiobook</param>
        public void RemoveFromGroup(ISortingGroup group, object linkedObject);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="linkedObject">eg. audiobook</param>
        public void RemoveFromGroup(string groupName, object linkedObject);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item">eg. audiobook</param>
        public void RemoveFromAllGroups(object item);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="group"></param>
        /// <param name="newName"></param>
        public void RenameGroup(ISortingGroup group, string newName);
        /// <summary>
        /// trims groupname
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public string SanitizeGroupName(string groupName);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetDefaultGroupName();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ISortingGroup GetDefaultGroup();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupName">case insensitive</param>
        /// <returns></returns>
        public ISortingGroup? GetGroup(string groupName);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupName">case insensitive</param>
        /// <returns>true if the group is registered in this manager</returns>
        public bool DoesGroupExist(string groupName);

        /// <summary>
        /// 
        /// </summary>
        public event GroupDelegate AddedGroup;
        /// <summary>
        /// 
        /// </summary>
        public event GroupDelegate RemovedGroup;
        /// <summary>
        /// 
        /// </summary>
        public event GroupRenamedDelegate RenamedGroup;
    }
}
