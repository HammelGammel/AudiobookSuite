﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Controls;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// serialization structure for datagrid layout column
    /// </summary>
    public interface IDataGridLayoutInfoSettings
    {
        /// <summary>
        /// index of the column within datagrid's column order
        /// </summary>
        public int DisplayIndex { get; set; }

        /// <summary>
        /// width of the datagrid column
        /// </summary>
        public DataGridLength Width { get; set; }

        /// <summary>
        /// sort direction of the datagrid column
        /// </summary>
        public ListSortDirection? SortDirection { get; set; }
    }

    /// <summary>
    /// deserialization structure for group definitions (see 'group definitions.json' for an explanation)
    /// </summary>
    public class GroupDefinitions
    {
        /// <summary>
        /// see 'group definitions.json' for an explanation
        /// </summary>
        public List<List<string>> MergeLists;

        /// <summary>
        /// see 'group definitions.json' for an explanation
        /// </summary>
        public List<string> IgnoreList;

        /// <summary>
        /// see 'group definitions.json' for an explanation
        /// </summary>
        public GroupDefinitions()
        {
            MergeLists = new List<List<string>>();
            IgnoreList = new List<string>();
        }
    }

    public enum ETTSVoiceType
    {
        Male,
        Female,
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="identifier"></param>
    public delegate void ChangeOptionDelegate(string identifier);

    /// <summary>
    /// contains various data that should be saved across the application and handles serialization/deserialization of it
    /// </summary>
    public interface ISettings
    {
        /// <summary>
        /// 
        /// </summary>
        //public static readonly string SettingsFolderPath = "\\saved\\";
        /// <summary>
        /// 
        /// </summary>
        public static readonly string SettingsFileName = "config_settings.json";
        /// <summary>
        /// 
        /// </summary>
        //public static readonly string SettingsFilePath = SettingsFolderPath + SettingsFileName;

        /// <summary>
        /// 
        /// </summary>
        public static readonly string ConfigFolderPath = "\\config\\";
        /// <summary>
        /// 
        /// </summary>
        public static readonly string GroupDefinitionsFileName = "group definitions.json";
        /// <summary>
        /// 
        /// </summary>
        public static readonly string GroupDefinitionsFilePath = ConfigFolderPath + GroupDefinitionsFileName;

        /// <summary>
        /// maximum number of recently played Audiobooks to save
        /// </summary>
        public int MaxLastPlayedAudiobooks { get; set; }

        /// <summary>
        /// default structure of library datagrid columns, so they can be restored when necessary
        /// </summary>
        public List<IDataGridLayoutInfoSettings> LibraryColumnsDefault { get; set; }

        /* */
        /* savable properties */
        /* */

        /// <summary>
        /// interval in minutes of how often the settings and library should be autosaved
        /// </summary>
        public double AutosaveIntervalMinutes { get; set; }

        /// <summary>
        /// directory where library scans for audiofiles
        /// </summary>
        public List<string> ScanDirectories { get; set; }

        /// <summary>
        /// list of keyboard / controller shortcuts
        /// </summary>
        public List<IKeyBinding> KeyBindings { get; set; }

        /// <summary>
        /// interval for PlayerControls determining how many seconds to go forward/backwards
        /// when pressing the forward/backward button
        /// </summary>
        public int ForwardBackwardIntervalSeconds { get; set; }

        /// <summary>
        /// PlayerControls volume
        /// </summary>
        public int Volume { get; set; }

        /// <summary>
        /// depth for recursive search when scanning directory
        /// </summary>
        public int FolderSearchDepth { get; set; }

        /// <summary>
        /// hardware acceleration apparently messes with Discord streaming
        /// </summary>
        public bool EnableHardwareAcceleration { get; set; }

        /// <summary>
        /// enables/disables image caching. Uses more memory, but means images don't constantly have to be re-read
        /// speeds up image display
        /// </summary>
        public bool EnableImageCache { get; set; }

        /// <summary>
        /// name of the currently playing audiobook. Might switch to id at some point. Has to be separate
        /// from LastPlayedIDs, because it's possible no file is currently playing, but one was playing in
        /// the past
        /// </summary>
        public string PlayingAudiobookName { get; set; }

        /// <summary>
        /// list of unique Audiobook ids of all recently played Audiobooks
        /// </summary>
        public List<int> LastPlayedIDs { get; set; }

        /// <summary>
        /// playback speed, 1.0 is default
        /// </summary>
        public float PlaybackSpeed { get; set; }

        /// <summary>
        /// true if playback is currently muted
        /// </summary>
        public bool IsMuted { get; set; }

        /// <summary>
        /// search bar content in library
        /// </summary>
        public string SearchFilter { get; set; }

        /// <summary>
        /// true if the console should be displayed. No matter what is saved here, the console is
        /// always constructed, but hidden by default, so it still registers input while
        /// not being displayed
        /// </summary>
        public bool IsConsoleOpen { get; set; }

        /// <summary>
        /// true if the settings have changed sufficiently to allow an autosave
        /// </summary>
        public bool HasAnythingChanged { get; set; }

        /// <summary>
        /// true if AudiobookScanner should automatically create groups from scanned audiobook genres
        /// </summary>
        public bool ScanGenres { get; set; }

        /// <summary>
        /// true if AudiobookScanner should automatically create a group from the name of the folder containing the scanned audiobook.
        /// </summary>
        public bool CreateParentFolderGroup { get; set; }

        /// <summary>
        ///
        /// </summary>
        public string SleepTimerWarningSound { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int SleepTimerRewindSeconds { get; set; }

        /// <summary>
        ///
        /// </summary>
        public int SleepTimerWarningSoundVolume { get; set; }

        /// <summary>
        /// how much time it takes to constantly interpolate volume toward 0, when sleepTimer triggers
        /// </summary>
        public TimeSpan SleepTimerFadeOut { get; set; }

        /// <summary>
        /// how much (per tick) to decrease volume by
        /// </summary>
        public int SleepTimerFadeOutSpeed { get; set; }

        /// <summary>
        /// how much time until the sleeptimer triggers, if enabled
        /// </summary>
        public TimeSpan SleepTimerInterval { get; set; }

        /// <summary>
        /// type of sleepTimer. Currently only TimerSeconds is implemented
        /// </summary>
        public ESleepTimerType SleepTimerType { get; set; }

        /// <summary>
        /// stores group definitions data from 'group definitions.json', see file in question for more info
        /// </summary>
        public GroupDefinitions SettingsGroupDefinitions { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<IDataGridLayoutInfoSettings> LibraryColumns { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double LibraryScrollOffset { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<ILibraryFilter> LibraryFilters { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, bool> LibraryExpanderStates { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsTTSEnabled { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ETTSVoiceType TTSVoiceType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public EATLReadChaptersFormat ATLReadChaptersFormat { get; set; }

        /// <summary>
        /// called after all App.Globals have been initialized and registered
        /// </summary>
        public void FinishedInit();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="mode"></param>
        public void ToggleLibraryFilter(string identifier, bool? mode = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public bool IsLibraryFilterActive(string identifier);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="audiobook"></param>
        public void AddLastPlayedAudiobook(IAudiobook audiobook);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="audiobook"></param>
        public void RemoveLastPlayedAudiobook(IAudiobook audiobook);

        /// <summary>
        /// allow one autosave
        /// </summary>
        public void MarkChanged();

        /// <summary>
        /// postpone autosave
        /// </summary>
        public void MarkNothingChanged();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="saveCause"></param>
        /// <param name="path">if not supplied, use default settings path</param>
        /// <returns></returns>
        public bool SaveSettings(ESaveCause saveCause = ESaveCause.Manual, string? path = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path">if not supplied, use default settings path</param>
        /// <returns></returns>
        public bool LoadSettings(string? path = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool LoadGroupDefinitions(string? path = null);

        /// <summary>
        /// checks if loaded groups (for expander states) actually still exist in the library. if not,
        /// drop those. Without this, creating a group, saving its data to the settings and then deleting it
        /// would keep the group data in the settings file forever.
        /// </summary>
        public void ValidateLoadedGroups();

        /// <summary>
        /// json deserializes to a separate instance of Settings, then transfers some of the
        /// relevant data back to this main instance. Some serialized data is incomplete to save in
        /// file size, because the data can easily be inferred at runtime.
        /// </summary>
        /// <param name="loadedSettings"></param>
        public void RestoreFromLoadedObject(ISettings loadedSettings);

        /// <summary>
        /// returns the absolute path where the settings files are stored as a string.
        /// This should either be somewhere in AppData, or in the case of an MSIX packaged app, in the app's virtual appdata directory
        /// </summary>
        /// <returns></returns>
        public string GetSettingsDirectory();

        /// <summary>
        /// returns true if at least one valid directory is in ScanDirectories is 
        /// </summary>
        /// <returns></returns>
        public bool HasValidScanDirectory();

        /// <summary>
        /// 
        /// </summary>
        public event Action LoadedSettings;

        /// <summary>
        /// 
        /// </summary>
        public void OnLoadedSettings();

        /// <summary>
        /// 
        /// </summary>
        public event ChangeOptionDelegate ChangeOption;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        public void OnChangeOption(string identifier);
    }
}
