﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// contains data for an audiofile chapter
    /// </summary>
    /// 
    public interface IAudioFileChapterData
    {
        /// <summary>
        /// chapter start time within audiofile in MS
        /// </summary>
        public uint StartTime { get; set; }
        /// <summary>
        /// chapter title
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// chapter number
        /// </summary>
        public int ChapterIdx { get; set; }
    }

    /// <summary>
    /// contains data for an audiofile that can be cached and freed at will
    /// </summary>
    /// 
    public interface IAudioFileData
    {
        /// <summary>
        /// AudioFile the data belongs to
        /// </summary>
        public IAudioFile SourceFile { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DirectoryPath { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AudioFilePath { get; set; }
        /// <summary>
        /// title generated from audiofile metadata
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// album generated from audiofile metadata
        /// </summary>
        public string Album { get; set; }
        /// <summary>
        /// sanitized list of authors generated from audiofile metadata
        /// </summary>
        public List<string> Authors { get; set; }
        /// <summary>
        /// sanitized list of narrators generated from audiofile metadata
        /// </summary>
        public List<string> Narrators { get; set; }
        /// <summary>
        /// sanitized list of genres generated from audiofile metadata
        /// </summary>
        public List<string> Genres { get; set; }
        /// <summary>
        /// sanitized list of genres generated from audiofile metadata
        /// </summary>
        public List<IAudioFileChapterData> Chapters { get; set; }
        /// <summary>
        /// tracknumber generated from audiofile metadata
        /// </summary>
        public int TrackNumber { get; set; }
        /// <summary>
        /// comment generated from audiofile metadata
        /// </summary>
        public string Comment { get; set; }
        /// <summary>
        /// true if the audiofile has an embedded image cover art
        /// </summary>
        public bool ContainsPicture { get; set; }
        /// <summary>
        /// file size in megabytes
        /// </summary>
        public double FileSizeMB { get; set; }
    }

    /// <summary>
    /// data type for an AudioFile
    /// </summary>
    public interface IAudioFile
    {
        /// <summary>
        /// full disk path of the AudioFile
        /// </summary>
        public string AudioFilePath { get; set; }
        /// <summary>
        /// total duration of the AudioFile, or empty TimeSpan if duration has not yet been cached
        /// </summary>
        public TimeSpan Duration { get; set; }
        /// <summary>
        /// Audiobook the AudioFile belongs to. Every file can only belong to one Audiobook
        /// </summary>
        public IAudiobook AssociatedAudiobook { get; set; }
        /// <summary>
        /// index within the associated Audiobook
        /// </summary>
        public int TrackNumber { get; }
        /// <summary>
        /// true if the file is the currently playing audiofile in the linked Audiobook and
        /// the linked Audiobook is currently playing in the PlayerControls
        /// </summary>
        public bool IsPlaying { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Task CacheDurationTask { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsCachingDuration();

        /// <summary>
        /// unregister from current LinkedAudiobook, register to new one
        /// </summary>
        /// <param name="audiobook"></param>
        public void MoveToAudiobook(IAudiobook audiobook);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetTrackNumber();

        /// <summary>
        /// caches AudioFileData of this AudioFile, such as file metadata, embedded image status etc.
        /// </summary>
        /// <returns></returns>
        public IAudioFileData LoadAudioFileData();

        /// <summary>
        /// only loads file chapter metadata
        /// </summary>
        /// <param name="timeOffset">time offset of this file relative to the beginning of the Audiobook</param>
        /// <param name="idxOffset">index of this file inside of its Audiobook</param>
        /// <returns></returns>
        public List<IAudioFileChapterData> LoadChapterData(TimeSpan timeOffset, int idxOffset);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetFileName();

        /// <summary>
        /// synchronously caches full duration of this AudioFile, notifies LinkedAudiobook after it is finished
        /// </summary>
        public void CacheDuration();

        /// <summary>
        /// asynchronously caches full duration of this AudioFile, notifies LinkedAudiobook after it is finished
        /// </summary>
        /// <returns>asynchronous task</returns>
        public Task CacheDurationAsync();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        public void ReadFileSize(IAudioFileData data);

        /// <summary>
        /// using TagLibSharp, reads audiofile metadata
        /// </summary>
        /// <param name="data"></param>
        public void ReadMetaData(IAudioFileData data);

        /// <summary>
        /// utility method for TagLib scan
        /// </summary>
        /// <param name="targetList"></param>
        /// <param name="listToAdd"></param>
        public void AddToListAndSanitize(List<string> targetList, string[] listToAdd);

        /// <summary>
        /// 
        /// </summary>
        public event Action CacheDurationFinished;
        /// <summary>
        /// 
        /// </summary>
        public void OnCacheDurationFinished();
    }
}
