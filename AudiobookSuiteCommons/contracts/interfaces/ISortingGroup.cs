﻿using System.Collections.Generic;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// library sorting group
    /// </summary>
    public interface ISortingGroup
    {
        /// <summary>
        /// UNUSED
        /// </summary>
        public bool IsEnabled { get; set; }
        /// <summary>
        /// unique name of this group
        /// </summary>
        public string GroupName { get; set; }
        /// <summary>
        /// list of all registered items in this group (usually Audiobooks)
        /// </summary>
        public List<object> LinkedObjects { get; set; }
    }
}
