﻿namespace AudiobookSuite.Commons
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="audiobook"></param>
    /// <returns></returns>
    public delegate bool FilterActionDelegate(IAudiobook audiobook);

    /// <summary>
    /// generic list of library filters
    /// </summary>
    public interface ILibraryFilter
    {
        /// <summary>
        /// unique identifier
        /// </summary>
        public string Identifier { get; set; }
        /// <summary>
        /// should this filter currently be used?
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// action to determine whether to filter an audiobook or not
        /// </summary>
        public FilterActionDelegate FilterAction { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="audiobook"></param>
        /// <returns>returns true if the given audiobook should be filtered</returns>
        public bool CheckFilter(IAudiobook audiobook);
    }
}
