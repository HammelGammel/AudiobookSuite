﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// item of the library for grouping. For each group of the LinkedAudiobook, one entry of this
    /// must exist, so the Audiobook can be displayed in multiple groups at once
    /// </summary>
    public class LibraryCollectionItem
    {
        /// <summary>
        /// name of one of the LinkedAudiobook's groups
        /// </summary>
        public string GroupName { get; set; }
        /// <summary>
        /// Audiobook
        /// </summary>
        public IAudiobook LinkedAudiobook { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="audiobook"></param>
        public LibraryCollectionItem(string groupName, IAudiobook audiobook)
        {
            GroupName = groupName;
            LinkedAudiobook = audiobook;
        }
    }

    /// <summary>
    /// interface for the library page
    /// </summary>
    public interface ILibrary
    {
        /// <summary>
        /// list of all Audiobooks, containing an entry for each group the Audiobook is registered in,
        /// to allow the Audiobook to be displayed in the DataGrid in multiple groups at once
        /// </summary>
        public ObservableCollection<LibraryCollectionItem> LibraryAudiobooksFlattened { get; set; }

        /// <summary>
        /// search filter. Corresponds with input of the search bar
        /// </summary>
        public string SearchString { get; set; }

        /// <summary>
        /// called before the library layout is loaded from settings data. Saves default layout,
        /// so it can be restored if necessary
        /// </summary>
        public void SaveLibraryLayoutDefault();

        /// <summary>
        /// restors library DataGrid layout to its default values, such as column order,
        /// column sizes, column sorting etc.
        /// </summary>
        public void ResetLibraryLayoutDefault();

        /// <summary>
        /// resets library DataGrid layout to the data stored in App.Globals.settings
        /// </summary>
        public void ResetLibraryLayout();

        /// <summary>
        /// saves library DataGrid layout to App.Globals.settings
        /// </summary>
        public void SaveLibraryLayout();

        /// <summary>
        /// viewsource is an interface between data backend and WPF frontend, can contain 
        /// efficient filters, sorting etc.without altering the actual data
        /// </summary>
        public void SetupCollectionFilters();

        /// <summary>
        /// 
        /// </summary>
        public void ReloadLibrary();

        /// <summary>
        /// 
        /// </summary>
        public void ApplyFilters();

        /// <summary>
        /// opens a details page for the supplied audiobook
        /// </summary>
        /// <param name="audiobook"></param>
        public void OpenAudiobookDetails(IAudiobook audiobook);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="audiobooks"></param>
        public void OpenRemoveAudiobooksDialog(List<IAudiobook> audiobooks);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="audiobook"></param>
        public void RemoveAudiobook(IAudiobook audiobook);

        /// <summary>
        /// 
        /// </summary>
        public void SetUndoRedoButtonsVisibility();
    }
}
