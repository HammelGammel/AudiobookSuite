﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="file"></param>
    public delegate void AudioFileDelegate(IAudioFile file);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="audiobook"></param>
    public delegate void AudiobookDelegate(IAudiobook audiobook);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="audiobook"></param>
    public delegate void PlayedAudiobookDelegate(IAudiobook audiobook);

    /// <summary>
    /// interface for the PlayerControls control
    /// </summary>
    public interface IPlayerControls
    {
        /// <summary>
        /// volume percentage
        /// </summary>
        public int Volume { get; set; }
        /// <summary>
        /// playback speed, where 1.0 is normal
        /// </summary>
        public float PlaybackSpeed { get; set; }
        /// <summary>
        /// true if playback should be muted
        /// </summary>
        public bool IsMuted { get; set; }
        /// <summary>
        /// true if the popup displaying the time at the cursor position (while hovering timeline) should be displayed
        /// </summary>
        public bool IsTimelineIndicatorVisible { get; set; }
        /// <summary>
        /// currently playing Audiobook or null if no Audiobook is playing
        /// </summary>
        public IAudiobook? CurrentAudiobook { get; set; }
        /// <summary>
        /// currently playing Audiofile, or null if no AudioFile is playing
        /// </summary>
        public IAudioFile? CurrentAudioFile { get; set; }
        /// <summary>
        /// true if not paused
        /// </summary>
        public bool IsPlaying { get; set; }
        /// <summary>
        /// timeline max
        /// </summary>
        public int AudiobookProgressMaxSeconds { get; set; }
        /// <summary>
        /// timeline current
        /// </summary>
        public int AudiobookProgressSeconds { get; set; }
        /// <summary>
        /// true if VLClib is currently buffering the playing AudioFile. Be careful, while buffering,
        /// position within the AudioFile is always at 0, even if it has already been set manually
        /// </summary>
        public bool IsMediaPlayerBuffering { get; set; }

        /// <summary>
        /// when enabling/disabling input from multiple sources, keep track of them. Eg.
        /// Two separate controls disable input, one of them reenables it, but
        /// the other control is still active and input is still desired to be disabled
        /// </summary>
        public List<string> InputDisabledSources { get; set; }

        /// <summary>
        /// fully restore input to the PlayerControls, such as space to play/pause
        /// </summary>
        public void ClearInputDisabled();
        /// <summary>
        /// removes the given source from causes that currently disable input
        /// </summary>
        /// <param name="source"></param>
        public void SetInputEnabled(string source = "");
        /// <summary>
        /// adds the given source to causes that currently disable input
        /// </summary>
        /// <param name="source"></param>
        public void SetInputDisabled(string source = "");
        /// <summary>
        /// returns true if no source currently disables input
        /// </summary>
        /// <returns></returns>
        public bool IsInputEnabled();

        /// <summary>
        /// called after all App.Globals have been initialized and registered
        /// </summary>
        public void FinishedInit();

        /// <summary>
        /// 
        /// </summary>
        public void UpdateControlData();
        /// <summary>
        /// 
        /// </summary>
        public void UpdateMaxProgress();

        /// <summary>
        /// 
        /// </summary>
        public void UpdateTimelineIndicator();
        /// <summary>
        /// 
        /// </summary>
        public void UpdateProgress();

        /// <summary>
        /// set position to bookmark pos, also pushes UndoRedo action
        /// </summary>
        /// <param name="bookMark"></param>
        public void ApplyBookMark(IBookMark bookMark);

        /// <summary>
        /// remove bookmark
        /// </summary>
        /// <param name="bookMark"></param>
        public void RemoveBookMark(IBookMark bookMark);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="duration"></param>
        public void SetAudiobookToPosition(TimeSpan duration);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="audiobook"></param>
        /// <param name="startPlaying"></param>
        /// <param name="isUndoRedo"></param>
        public void PlayAudiobook(IAudiobook audiobook, bool startPlaying = true, bool isUndoRedo = false);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <param name="startPlaying"></param>
        public void PlayAudiobookByTitle(string title, bool startPlaying = true);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        public void PlaySFXAudioFile(string filePath, int volume);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="position"></param>
        /// <param name="startPlaying"></param>
        public void PlayAudioFile(IAudioFile file, TimeSpan position, bool startPlaying = true);
        /// <summary>
        /// toggle play/pause
        /// </summary>
        public void PlayPause();
        /// <summary>
        /// 
        /// </summary>
        public void Play();
        /// <summary>
        /// 
        /// </summary>
        public void Pause();
        /// <summary>
        /// not only pauses, but removes currently playing audiobook data from controls
        /// </summary>
        public void Stop();
        /// <summary>
        /// 
        /// </summary>
        public void NextChapter();
        /// <summary>
        /// 
        /// </summary>
        public void PreviousChapter();
        /// <summary>
        /// 
        /// </summary>
        public void PlayNextAudiobook();
        /// <summary>
        /// 
        /// </summary>
        public void PlayPreviousAudiobook();
        /// <summary>
        /// 
        /// </summary>
        public void NextFile();
        /// <summary>
        /// 
        /// </summary>
        public void PreviousFile();
        /// <summary>
        /// 
        /// </summary>
        public void RemoveCloseBookMark();
        /// <summary>
        /// 
        /// </summary>
        public void AddAutomaticBookMark();
        /// <summary>
        /// 
        /// </summary>
        public void JumpToNextBookmark();
        /// <summary>
        /// 
        /// </summary>
        public void JumpToPreviousBookmark();
        /// <summary>
        /// 
        /// </summary>
        public void Rewind();
        /// <summary>
        /// 
        /// </summary>
        public void Forward();
        /// <summary>
        /// 
        /// </summary>
        public void IncrementVolume();
        /// <summary>
        /// 
        /// </summary>
        public void DecrementVolume();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void SetVolume(int value);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetCurrentAudiobookPositionSeconds();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public TimeSpan GetCurrentAudiobookPosition();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="isMuted"></param>
        public void SetIsMuted(bool isMuted);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rate"></param>
        public void SetPlaybackSpeed(float rate);

        /// <summary>
        /// 
        /// </summary>
        public void ToggleSleepTimerDialog(DependencyObject sender);
        /// <summary>
        /// 
        /// </summary>
        public void CloseSleepTimerDialog();
        /// <summary>
        /// 
        /// </summary>
        public void OpenSleepTimerDialog(DependencyObject sender);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filesFailedToLoad"></param>
        public void OpenDialog_FailedLoadedFiles();

        /// <summary>
        /// 
        /// </summary>
        public event AudiobookDelegate FinishedAudiobook;
        /// <summary>
        /// 
        /// </summary>
        public event AudioFileDelegate PlayedAudioFile;
        /// <summary>
        /// 
        /// </summary>
        public event AudiobookDelegate PlayedAudiobook;
    }
}
