﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="state"></param>
    /// <param name="progress"></param>
    public delegate void UpdatedScannerProgressHandler(EScanState state, int progress);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="newBook"></param>
    public delegate void AddedAudiobookHandler(IAudiobook newBook);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="newBook"></param>
    public delegate void AddedAudioFileHandler(IAudioFile newBook);

    /// <summary>
    /// handles scanning files and folders from disk, bundling into Audiobooks, creating groups and
    /// storing scanned Audiobooks and AudioFiles
    /// </summary>
    public interface IAudiobookScanner
    {
        /// <summary>
        /// name of the path where saved data is stored
        /// </summary>
        public static readonly string AudiobooksFolderPath = "\\saved\\";
        /// <summary>
        /// name of the file where audiobook library is stored
        /// </summary>
        public static readonly string AudiobooksFileName = "audiobooks.json";
        /// <summary>
        /// folder and filename for library file
        /// </summary>
        public static readonly string AudiobooksFilePath = AudiobooksFolderPath + AudiobooksFileName;

        /// <summary>
        /// max Levenstein distance of two audiofile names to be bundled into the same Audiobook automatically
        /// </summary>
        public const int MaxStrDistanceBetweenFilenames = 3;
        
        /// <summary>
        /// list of file extension allowed to be scanned
        /// </summary>
        public static readonly List<string> SupportedMediaExtensions = new List<string>(){
            ".opus", ".flv", ".flac", ".mkv", ".ogm", ".mov", ".ogg", ".es", ".ps", ".ts", ".pva", ".m4a", ".m4b", ".asf", ".aif", ".aifc", ".aiff", ".ai", ".avi", ".mid", ".mpe", ".mpeg", ".mpg", ".mpv2", ".mp2", ".mp3", ".m1v", ".snd", ".wav", ".wm", ".wma", ".wmv" };

        /// <summary>
        /// filter for allowed media file extensions, to be used with an OpenFileDialog for example
        /// </summary>
        public static readonly string SupportedMediaExtensionsFilter =
            "All Audio Files|*.opus;*.flv;*.flac;*.mkv;*.ogm;*.mov;*.ogg;*.es;*.ps;*.ts;*.pva;*.m4a;*.m4b;*.asf;*.aif;*.aifc;*.aiff;*.ai;*.avi;*.mid;*.mpe;*.mpeg;*.mpg;*.mpv2;*.mp2;*.mp3;*.m1v;*.snd;*.wav;*.wm;*.wma;*.wmv;" +
            "|All Files|*";

        /// <summary>
        /// list of all registered Audiobooks
        /// </summary>
        public ObservableCollection<IAudiobook> AudioBookList { get; set; }

        /// <summary>
        /// BackgroundWorker for threaded scanning
        /// </summary>
        public BackgroundWorker? ScanWorker { get; set; }
        /// <summary>
        /// path the current ScanWorker is looking for AudioFiles in
        /// </summary>
        public List<string> ScanWorkerPaths { get; set; }
        /// <summary>
        /// progress state of the ScanWorker
        /// </summary>
        public EScanState ScanState { get; set; }
        /// <summary>
        /// Rough estimate of how far the ScanWorker is with its progress
        /// </summary>
        public int ScanProgressPercentage { get; set; }

        /// <summary>
        /// temporary list of (audio-)filepaths, yet to be iterated over, sorted into single directories
        /// </summary>
        public List<IEnumerable<string>> FilePathDirsOpen { get; set; }

        /// <summary>
        /// Dictionary of all registered AudioFiles, independently of the Audiobook they are registered in.
        /// key is the full audiofile path
        /// </summary>
        public Dictionary<string, IAudioFile> AudioFilesMasterList { get; set; }

        /// <summary>
        /// temporary list of newly created audiobooks. Used to determine on which audiobooks
        /// to call track sorting
        /// </summary>
        public List<IAudiobook> NewAudiobooks { get; set; }

        /// <summary>
        /// true if the library has changed enough to allow an autosave of the library
        /// </summary>
        public bool HasAnythingChanged { get; set; }

        /// <summary>
        /// files that couldn't be found on last library load
        /// </summary>
        public List<IAudioFile> FilesFailedToLoad { get; set; }

        /// <summary>
        /// sanitizes the given title using Regex
        /// </summary>
        /// <param name="titleIn"></param>
        /// <returns></returns>
        public string MakeTitle(string titleIn);

        /// <summary>
        /// called after all App.Globals have been constructed and registered
        /// </summary>
        public void FinishedInit();

        /// <summary>
        /// mark changed for next autosave. As long as nothing has changed, don't autosave
        /// </summary>
        public void MarkChanged();

        /// <summary>
        /// mark nothing changed for next autosave. As long as nothing has changed, don't autosave
        /// </summary>
        public void MarkNothingChanged();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <returns>Audiobook with a corresponding title, or null</returns>
        public IAudiobook GetAudiobookByTitle(string title);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">unique id of the Audiobook</param>
        /// <returns>Audiobook with a corresponding title, or null</returns>
        public IAudiobook GetAudiobookByID(int id);

        /// <summary>
        /// saves library file to a given path, or to the default library save path. Serialized as json
        /// </summary>
        /// <param name="saveCause">cause of the save</param>
        /// <param name="path">full path, including filename and extension where the library file 
        /// should be saved to. If null, uses default path</param>
        /// <returns>true if the library was saved successfully</returns>
        public bool SaveBooksToFile(ESaveCause saveCause = ESaveCause.Manual, string? path = null);

        /// <summary>
        /// loads library data from the given path
        /// </summary>
        /// <param name="path">full path, including filename and extension where the library file 
        /// should be saved to. If null, uses default path</param>
        /// <returns></returns>
        public bool LoadBooksFromFile(string? path = null);

        /// <summary>
        /// removes all Audiobooks and AudioFiles from the library. 
        /// </summary>
        public void ClearAudiobooks();

        /// <summary>
        /// removes specified Audiobook from the library
        /// </summary>
        /// <param name="audiobook"></param>
        public void RemoveAudiobook(IAudiobook audiobook);

        /// <summary>
        ///  removes specified AudioFile from the library
        /// </summary>
        /// <param name="file"></param>
        public void RemoveAudioFile(IAudioFile file);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="audiobook"></param>
        public void AddAudiobook(IAudiobook audiobook);

        /// <summary>
        /// checks if the given title already exists. If so, adds '(copy[n])' to it
        /// </summary>
        /// <param name="title"></param>
        /// <returns>the altered string</returns>
        public string GetFreeTitle(string title);

        /// <summary>
        /// does the library already have an Audiobook with the given title
        /// </summary>
        /// <param name="title"></param>
        /// <returns>true if the title already exists</returns>
        public bool DoesTitleExist(string title);

        /// <summary>
        /// if both supplied Audiobooks are in the same folder, merge them
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public bool AttemptMergeAudiobooks(IAudiobook first, IAudiobook second);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <param name="audiobookPath"></param>
        /// <returns></returns>
        public IAudiobook CreateAudiobook(string title, string? audiobookPath = null);

        /// <summary>
        /// using Regex, checks a few special cases of invalid filenames. Sometimes, Audiobooks
        /// are a folder with the Audiobook name containing various files with unspecific file names and no
        /// title metadata. Eg. the folder is called "Some story" and contains AudioFiles called "chapter 1", "chapter 2",
        /// "chapter 3" etc. all with no title metadata supplied. In this case, the scanner needs to know that neither
        /// the title metadata, nor the first audiofile's file name can be used. In this case, use the folder name to
        /// generate the Audiobook title.
        /// </summary>
        /// <param name="title"></param>
        /// <returns>true if the supplied title is valid</returns>
        public bool IsValidTitle(string title);

        /* - - - */

        /// <summary>
        /// is any scanner thread currently active?
        /// </summary>
        /// <returns>true if already scanning</returns>
        public bool IsScanning();

        /// <summary>
        /// stop scanning on all threads
        /// </summary>
        public void Cancel();

        /// <summary>
        /// scans the supplied path for AudioFiles and automatically sorts them into Audiobooks
        /// </summary>
        /// <param name="path">full path</param>
        public void ScanDirectory(string path);

        /// <summary>
        /// scans the supplied paths for AudioFiles and automatically sorts them into Audiobooks
        /// </summary>
        /// <param name="paths"></param>
        public void ScanDirectories(List<string> paths);

        /// <summary>
        /// Asynchronous part of ScanDirectory
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public Task ScanDirectoryAsync(object sender, DoWorkEventArgs e);

        /// <summary>
        /// given a list of audiofile paths, scans those files and adds them to the supplied Audiobook or
        /// automatically sorts them into an Audiobook if none is supplied
        /// </summary>
        /// <param name="filePaths">list of full audiofile paths, including filenames and extensions</param>
        /// <param name="audiobook">null if it should sort the files automatically into an Audiobook</param>
        /// <returns></returns>
        public Task<List<IAudioFile>> AddAudioFiles(List<string> filePaths, IAudiobook? audiobook = null);

        /// <summary>
        /// given an Audiofile path, scans this file and adds it to the supplied Audiobook or
        /// automatically sorts it into an Audiobook if none is supplied
        /// </summary>
        /// <param name="filePath">full audiofile path, including filename and extension</param>
        /// <param name="audiobook">null if it should sort the files automatically into an Audiobook</param>
        /// <returns></returns>
        public Task<IAudioFile> AddAudioFile(string filePath, IAudiobook? audiobook = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        /// <param name="progress"></param>
        public void ReportScannerProgress(EScanState state, int progress);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ScanDirectoryProgressChanged(object sender, ProgressChangedEventArgs e);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ScanDirectoryCompleted(object sender, RunWorkerCompletedEventArgs e);

        /// <summary>
        /// creates a temporary list of (audio-)filepaths, yet to be iterated over, sorted into single directories
        /// </summary>
        /// <param name="path"></param>
        /// <param name="recursive"></param>
        /// <param name="depth"></param>
        public void GatherFilesFromDir(string path, bool recursive = true, int depth = 0);

        /// <summary>
        /// creates AudioFiles from gathered file paths
        /// </summary>
        public List<IAudioFile> IndexFilesInDir();

        /// <summary>
        /// creates AudioFiles from gathered file paths
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        public List<IAudioFile> IndexFiles(IEnumerable<string> dir);

        /// <summary>
        /// adds file to output list, even if they already exist in masterlist. In that case, return files from masterlist
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        public List<IAudioFile> IndexFilesAllowExisting(IEnumerable<string> dir);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dictionary"></param>
        public void CreateAudiobooksFromDictionary(List<IAudioFile> dictionary);

        /// <summary>
        /// optional data parameter is there so you can pass in already
        /// cached file data.Use if possible to improve performance by avoiding
        /// to read file multiple times
        /// </summary>
        /// <param name="file"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public IAudiobook CreateAudiobookFromFile(IAudioFile file, IAudioFileData? data = null);

        /// <summary>
        /// iterates all genres of the Audiobook, sanitizes them (see group definitions.json), and
        /// creates library groups from them
        /// </summary>
        /// <param name="audiobook"></param>
        /// <param name="data">if null, automatically caches sample file to gather genre data</param>
        /// <returns></returns>
        public List<string> CreateGenreGroups(IAudiobook audiobook, IAudioFileData? data = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="audiobook"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public string CreateFolderGroup(IAudiobook audiobook, IAudioFileData? data = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public IAudioFile? CreateAudioFileFromPath(string path);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns>true if the path is neither null nor an empty string</returns>
        public bool IsValidPath(string path);

        /// <summary>
        /// gets a valid title for a new audiobook this audiofile should be
        /// grouped into.Call with an audiofile contained in the audiobook as a sample
        /// </summary>
        /// <param name="audioFile"></param>
        /// <param name="data">if null, automatically caches sample file to gather genre data</param>
        /// <returns></returns>
        public string? GetTitleForAudiobook(IAudioFile audioFile, IAudioFileData? data = null);

        /// <summary>
        /// for all newly scanned Audiobooks, sort scanned files by filename
        /// </summary>
        public void SortNewTrackNumbers();

        /// <summary>
        /// for all newly scanned AudioFiles, cache their durations from disk using Windows shell
        /// </summary>
        public Task CacheAudioFileDurations(List<IAudioFile> files);

        /// <summary>
        /// sort all registered AudioFiles in the supplied Audiobook by filename
        /// </summary>
        /// <param name="audiobook"></param>
        public void SortAudiobook(IAudiobook audiobook);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public EScanState GetScannerState();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetScannerProgress();

        /// <summary>
        /// 
        /// </summary>
        public event UpdatedScannerProgressHandler UpdatedScanning;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scanState"></param>
        /// <param name="progress"></param>
        public void OnUpdatedScanning(EScanState scanState, int progress);

        /// <summary>
        /// 
        /// </summary>
        public event Action FinishedScanning;
        /// <summary>
        /// 
        /// </summary>
        public void OnFinishedScanning();

        /// <summary>
        /// 
        /// </summary>
        public event Action StartedScanning;
        /// <summary>
        /// 
        /// </summary>
        public void OnStartedScanning();

        /// <summary>
        /// 
        /// </summary>
        public event Action UpdatedAudiobook;
        /// <summary>
        /// 
        /// </summary>
        public void OnUpdatedAudiobook();

        /// <summary>
        /// 
        /// </summary>
        public event AddedAudiobookHandler AddedAudiobook;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newBook"></param>
        public void OnAddedAudiobook(IAudiobook newBook);

        /// <summary>
        /// 
        /// </summary>
        public event AddedAudiobookHandler RemovedAudiobook;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newBook"></param>
        public void OnRemovedAudiobook(IAudiobook newBook);

        /// <summary>
        /// 
        /// </summary>
        public event AddedAudioFileHandler AddedAudioFile;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newBook"></param>
        public void OnAddedAudioFile(IAudioFile newBook);

        /// <summary>
        /// 
        /// </summary>
        public event AddedAudioFileHandler RemovedAudioFile;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newBook"></param>
        public void OnRemovedAudioFile(IAudioFile newBook);

        /// <summary>
        /// 
        /// </summary>
        public event Action LoadedAudiobooks;
        /// <summary>
        /// 
        /// </summary>
        public void OnLoadedAudiobooks();

        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
    }
}