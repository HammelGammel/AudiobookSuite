﻿namespace AudiobookSuite.Commons
{
    /// <summary>
    /// 
    /// </summary>
    public enum ESleepTimerType
    {
        /// <summary>
        /// stop playback after this many seconds
        /// </summary>
        TimerSeconds,
        /// <summary>
        /// stop playback at chapter end (not implemented)
        /// </summary>
        ChapterEnd,
    }
}
