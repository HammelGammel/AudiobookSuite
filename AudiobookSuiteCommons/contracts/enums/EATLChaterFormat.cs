﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudiobookSuite.Commons
{
    public enum EATLReadChaptersFormat
    {
        PreferQuickTime = 0,
        QuickTime = 1,
        PreferNero = 2,
        Nero = 3
    }
}
