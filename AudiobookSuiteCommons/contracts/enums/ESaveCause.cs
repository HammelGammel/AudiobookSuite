﻿namespace AudiobookSuite.Commons
{
    /// <summary>
    /// cause of a save
    /// </summary>
    public enum ESaveCause
    {
        /// <summary>
        /// autosave
        /// </summary>
        Autosave,
        /// <summary>
        /// manually saved
        /// </summary>
        Manual,
        /// <summary>
        /// save on application exit
        /// </summary>
        Shutdown
    }
}
