﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// 
    /// </summary>
    public class BoolConverter : MarkupExtension, IValueConverter
    {
        /// <summary>
        /// 
        /// </summary>
        public object TrueValue { get; set; } = Binding.DoNothing;
        /// <summary>
        /// 
        /// </summary>
        public object FalseValue { get; set; } = Binding.DoNothing;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
                return Binding.DoNothing;

            return (bool)value ? TrueValue : FalseValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == TrueValue)
                return true;

            if (value == FalseValue)
                return false;

            return Binding.DoNothing;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
