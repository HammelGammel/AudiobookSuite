﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// takes two values and returns binding for first valid of the two
    /// </summary>
    public class PriorityConverterTwo : IMultiValueConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length < 4)
                return Binding.DoNothing;

            string first = values[0].ToString();
            string second = values[1].ToString();

            if (first == "True")
                return values[2];

            if (second == "True")
                return values[3];

            /* default value */
            if (values.Length > 4)
                return values[4];

            return Binding.DoNothing;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetTypes"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
