﻿using System;
using System.Collections.Generic;
using System.Windows.Data;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// converts a list of string to a single string, separated by comma
    /// </summary>
    [ValueConversion(typeof(List<string>), typeof(string))]
    public class ListToCSV : IValueConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return "";

            if (value is List<string>)
            {
                return String.Join(", ", ((List<string>)value).ToArray());
            }
            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
