﻿using AudiobookSuite.lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// converter to allow setting audio file as an image path, when the audiofile has an embedded image inside of it
    /// </summary>
    public class ImageSourceConverter : IValueConverter
    {
        private static List<string> SupportedAudioFileExtensions = new List<string>() { ".mp3", ".mp4", ".m4b", ".m4a" };
        private static bool IsImageCacheEnabled = false;
        private static Dictionary<string, object> CachedImages = new Dictionary<string, object>();

        public static void EnableImageCache()
        {
            IsImageCacheEnabled = true;
        }

        public static void DisableImageCache()
        {
            IsImageCacheEnabled = false;
            CachedImages.Clear();
        }

        public object GetImage(string path)
        {
            if (IsImageCacheEnabled)
                if (CachedImages.TryGetValue(path, out var image))
                    if (image != null)
                        return image;

            object newImage = null;
            try
            {
                newImage = Utility.ReadFirstPicture(path);
                if (IsImageCacheEnabled && newImage != null)
                    CachedImages.Add(path, newImage);
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("Error: failed to read/cache image file '" + path + "'");
            }

            return newImage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object path, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (path == null)
                return "";

            if (path is string)
            {
                string stringPath = (string)path;
                string extension = Path.GetExtension(stringPath);

                if (extension != null && extension.Length > 0 &&
                    SupportedAudioFileExtensions.Exists(item => item == extension))
                {
                    /* value is supported audio file format! Create bitmap image from it */
                    return GetImage(stringPath);
                }
                else
                    return path;
            }
            else if (path is BitmapImage)
                return path;

            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
