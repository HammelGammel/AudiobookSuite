﻿using System;
using System.Windows.Data;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// timespan formated to string, formatting HH:MM:SS, handles hours above 24
    /// </summary>
    public class TimeSpanConverter : IValueConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value.GetType() == typeof(TimeSpan))
            {
                TimeSpan timeSpan = (TimeSpan)value;

                return String.Format("{0}:{1:00}:{2:00}", Math.Truncate(timeSpan.TotalHours), timeSpan.Minutes, timeSpan.Seconds);
            }

            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}