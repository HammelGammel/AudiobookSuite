﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// adds 1 to a double value. Useful when binding to an index in
    /// a collection for example. Sometimes a zero based index is
    /// not desired.
    /// </summary>
    public class AddOneConverter : IValueConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double)
                return ((double)value) + 1;
            else if (value is int)
                return ((int)value) + 1;
            else if (value is String)
            {
                try
                {
                    return Double.Parse((string)value) + 1;
                }
                catch (Exception e)
                {
                }
            }

            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double)
                return ((double)value) - 1;
            else if (value is int)
                return ((int)value) - 1;
            else if (value is String)
            {
                try
                {
                    return Double.Parse((string)value) - 1;
                }
                catch (Exception e)
                {
                }
            }

            return value;
        }
    }
}
