﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Input;

namespace AudiobookSuite.Commons
{
    /// <summary>
    /// allows setting up keyboard events even when the application is not focused
    /// </summary>
    public class LowLevelKeyboardListener
    {
        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYDOWN = 0x0100;
        private const int WM_KEYUP = 0x0101;
        private const int WM_SYSKEYDOWN = 0x0104;

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook, LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nCode"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        public delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

        public Dictionary<Key, bool> PressedKeys = new Dictionary<Key, bool>(256);

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<KeyPressedArgs> OnKeyPressed;
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<KeyPressedArgs> OnKeyReleased;

        private LowLevelKeyboardProc _proc;
        private IntPtr _hookID = IntPtr.Zero;

        /// <summary>
        /// 
        /// </summary>
        public LowLevelKeyboardListener()
        {
            _proc = HookCallback;
            OnKeyPressed += OnKeyPressedCallback;
            OnKeyReleased += OnKeyReleasedCallback;
        }

        ~LowLevelKeyboardListener()
        {
            OnKeyPressed -= OnKeyPressedCallback;
            OnKeyReleased -= OnKeyReleasedCallback;
        }

        public bool IsKeyDown(Key key)
        {
            bool valueOut;
            if (!PressedKeys.TryGetValue(key, out valueOut))
                return false;

            return valueOut;
        }

        private void OnKeyPressedCallback(object? sender, KeyPressedArgs e)
        {
            PressedKeys.TryAdd(e.KeyPressed, true);
        } 

        private void OnKeyReleasedCallback(object? sender, KeyPressedArgs e)
        {
            PressedKeys.Remove(e.KeyPressed);
        } 

        /// <summary>
        /// called after all App.Globals have been initialized and registered
        /// </summary>
        public void FinishedInit()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public void HookKeyboard()
        {
            _hookID = SetHook(_proc);
        }

        /// <summary>
        /// 
        /// </summary>
        public void UnHookKeyboard()
        {
            UnhookWindowsHookEx(_hookID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="proc"></param>
        /// <returns></returns>
        private IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                return SetWindowsHookEx(WH_KEYBOARD_LL, proc, GetModuleHandle(curModule.ModuleName), 0);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nCode"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        private IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0)
            {
                int vkCode = Marshal.ReadInt32(lParam);

                if (wParam == (IntPtr)WM_KEYDOWN || wParam == (IntPtr)WM_SYSKEYDOWN)
                {
                    if (OnKeyPressed != null) { OnKeyPressed(this, new KeyPressedArgs(KeyInterop.KeyFromVirtualKey(vkCode))); }
                }
                else if (wParam == (IntPtr)WM_KEYUP)
                {
                    if (OnKeyReleased != null) { OnKeyReleased(this, new KeyPressedArgs(KeyInterop.KeyFromVirtualKey(vkCode))); }
                }
            }

            return CallNextHookEx(_hookID, nCode, wParam, lParam);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class KeyPressedArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public Key KeyPressed { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public KeyPressedArgs(Key key)
        {
            KeyPressed = key;
        }
    }
}
