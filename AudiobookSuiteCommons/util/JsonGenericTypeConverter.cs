﻿using Newtonsoft.Json;
using System;
using JsonConverter = Newtonsoft.Json.JsonConverter;

namespace AudiobookSuite.Commons
{

#pragma warning disable CS1570 // XML comment has badly formed XML
    /// <summary>
    /// json failes when deserializing interface types. Makes sense, since by default json doesn't
    /// serialize type information, and for compatibility's sake I don't want it to either.
    /// Json obviously doesn't know, which implementation of the interface to instanciate when
    /// deserializing. This converter allows to specify, which type to deserialize the property to
    /// </summary>
    /// <code>
    /// [JsonProperty(ItemConverterType = typeof(JsonGenericTypeConverter<AudioFile>))]
    /// IAudioFile myAudioFile;
    /// </code>
    /// <typeparam name="TConcrete"></typeparam>
    public class JsonGenericTypeConverter<TConcrete> : JsonConverter
#pragma warning restore CS1570 // XML comment has badly formed XML
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return serializer.Deserialize<TConcrete>(reader);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }
}
