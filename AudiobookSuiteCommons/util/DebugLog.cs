﻿using System;
using System.Diagnostics;

namespace AudiobookSuite.lib
{
    /// <summary>
    /// easy wrapper for debugging, both to VS console and application console output
    /// </summary>
    public class DebugLog
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sIn"></param>
        public static void WriteLine(string sIn)
        {
            Trace.WriteLine(sIn);
            Console.WriteLine(sIn);
        }
    }
}
